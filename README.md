# Tempo.io REST API client

This [Tempo API v4.0](https://apidocs.tempo.io/) code was generated with [Jane PHP](https://github.com/janephp/janephp)
by using the [OpenAPI specification of the Tempo API](https://apidocs.tempo.io/tempo-core.yaml).

## Installing

Add this in your composer.json:
```json
    "repositories":[
        {
            "type": "vcs",
            "url": "git@gitlab.com:steefdw/tempo-api.git"
        }
    ],
```

And run in your terminal:
```
composer require steefdw/tempo-api
```

## Example usage:

```php
$curlClient = new \Symfony\Component\HttpClient\CurlHttpClient([
    'auth_bearer' => config('tempo.token'),
]);
$httpClient = new \Symfony\Component\HttpClient\Psr18Client($curlClient);
$uri = \Http\Discovery\Psr17FactoryDiscovery::findUriFactory()->createUri('https://api.tempo.io/4');
$plugins = [
    new \Http\Client\Common\Plugin\AddHostPlugin($uri),
    new \Http\Client\Common\Plugin\AddPathPlugin($uri),
];
$httpClient = new \Http\Client\Common\PluginClient($httpClient, $plugins);
$client = \Steefdw\TempoApi\Client::create($httpClient);

// now you can run commands like:
$client->getTeams();      // Steefdw\TempoApi\Model\PageableTeam
$client->getWorklogs();   // Steefdw\TempoApi\Model\PageableWorklog
$client->getCategories(); // Steefdw\TempoApi\Model\CategoryResults
```

## Extra features on top of JanePHP generated code:

- [x] import classes that were used in "extends" and "implements" 
- [x] Fixed error: NonBodyParameterGenerator::convertParameterType(): Return value must be of type array, null returned
- [x] Code-style fixes (@PER, declare strict, ...)
- [ ] multi-line arrays
- [ ] PHP8.x syntax like
    - [ ] match operator
    - [ ] enums
    - [ ] promoted properties
    - [ ] ...