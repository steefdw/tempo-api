<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class MembershipInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     *
     *
     * @var int
     */
    protected $commitmentPercent;
    /**
     * The start date of the `Membership`
     *
     * @var \DateTime
     */
    protected $from;
    /**
     * The Role Id of the `Membership`
     *
     * @var int
     */
    protected $roleId;
    /**
     * The end date of the `Membership`
     *
     * @var \DateTime
     */
    protected $to;

    /**
     *
     *
     * @return int
     */
    public function getCommitmentPercent(): int
    {
        return $this->commitmentPercent;
    }

    /**
     *
     *
     * @param int $commitmentPercent
     *
     * @return Self_
     */
    public function setCommitmentPercent(int $commitmentPercent): self
    {
        $this->initialized['commitmentPercent'] = true;
        $this->commitmentPercent = $commitmentPercent;

        return $this;
    }

    /**
     * The start date of the `Membership`
     *
     * @return \DateTime
     */
    public function getFrom(): \DateTime
    {
        return $this->from;
    }

    /**
     * The start date of the `Membership`
     *
     * @param \DateTime $from
     *
     * @return Self_
     */
    public function setFrom(\DateTime $from): self
    {
        $this->initialized['from'] = true;
        $this->from = $from;

        return $this;
    }

    /**
     * The Role Id of the `Membership`
     *
     * @return int
     */
    public function getRoleId(): int
    {
        return $this->roleId;
    }

    /**
     * The Role Id of the `Membership`
     *
     * @param int $roleId
     *
     * @return Self_
     */
    public function setRoleId(int $roleId): self
    {
        $this->initialized['roleId'] = true;
        $this->roleId = $roleId;

        return $this;
    }

    /**
     * The end date of the `Membership`
     *
     * @return \DateTime
     */
    public function getTo(): \DateTime
    {
        return $this->to;
    }

    /**
     * The end date of the `Membership`
     *
     * @param \DateTime $to
     *
     * @return Self_
     */
    public function setTo(\DateTime $to): self
    {
        $this->initialized['to'] = true;
        $this->to = $to;

        return $this;
    }
}
