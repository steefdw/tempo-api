<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class SkillsAssignmentInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * A set of characters that uniquely identify a resource
     *
     * @var string
     */
    protected $assigneeId;
    /**
     * A string that identify the resource type
     *
     * @var string
     */
    protected $assigneeType;
    /**
     * A list of `Skill` ids
     *
     * @var int[]
     */
    protected $skillIds;

    /**
     * A set of characters that uniquely identify a resource
     *
     * @return string
     */
    public function getAssigneeId(): string
    {
        return $this->assigneeId;
    }

    /**
     * A set of characters that uniquely identify a resource
     *
     * @param string $assigneeId
     *
     * @return Self_
     */
    public function setAssigneeId(string $assigneeId): self
    {
        $this->initialized['assigneeId'] = true;
        $this->assigneeId = $assigneeId;

        return $this;
    }

    /**
     * A string that identify the resource type
     *
     * @return string
     */
    public function getAssigneeType(): string
    {
        return $this->assigneeType;
    }

    /**
     * A string that identify the resource type
     *
     * @param string $assigneeType
     *
     * @return Self_
     */
    public function setAssigneeType(string $assigneeType): self
    {
        $this->initialized['assigneeType'] = true;
        $this->assigneeType = $assigneeType;

        return $this;
    }

    /**
     * A list of `Skill` ids
     *
     * @return int[]
     */
    public function getSkillIds(): array
    {
        return $this->skillIds;
    }

    /**
     * A list of `Skill` ids
     *
     * @param int[] $skillIds
     *
     * @return Self_
     */
    public function setSkillIds(array $skillIds): self
    {
        $this->initialized['skillIds'] = true;
        $this->skillIds = $skillIds;

        return $this;
    }
}
