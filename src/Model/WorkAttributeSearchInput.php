<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class WorkAttributeSearchInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * Ids of the worklogs you want to search work attribute values for
     *
     * @var int[]
     */
    protected $tempoWorklogIds;

    /**
     * Ids of the worklogs you want to search work attribute values for
     *
     * @return int[]
     */
    public function getTempoWorklogIds(): array
    {
        return $this->tempoWorklogIds;
    }

    /**
     * Ids of the worklogs you want to search work attribute values for
     *
     * @param int[] $tempoWorklogIds
     *
     * @return Self_
     */
    public function setTempoWorklogIds(array $tempoWorklogIds): self
    {
        $this->initialized['tempoWorklogIds'] = true;
        $this->tempoWorklogIds = $tempoWorklogIds;

        return $this;
    }
}
