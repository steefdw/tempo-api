<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class GlobalConfiguration extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * Boolean to inform if it is possible to login on closed account
     *
     * @var bool
     */
    protected $allowLoggingOnClosedAccount;
    /**
     * Shows if Timesheet approval periods are weekly or monthly.
     *
     * @var string
     */
    protected $approvalPeriod;
    /**
     * Shows the start day. 1 is monday, 7 is sunday.
     *
     * @var int
     */
    protected $approvalWeekStart;
    /**
     * The max hours a user can work
     *
     * @var int
     */
    protected $maxHoursPerDayPerUser;
    /**
     * Shows how many days you can track time into the future.
     *
     * @var int
     */
    protected $numberOfDaysAllowedIntoFuture;
    /**
     * Boolean to inform if planning approval is enabled
     *
     * @var bool
     */
    protected $planApprovalEnabled;
    /**
     * Shows if remaining estimate is required when tracking time.
     *
     * @var bool
     */
    protected $remainingEstimateOptional;
    /**
     * Boolean to inform if start and end times should be used for logging time
     *
     * @var bool
     */
    protected $startAndEndTimesEnabled;
    /**
     * Boolean to inform if start and end times should be used for planning time
     *
     * @var bool
     */
    protected $startAndEndTimesForPlanningEnabled;
    /**
     * 1 is monday, 7 is sunday
     *
     * @var int
     */
    protected $weekStart;
    /**
     * Shows if description is required when tracking time.
     *
     * @var bool
     */
    protected $worklogDescriptionOptional;

    /**
     * Boolean to inform if it is possible to login on closed account
     *
     * @return bool
     */
    public function getAllowLoggingOnClosedAccount(): bool
    {
        return $this->allowLoggingOnClosedAccount;
    }

    /**
     * Boolean to inform if it is possible to login on closed account
     *
     * @param bool $allowLoggingOnClosedAccount
     *
     * @return Self_
     */
    public function setAllowLoggingOnClosedAccount(bool $allowLoggingOnClosedAccount): self
    {
        $this->initialized['allowLoggingOnClosedAccount'] = true;
        $this->allowLoggingOnClosedAccount = $allowLoggingOnClosedAccount;

        return $this;
    }

    /**
     * Shows if Timesheet approval periods are weekly or monthly.
     *
     * @return string
     */
    public function getApprovalPeriod(): string
    {
        return $this->approvalPeriod;
    }

    /**
     * Shows if Timesheet approval periods are weekly or monthly.
     *
     * @param string $approvalPeriod
     *
     * @return Self_
     */
    public function setApprovalPeriod(string $approvalPeriod): self
    {
        $this->initialized['approvalPeriod'] = true;
        $this->approvalPeriod = $approvalPeriod;

        return $this;
    }

    /**
     * Shows the start day. 1 is monday, 7 is sunday.
     *
     * @return int
     */
    public function getApprovalWeekStart(): int
    {
        return $this->approvalWeekStart;
    }

    /**
     * Shows the start day. 1 is monday, 7 is sunday.
     *
     * @param int $approvalWeekStart
     *
     * @return Self_
     */
    public function setApprovalWeekStart(int $approvalWeekStart): self
    {
        $this->initialized['approvalWeekStart'] = true;
        $this->approvalWeekStart = $approvalWeekStart;

        return $this;
    }

    /**
     * The max hours a user can work
     *
     * @return int
     */
    public function getMaxHoursPerDayPerUser(): int
    {
        return $this->maxHoursPerDayPerUser;
    }

    /**
     * The max hours a user can work
     *
     * @param int $maxHoursPerDayPerUser
     *
     * @return Self_
     */
    public function setMaxHoursPerDayPerUser(int $maxHoursPerDayPerUser): self
    {
        $this->initialized['maxHoursPerDayPerUser'] = true;
        $this->maxHoursPerDayPerUser = $maxHoursPerDayPerUser;

        return $this;
    }

    /**
     * Shows how many days you can track time into the future.
     *
     * @return int
     */
    public function getNumberOfDaysAllowedIntoFuture(): int
    {
        return $this->numberOfDaysAllowedIntoFuture;
    }

    /**
     * Shows how many days you can track time into the future.
     *
     * @param int $numberOfDaysAllowedIntoFuture
     *
     * @return Self_
     */
    public function setNumberOfDaysAllowedIntoFuture(int $numberOfDaysAllowedIntoFuture): self
    {
        $this->initialized['numberOfDaysAllowedIntoFuture'] = true;
        $this->numberOfDaysAllowedIntoFuture = $numberOfDaysAllowedIntoFuture;

        return $this;
    }

    /**
     * Boolean to inform if planning approval is enabled
     *
     * @return bool
     */
    public function getPlanApprovalEnabled(): bool
    {
        return $this->planApprovalEnabled;
    }

    /**
     * Boolean to inform if planning approval is enabled
     *
     * @param bool $planApprovalEnabled
     *
     * @return Self_
     */
    public function setPlanApprovalEnabled(bool $planApprovalEnabled): self
    {
        $this->initialized['planApprovalEnabled'] = true;
        $this->planApprovalEnabled = $planApprovalEnabled;

        return $this;
    }

    /**
     * Shows if remaining estimate is required when tracking time.
     *
     * @return bool
     */
    public function getRemainingEstimateOptional(): bool
    {
        return $this->remainingEstimateOptional;
    }

    /**
     * Shows if remaining estimate is required when tracking time.
     *
     * @param bool $remainingEstimateOptional
     *
     * @return Self_
     */
    public function setRemainingEstimateOptional(bool $remainingEstimateOptional): self
    {
        $this->initialized['remainingEstimateOptional'] = true;
        $this->remainingEstimateOptional = $remainingEstimateOptional;

        return $this;
    }

    /**
     * Boolean to inform if start and end times should be used for logging time
     *
     * @return bool
     */
    public function getStartAndEndTimesEnabled(): bool
    {
        return $this->startAndEndTimesEnabled;
    }

    /**
     * Boolean to inform if start and end times should be used for logging time
     *
     * @param bool $startAndEndTimesEnabled
     *
     * @return Self_
     */
    public function setStartAndEndTimesEnabled(bool $startAndEndTimesEnabled): self
    {
        $this->initialized['startAndEndTimesEnabled'] = true;
        $this->startAndEndTimesEnabled = $startAndEndTimesEnabled;

        return $this;
    }

    /**
     * Boolean to inform if start and end times should be used for planning time
     *
     * @return bool
     */
    public function getStartAndEndTimesForPlanningEnabled(): bool
    {
        return $this->startAndEndTimesForPlanningEnabled;
    }

    /**
     * Boolean to inform if start and end times should be used for planning time
     *
     * @param bool $startAndEndTimesForPlanningEnabled
     *
     * @return Self_
     */
    public function setStartAndEndTimesForPlanningEnabled(bool $startAndEndTimesForPlanningEnabled): self
    {
        $this->initialized['startAndEndTimesForPlanningEnabled'] = true;
        $this->startAndEndTimesForPlanningEnabled = $startAndEndTimesForPlanningEnabled;

        return $this;
    }

    /**
     * 1 is monday, 7 is sunday
     *
     * @return int
     */
    public function getWeekStart(): int
    {
        return $this->weekStart;
    }

    /**
     * 1 is monday, 7 is sunday
     *
     * @param int $weekStart
     *
     * @return Self_
     */
    public function setWeekStart(int $weekStart): self
    {
        $this->initialized['weekStart'] = true;
        $this->weekStart = $weekStart;

        return $this;
    }

    /**
     * Shows if description is required when tracking time.
     *
     * @return bool
     */
    public function getWorklogDescriptionOptional(): bool
    {
        return $this->worklogDescriptionOptional;
    }

    /**
     * Shows if description is required when tracking time.
     *
     * @param bool $worklogDescriptionOptional
     *
     * @return Self_
     */
    public function setWorklogDescriptionOptional(bool $worklogDescriptionOptional): self
    {
        $this->initialized['worklogDescriptionOptional'] = true;
        $this->worklogDescriptionOptional = $worklogDescriptionOptional;

        return $this;
    }
}
