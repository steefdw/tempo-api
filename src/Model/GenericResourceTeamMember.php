<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class GenericResourceTeamMember extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * Timestamp (in UTC) of when this `Generic Resource` was added to this team
     *
     * @var string
     */
    protected $assignedToTeamAt;
    /**
     * The creator of the `Generic Resource`
     *
     * @var UserRefBean
     */
    protected $assignedToTeamBy;
    /**
     * A list of references to the the `Generic Resources` that are members of this team
     *
     * @var GenericResourceReference
     */
    protected $member;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;
    /**
     * A reference to the team this `Generic Resource Team Member` belongs to
     *
     * @var TeamReference
     */
    protected $team;

    /**
     * Timestamp (in UTC) of when this `Generic Resource` was added to this team
     *
     * @return string
     */
    public function getAssignedToTeamAt(): string
    {
        return $this->assignedToTeamAt;
    }

    /**
     * Timestamp (in UTC) of when this `Generic Resource` was added to this team
     *
     * @param string $assignedToTeamAt
     *
     * @return Self_
     */
    public function setAssignedToTeamAt(string $assignedToTeamAt): self
    {
        $this->initialized['assignedToTeamAt'] = true;
        $this->assignedToTeamAt = $assignedToTeamAt;

        return $this;
    }

    /**
     * The creator of the `Generic Resource`
     *
     * @return UserRefBean
     */
    public function getAssignedToTeamBy(): UserRefBean
    {
        return $this->assignedToTeamBy;
    }

    /**
     * The creator of the `Generic Resource`
     *
     * @param UserRefBean $assignedToTeamBy
     *
     * @return Self_
     */
    public function setAssignedToTeamBy(UserRefBean $assignedToTeamBy): self
    {
        $this->initialized['assignedToTeamBy'] = true;
        $this->assignedToTeamBy = $assignedToTeamBy;

        return $this;
    }

    /**
     * A list of references to the the `Generic Resources` that are members of this team
     *
     * @return GenericResourceReference
     */
    public function getMember(): GenericResourceReference
    {
        return $this->member;
    }

    /**
     * A list of references to the the `Generic Resources` that are members of this team
     *
     * @param GenericResourceReference $member
     *
     * @return Self_
     */
    public function setMember(GenericResourceReference $member): self
    {
        $this->initialized['member'] = true;
        $this->member = $member;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }

    /**
     * A reference to the team this `Generic Resource Team Member` belongs to
     *
     * @return TeamReference
     */
    public function getTeam(): TeamReference
    {
        return $this->team;
    }

    /**
     * A reference to the team this `Generic Resource Team Member` belongs to
     *
     * @param TeamReference $team
     *
     * @return Self_
     */
    public function setTeam(TeamReference $team): self
    {
        $this->initialized['team'] = true;
        $this->team = $team;

        return $this;
    }
}
