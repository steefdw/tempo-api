<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class WorkloadSchemeInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The list of days and their required hours in the workload scheme. This list must be unique and contains all weekdays [Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday]
     *
     * @var WorkloadSchemeDayInput[]
     */
    protected $days;
    /**
     * The description of the workload scheme
     *
     * @var string
     */
    protected $description;
    /**
     * The name of the workload scheme
     *
     * @var string
     */
    protected $name;

    /**
     * The list of days and their required hours in the workload scheme. This list must be unique and contains all weekdays [Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday]
     *
     * @return WorkloadSchemeDayInput[]
     */
    public function getDays(): array
    {
        return $this->days;
    }

    /**
     * The list of days and their required hours in the workload scheme. This list must be unique and contains all weekdays [Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday]
     *
     * @param WorkloadSchemeDayInput[] $days
     *
     * @return Self_
     */
    public function setDays(array $days): self
    {
        $this->initialized['days'] = true;
        $this->days = $days;

        return $this;
    }

    /**
     * The description of the workload scheme
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * The description of the workload scheme
     *
     * @param string $description
     *
     * @return Self_
     */
    public function setDescription(string $description): self
    {
        $this->initialized['description'] = true;
        $this->description = $description;

        return $this;
    }

    /**
     * The name of the workload scheme
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * The name of the workload scheme
     *
     * @param string $name
     *
     * @return Self_
     */
    public function setName(string $name): self
    {
        $this->initialized['name'] = true;
        $this->name = $name;

        return $this;
    }
}
