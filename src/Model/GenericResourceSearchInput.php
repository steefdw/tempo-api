<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class GenericResourceSearchInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * A list of `Generic Resource` ids
     *
     * @var int[]
     */
    protected $ids;
    /**
     * Maximum number of results on each page
     *
     * @var int
     */
    protected $limit;
    /**
     * Number of skipped results
     *
     * @var int
     */
    protected $offset;
    /**
     * A query to be used for string matching against name of `Generic Resources`
     *
     * @var string
     */
    protected $query;

    /**
     * A list of `Generic Resource` ids
     *
     * @return int[]
     */
    public function getIds(): array
    {
        return $this->ids;
    }

    /**
     * A list of `Generic Resource` ids
     *
     * @param int[] $ids
     *
     * @return Self_
     */
    public function setIds(array $ids): self
    {
        $this->initialized['ids'] = true;
        $this->ids = $ids;

        return $this;
    }

    /**
     * Maximum number of results on each page
     *
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * Maximum number of results on each page
     *
     * @param int $limit
     *
     * @return Self_
     */
    public function setLimit(int $limit): self
    {
        $this->initialized['limit'] = true;
        $this->limit = $limit;

        return $this;
    }

    /**
     * Number of skipped results
     *
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * Number of skipped results
     *
     * @param int $offset
     *
     * @return Self_
     */
    public function setOffset(int $offset): self
    {
        $this->initialized['offset'] = true;
        $this->offset = $offset;

        return $this;
    }

    /**
     * A query to be used for string matching against name of `Generic Resources`
     *
     * @return string
     */
    public function getQuery(): string
    {
        return $this->query;
    }

    /**
     * A query to be used for string matching against name of `Generic Resources`
     *
     * @param string $query
     *
     * @return Self_
     */
    public function setQuery(string $query): self
    {
        $this->initialized['query'] = true;
        $this->query = $query;

        return $this;
    }
}
