<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class HolidayInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The date of the `Holiday`
     *
     * @var \DateTime
     */
    protected $date;
    /**
     * The description of the `Holiday`
     *
     * @var string
     */
    protected $description;
    /**
     * The duration in seconds of the `Holiday`
     *
     * @var int
     */
    protected $durationSeconds;
    /**
     * The name of the `Holiday`
     *
     * @var string
     */
    protected $name;
    /**
     * The type of the `Holiday`
     *
     * @var string
     */
    protected $type;

    /**
     * The date of the `Holiday`
     *
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * The date of the `Holiday`
     *
     * @param \DateTime $date
     *
     * @return Self_
     */
    public function setDate(\DateTime $date): self
    {
        $this->initialized['date'] = true;
        $this->date = $date;

        return $this;
    }

    /**
     * The description of the `Holiday`
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * The description of the `Holiday`
     *
     * @param string $description
     *
     * @return Self_
     */
    public function setDescription(string $description): self
    {
        $this->initialized['description'] = true;
        $this->description = $description;

        return $this;
    }

    /**
     * The duration in seconds of the `Holiday`
     *
     * @return int
     */
    public function getDurationSeconds(): int
    {
        return $this->durationSeconds;
    }

    /**
     * The duration in seconds of the `Holiday`
     *
     * @param int $durationSeconds
     *
     * @return Self_
     */
    public function setDurationSeconds(int $durationSeconds): self
    {
        $this->initialized['durationSeconds'] = true;
        $this->durationSeconds = $durationSeconds;

        return $this;
    }

    /**
     * The name of the `Holiday`
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * The name of the `Holiday`
     *
     * @param string $name
     *
     * @return Self_
     */
    public function setName(string $name): self
    {
        $this->initialized['name'] = true;
        $this->name = $name;

        return $this;
    }

    /**
     * The type of the `Holiday`
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * The type of the `Holiday`
     *
     * @param string $type
     *
     * @return Self_
     */
    public function setType(string $type): self
    {
        $this->initialized['type'] = true;
        $this->type = $type;

        return $this;
    }
}
