<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class WorklogSearchInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * Ids of the authors you want to search worklogs for.
     *
     * @var string[]
     */
    protected $authorIds;
    /**
     * Retrieve worklogs starting with this date.
     *
     * @var string
     */
    protected $from;
    /**
     * Ids of the issues you want to search worklogs for
     *
     * @var int[]
     */
    protected $issueIds;
    /**
     * Ids of the projects you want to search worklogs for
     *
     * @var int[]
     */
    protected $projectIds;
    /**
     * Retrieve worklogs that ends up to and including this date.
     *
     * @var string
     */
    protected $to;
    /**
     * Retrieve worklogs that have been updated from this date
     *
     * @var string
     */
    protected $updatedFrom;

    /**
     * Ids of the authors you want to search worklogs for.
     *
     * @return string[]
     */
    public function getAuthorIds(): array
    {
        return $this->authorIds;
    }

    /**
     * Ids of the authors you want to search worklogs for.
     *
     * @param string[] $authorIds
     *
     * @return Self_
     */
    public function setAuthorIds(array $authorIds): self
    {
        $this->initialized['authorIds'] = true;
        $this->authorIds = $authorIds;

        return $this;
    }

    /**
     * Retrieve worklogs starting with this date.
     *
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * Retrieve worklogs starting with this date.
     *
     * @param string $from
     *
     * @return Self_
     */
    public function setFrom(string $from): self
    {
        $this->initialized['from'] = true;
        $this->from = $from;

        return $this;
    }

    /**
     * Ids of the issues you want to search worklogs for
     *
     * @return int[]
     */
    public function getIssueIds(): array
    {
        return $this->issueIds;
    }

    /**
     * Ids of the issues you want to search worklogs for
     *
     * @param int[] $issueIds
     *
     * @return Self_
     */
    public function setIssueIds(array $issueIds): self
    {
        $this->initialized['issueIds'] = true;
        $this->issueIds = $issueIds;

        return $this;
    }

    /**
     * Ids of the projects you want to search worklogs for
     *
     * @return int[]
     */
    public function getProjectIds(): array
    {
        return $this->projectIds;
    }

    /**
     * Ids of the projects you want to search worklogs for
     *
     * @param int[] $projectIds
     *
     * @return Self_
     */
    public function setProjectIds(array $projectIds): self
    {
        $this->initialized['projectIds'] = true;
        $this->projectIds = $projectIds;

        return $this;
    }

    /**
     * Retrieve worklogs that ends up to and including this date.
     *
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * Retrieve worklogs that ends up to and including this date.
     *
     * @param string $to
     *
     * @return Self_
     */
    public function setTo(string $to): self
    {
        $this->initialized['to'] = true;
        $this->to = $to;

        return $this;
    }

    /**
     * Retrieve worklogs that have been updated from this date
     *
     * @return string
     */
    public function getUpdatedFrom(): string
    {
        return $this->updatedFrom;
    }

    /**
     * Retrieve worklogs that have been updated from this date
     *
     * @param string $updatedFrom
     *
     * @return Self_
     */
    public function setUpdatedFrom(string $updatedFrom): self
    {
        $this->initialized['updatedFrom'] = true;
        $this->updatedFrom = $updatedFrom;

        return $this;
    }
}
