<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class GenericResourceInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The name of the `Generic Resource`
     *
     * @var string
     */
    protected $name;

    /**
     * The name of the `Generic Resource`
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * The name of the `Generic Resource`
     *
     * @param string $name
     *
     * @return Self_
     */
    public function setName(string $name): self
    {
        $this->initialized['name'] = true;
        $this->name = $name;

        return $this;
    }
}
