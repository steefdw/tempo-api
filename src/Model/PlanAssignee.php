<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class PlanAssignee extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The id of the `Assignee`.
     *
     * @var string
     */
    protected $id;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;
    /**
     * The type of the `Assignee`.
     *
     * @var string
     */
    protected $type;

    /**
     * The id of the `Assignee`.
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * The id of the `Assignee`.
     *
     * @param string $id
     *
     * @return Self_
     */
    public function setId(string $id): self
    {
        $this->initialized['id'] = true;
        $this->id = $id;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }

    /**
     * The type of the `Assignee`.
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * The type of the `Assignee`.
     *
     * @param string $type
     *
     * @return Self_
     */
    public function setType(string $type): self
    {
        $this->initialized['type'] = true;
        $this->type = $type;

        return $this;
    }
}
