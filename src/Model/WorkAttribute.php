<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class WorkAttribute extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The key of the `WorkAttribute`
     *
     * @var string
     */
    protected $key;
    /**
     * The name of the `WorkAttribute`
     *
     * @var string
     */
    protected $name;
    /**
     * Only relevant when type is `STATIC_LIST`. Each `STATIC_LIST` entry has an immutable `value` which is stored with the worklog, and a `name` that is shown in the UI. The `name` can be changed.
     *
     * @var array<string, string>
     */
    protected $names;
    /**
     * Whether this `WorkAttribute` is required
     *
     * @var bool
     */
    protected $required;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;
    /**
     * The type of the `WorkAttribute`
     *
     * @var string
     */
    protected $type;
    /**
     * Only relevant when type is `STATIC_LIST`. These values are immutable. Their UI representation can be looked up in the `names` object below
     *
     * @var string[]
     */
    protected $values;

    /**
     * The key of the `WorkAttribute`
     *
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * The key of the `WorkAttribute`
     *
     * @param string $key
     *
     * @return Self_
     */
    public function setKey(string $key): self
    {
        $this->initialized['key'] = true;
        $this->key = $key;

        return $this;
    }

    /**
     * The name of the `WorkAttribute`
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * The name of the `WorkAttribute`
     *
     * @param string $name
     *
     * @return Self_
     */
    public function setName(string $name): self
    {
        $this->initialized['name'] = true;
        $this->name = $name;

        return $this;
    }

    /**
     * Only relevant when type is `STATIC_LIST`. Each `STATIC_LIST` entry has an immutable `value` which is stored with the worklog, and a `name` that is shown in the UI. The `name` can be changed.
     *
     * @return array<string, string>
     */
    public function getNames(): iterable
    {
        return $this->names;
    }

    /**
     * Only relevant when type is `STATIC_LIST`. Each `STATIC_LIST` entry has an immutable `value` which is stored with the worklog, and a `name` that is shown in the UI. The `name` can be changed.
     *
     * @param array<string, string> $names
     *
     * @return Self_
     */
    public function setNames(iterable $names): self
    {
        $this->initialized['names'] = true;
        $this->names = $names;

        return $this;
    }

    /**
     * Whether this `WorkAttribute` is required
     *
     * @return bool
     */
    public function getRequired(): bool
    {
        return $this->required;
    }

    /**
     * Whether this `WorkAttribute` is required
     *
     * @param bool $required
     *
     * @return Self_
     */
    public function setRequired(bool $required): self
    {
        $this->initialized['required'] = true;
        $this->required = $required;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }

    /**
     * The type of the `WorkAttribute`
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * The type of the `WorkAttribute`
     *
     * @param string $type
     *
     * @return Self_
     */
    public function setType(string $type): self
    {
        $this->initialized['type'] = true;
        $this->type = $type;

        return $this;
    }

    /**
     * Only relevant when type is `STATIC_LIST`. These values are immutable. Their UI representation can be looked up in the `names` object below
     *
     * @return string[]
     */
    public function getValues(): array
    {
        return $this->values;
    }

    /**
     * Only relevant when type is `STATIC_LIST`. These values are immutable. Their UI representation can be looked up in the `names` object below
     *
     * @param string[] $values
     *
     * @return Self_
     */
    public function setValues(array $values): self
    {
        $this->initialized['values'] = true;
        $this->values = $values;

        return $this;
    }
}
