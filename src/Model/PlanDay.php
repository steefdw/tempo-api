<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class PlanDay extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * A date to represent a planned day for the `Plan`.
     *
     * @var string
     */
    protected $date;
    /**
     * The amount of seconds that will be invested in the `Plan` for the planned date.
     *
     * @var int
     */
    protected $plannedSeconds;

    /**
     * A date to represent a planned day for the `Plan`.
     *
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * A date to represent a planned day for the `Plan`.
     *
     * @param string $date
     *
     * @return Self_
     */
    public function setDate(string $date): self
    {
        $this->initialized['date'] = true;
        $this->date = $date;

        return $this;
    }

    /**
     * The amount of seconds that will be invested in the `Plan` for the planned date.
     *
     * @return int
     */
    public function getPlannedSeconds(): int
    {
        return $this->plannedSeconds;
    }

    /**
     * The amount of seconds that will be invested in the `Plan` for the planned date.
     *
     * @param int $plannedSeconds
     *
     * @return Self_
     */
    public function setPlannedSeconds(int $plannedSeconds): self
    {
        $this->initialized['plannedSeconds'] = true;
        $this->plannedSeconds = $plannedSeconds;

        return $this;
    }
}
