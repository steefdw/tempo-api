<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class WorkloadSchemeDay extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The day of the week
     *
     * @var string
     */
    protected $day;
    /**
     * The number of seconds required to be worked
     *
     * @var int
     */
    protected $requiredSeconds;

    /**
     * The day of the week
     *
     * @return string
     */
    public function getDay(): string
    {
        return $this->day;
    }

    /**
     * The day of the week
     *
     * @param string $day
     *
     * @return Self_
     */
    public function setDay(string $day): self
    {
        $this->initialized['day'] = true;
        $this->day = $day;

        return $this;
    }

    /**
     * The number of seconds required to be worked
     *
     * @return int
     */
    public function getRequiredSeconds(): int
    {
        return $this->requiredSeconds;
    }

    /**
     * The number of seconds required to be worked
     *
     * @param int $requiredSeconds
     *
     * @return Self_
     */
    public function setRequiredSeconds(int $requiredSeconds): self
    {
        $this->initialized['requiredSeconds'] = true;
        $this->requiredSeconds = $requiredSeconds;

        return $this;
    }
}
