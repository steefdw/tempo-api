<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class TeamLink extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The id of the `Team Link`
     *
     * @var int
     */
    protected $id;
    /**
     * The team scope
     *
     * @var TeamLinkScope
     */
    protected $scope;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;
    /**
     *
     *
     * @var Team
     */
    protected $team;

    /**
     * The id of the `Team Link`
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * The id of the `Team Link`
     *
     * @param int $id
     *
     * @return Self_
     */
    public function setId(int $id): self
    {
        $this->initialized['id'] = true;
        $this->id = $id;

        return $this;
    }

    /**
     * The team scope
     *
     * @return TeamLinkScope
     */
    public function getScope(): TeamLinkScope
    {
        return $this->scope;
    }

    /**
     * The team scope
     *
     * @param TeamLinkScope $scope
     *
     * @return Self_
     */
    public function setScope(TeamLinkScope $scope): self
    {
        $this->initialized['scope'] = true;
        $this->scope = $scope;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }

    /**
     *
     *
     * @return Team
     */
    public function getTeam(): Team
    {
        return $this->team;
    }

    /**
     *
     *
     * @param Team $team
     *
     * @return Self_
     */
    public function setTeam(Team $team): self
    {
        $this->initialized['team'] = true;
        $this->team = $team;

        return $this;
    }
}
