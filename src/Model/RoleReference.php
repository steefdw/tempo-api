<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class RoleReference extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The id of the `Role`
     *
     * @var int
     */
    protected $id;
    /**
     * The name of the `Role`
     *
     * @var string
     */
    protected $name;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;

    /**
     * The id of the `Role`
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * The id of the `Role`
     *
     * @param int $id
     *
     * @return Self_
     */
    public function setId(int $id): self
    {
        $this->initialized['id'] = true;
        $this->id = $id;

        return $this;
    }

    /**
     * The name of the `Role`
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * The name of the `Role`
     *
     * @param string $name
     *
     * @return Self_
     */
    public function setName(string $name): self
    {
        $this->initialized['name'] = true;
        $this->name = $name;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }
}
