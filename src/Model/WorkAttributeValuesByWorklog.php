<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class WorkAttributeValuesByWorklog extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * Id of the Worklog
     *
     * @var int
     */
    protected $tempoWorklogId;
    /**
     * List of the Work Attribute Values
     *
     * @var WorkAttributeValue[]
     */
    protected $workAttributeValues;

    /**
     * Id of the Worklog
     *
     * @return int
     */
    public function getTempoWorklogId(): int
    {
        return $this->tempoWorklogId;
    }

    /**
     * Id of the Worklog
     *
     * @param int $tempoWorklogId
     *
     * @return Self_
     */
    public function setTempoWorklogId(int $tempoWorklogId): self
    {
        $this->initialized['tempoWorklogId'] = true;
        $this->tempoWorklogId = $tempoWorklogId;

        return $this;
    }

    /**
     * List of the Work Attribute Values
     *
     * @return WorkAttributeValue[]
     */
    public function getWorkAttributeValues(): array
    {
        return $this->workAttributeValues;
    }

    /**
     * List of the Work Attribute Values
     *
     * @param WorkAttributeValue[] $workAttributeValues
     *
     * @return Self_
     */
    public function setWorkAttributeValues(array $workAttributeValues): self
    {
        $this->initialized['workAttributeValues'] = true;
        $this->workAttributeValues = $workAttributeValues;

        return $this;
    }
}
