<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class JiraWorklogIdList extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * A list of Jira Worklog ids you want to retrieve the respective Tempo Worklog ids
     *
     * @var int[]
     */
    protected $jiraWorklogIds;

    /**
     * A list of Jira Worklog ids you want to retrieve the respective Tempo Worklog ids
     *
     * @return int[]
     */
    public function getJiraWorklogIds(): array
    {
        return $this->jiraWorklogIds;
    }

    /**
     * A list of Jira Worklog ids you want to retrieve the respective Tempo Worklog ids
     *
     * @param int[] $jiraWorklogIds
     *
     * @return Self_
     */
    public function setJiraWorklogIds(array $jiraWorklogIds): self
    {
        $this->initialized['jiraWorklogIds'] = true;
        $this->jiraWorklogIds = $jiraWorklogIds;

        return $this;
    }
}
