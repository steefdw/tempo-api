<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class AccountSearchInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * An optional boolean to indicate if the account should be global or not
     *
     * @var bool
     */
    protected $global;
    /**
     * A list of `Account` ids
     *
     * @var int[]
     */
    protected $ids;
    /**
     * A list of `Account` keys
     *
     * @var string[]
     */
    protected $keys;
    /**
     * A list of `Account` statuses
     *
     * @var string[]
     */
    protected $statuses;

    /**
     * An optional boolean to indicate if the account should be global or not
     *
     * @return bool
     */
    public function getGlobal(): bool
    {
        return $this->global;
    }

    /**
     * An optional boolean to indicate if the account should be global or not
     *
     * @param bool $global
     *
     * @return Self_
     */
    public function setGlobal(bool $global): self
    {
        $this->initialized['global'] = true;
        $this->global = $global;

        return $this;
    }

    /**
     * A list of `Account` ids
     *
     * @return int[]
     */
    public function getIds(): array
    {
        return $this->ids;
    }

    /**
     * A list of `Account` ids
     *
     * @param int[] $ids
     *
     * @return Self_
     */
    public function setIds(array $ids): self
    {
        $this->initialized['ids'] = true;
        $this->ids = $ids;

        return $this;
    }

    /**
     * A list of `Account` keys
     *
     * @return string[]
     */
    public function getKeys(): array
    {
        return $this->keys;
    }

    /**
     * A list of `Account` keys
     *
     * @param string[] $keys
     *
     * @return Self_
     */
    public function setKeys(array $keys): self
    {
        $this->initialized['keys'] = true;
        $this->keys = $keys;

        return $this;
    }

    /**
     * A list of `Account` statuses
     *
     * @return string[]
     */
    public function getStatuses(): array
    {
        return $this->statuses;
    }

    /**
     * A list of `Account` statuses
     *
     * @param string[] $statuses
     *
     * @return Self_
     */
    public function setStatuses(array $statuses): self
    {
        $this->initialized['statuses'] = true;
        $this->statuses = $statuses;

        return $this;
    }
}
