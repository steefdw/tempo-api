<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class Team extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * Determine if this is an administrative team
     *
     * @var bool
     */
    protected $administrative;
    /**
     * The id of the `Team`
     *
     * @var int
     */
    protected $id;
    /**
     * Reference to the Leader of the `Team`
     *
     * @var TeamLead
     */
    protected $lead;
    /**
     * The collection of links pointing to this Team
     *
     * @var SelfLink
     */
    protected $links;
    /**
     * The collection of members pointing to this Team
     *
     * @var SelfMember
     */
    protected $members;
    /**
     * The Team's name
     *
     * @var string
     */
    protected $name;
    /**
     *
     *
     * @var Self_
     */
    protected $permissions;
    /**
     * Link to the program of this Team
     *
     * @var ProgramReference
     */
    protected $program;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;
    /**
     * Brief description of the `Team`
     *
     * @var string
     */
    protected $summary;

    /**
     * Determine if this is an administrative team
     *
     * @return bool
     */
    public function getAdministrative(): bool
    {
        return $this->administrative;
    }

    /**
     * Determine if this is an administrative team
     *
     * @param bool $administrative
     *
     * @return Self_
     */
    public function setAdministrative(bool $administrative): self
    {
        $this->initialized['administrative'] = true;
        $this->administrative = $administrative;

        return $this;
    }

    /**
     * The id of the `Team`
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * The id of the `Team`
     *
     * @param int $id
     *
     * @return Self_
     */
    public function setId(int $id): self
    {
        $this->initialized['id'] = true;
        $this->id = $id;

        return $this;
    }

    /**
     * Reference to the Leader of the `Team`
     *
     * @return TeamLead
     */
    public function getLead(): TeamLead
    {
        return $this->lead;
    }

    /**
     * Reference to the Leader of the `Team`
     *
     * @param TeamLead $lead
     *
     * @return Self_
     */
    public function setLead(TeamLead $lead): self
    {
        $this->initialized['lead'] = true;
        $this->lead = $lead;

        return $this;
    }

    /**
     * The collection of links pointing to this Team
     *
     * @return SelfLink
     */
    public function getLinks(): SelfLink
    {
        return $this->links;
    }

    /**
     * The collection of links pointing to this Team
     *
     * @param SelfLink $links
     *
     * @return Self_
     */
    public function setLinks(SelfLink $links): self
    {
        $this->initialized['links'] = true;
        $this->links = $links;

        return $this;
    }

    /**
     * The collection of members pointing to this Team
     *
     * @return SelfMember
     */
    public function getMembers(): SelfMember
    {
        return $this->members;
    }

    /**
     * The collection of members pointing to this Team
     *
     * @param SelfMember $members
     *
     * @return Self_
     */
    public function setMembers(SelfMember $members): self
    {
        $this->initialized['members'] = true;
        $this->members = $members;

        return $this;
    }

    /**
     * The Team's name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * The Team's name
     *
     * @param string $name
     *
     * @return Self_
     */
    public function setName(string $name): self
    {
        $this->initialized['name'] = true;
        $this->name = $name;

        return $this;
    }

    /**
     *
     *
     * @return Self_
     */
    public function getPermissions(): self
    {
        return $this->permissions;
    }

    /**
     *
     *
     * @param Self_ $permissions
     *
     * @return Self_
     */
    public function setPermissions(self $permissions): self
    {
        $this->initialized['permissions'] = true;
        $this->permissions = $permissions;

        return $this;
    }

    /**
     * Link to the program of this Team
     *
     * @return ProgramReference
     */
    public function getProgram(): ProgramReference
    {
        return $this->program;
    }

    /**
     * Link to the program of this Team
     *
     * @param ProgramReference $program
     *
     * @return Self_
     */
    public function setProgram(ProgramReference $program): self
    {
        $this->initialized['program'] = true;
        $this->program = $program;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }

    /**
     * Brief description of the `Team`
     *
     * @return string
     */
    public function getSummary(): string
    {
        return $this->summary;
    }

    /**
     * Brief description of the `Team`
     *
     * @param string $summary
     *
     * @return Self_
     */
    public function setSummary(string $summary): self
    {
        $this->initialized['summary'] = true;
        $this->summary = $summary;

        return $this;
    }
}
