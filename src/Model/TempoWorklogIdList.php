<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class TempoWorklogIdList extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * A list of Tempo Worklog ids you want to retrieve the respective Jira Worklog ids
     *
     * @var int[]
     */
    protected $tempoWorklogIds;

    /**
     * A list of Tempo Worklog ids you want to retrieve the respective Jira Worklog ids
     *
     * @return int[]
     */
    public function getTempoWorklogIds(): array
    {
        return $this->tempoWorklogIds;
    }

    /**
     * A list of Tempo Worklog ids you want to retrieve the respective Jira Worklog ids
     *
     * @param int[] $tempoWorklogIds
     *
     * @return Self_
     */
    public function setTempoWorklogIds(array $tempoWorklogIds): self
    {
        $this->initialized['tempoWorklogIds'] = true;
        $this->tempoWorklogIds = $tempoWorklogIds;

        return $this;
    }
}
