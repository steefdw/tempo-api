<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class GenericResource extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * Timestamp (in UTC) of when this `Generic Resource` was created
     *
     * @var string
     */
    protected $createdAt;
    /**
     * The creator of the `Generic Resource`
     *
     * @var UserRefBean
     */
    protected $createdBy;
    /**
     * The id of the `Generic Resource`
     *
     * @var int
     */
    protected $id;
    /**
     * The name of the `Generic Resource`
     *
     * @var string
     */
    protected $name;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;
    /**
     * Timestamp (in UTC) of when this `Generic Resource` was last updated
     *
     * @var string
     */
    protected $updatedAt;
    /**
     * The creator of the `Generic Resource`
     *
     * @var UserRefBean
     */
    protected $updatedBy;

    /**
     * Timestamp (in UTC) of when this `Generic Resource` was created
     *
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    /**
     * Timestamp (in UTC) of when this `Generic Resource` was created
     *
     * @param string $createdAt
     *
     * @return Self_
     */
    public function setCreatedAt(string $createdAt): self
    {
        $this->initialized['createdAt'] = true;
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * The creator of the `Generic Resource`
     *
     * @return UserRefBean
     */
    public function getCreatedBy(): UserRefBean
    {
        return $this->createdBy;
    }

    /**
     * The creator of the `Generic Resource`
     *
     * @param UserRefBean $createdBy
     *
     * @return Self_
     */
    public function setCreatedBy(UserRefBean $createdBy): self
    {
        $this->initialized['createdBy'] = true;
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * The id of the `Generic Resource`
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * The id of the `Generic Resource`
     *
     * @param int $id
     *
     * @return Self_
     */
    public function setId(int $id): self
    {
        $this->initialized['id'] = true;
        $this->id = $id;

        return $this;
    }

    /**
     * The name of the `Generic Resource`
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * The name of the `Generic Resource`
     *
     * @param string $name
     *
     * @return Self_
     */
    public function setName(string $name): self
    {
        $this->initialized['name'] = true;
        $this->name = $name;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }

    /**
     * Timestamp (in UTC) of when this `Generic Resource` was last updated
     *
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }

    /**
     * Timestamp (in UTC) of when this `Generic Resource` was last updated
     *
     * @param string $updatedAt
     *
     * @return Self_
     */
    public function setUpdatedAt(string $updatedAt): self
    {
        $this->initialized['updatedAt'] = true;
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * The creator of the `Generic Resource`
     *
     * @return UserRefBean
     */
    public function getUpdatedBy(): UserRefBean
    {
        return $this->updatedBy;
    }

    /**
     * The creator of the `Generic Resource`
     *
     * @param UserRefBean $updatedBy
     *
     * @return Self_
     */
    public function setUpdatedBy(UserRefBean $updatedBy): self
    {
        $this->initialized['updatedBy'] = true;
        $this->updatedBy = $updatedBy;

        return $this;
    }
}
