<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class PlanPeriod extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The starting date of the `Period`.
     *
     * @var string
     */
    protected $from;
    /**
     * Total planned seconds for the `Period`.
     *
     * @var int
     */
    protected $plannedSeconds;
    /**
     * The ending date of the `Period`.
     *
     * @var string
     */
    protected $to;

    /**
     * The starting date of the `Period`.
     *
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * The starting date of the `Period`.
     *
     * @param string $from
     *
     * @return Self_
     */
    public function setFrom(string $from): self
    {
        $this->initialized['from'] = true;
        $this->from = $from;

        return $this;
    }

    /**
     * Total planned seconds for the `Period`.
     *
     * @return int
     */
    public function getPlannedSeconds(): int
    {
        return $this->plannedSeconds;
    }

    /**
     * Total planned seconds for the `Period`.
     *
     * @param int $plannedSeconds
     *
     * @return Self_
     */
    public function setPlannedSeconds(int $plannedSeconds): self
    {
        $this->initialized['plannedSeconds'] = true;
        $this->plannedSeconds = $plannedSeconds;

        return $this;
    }

    /**
     * The ending date of the `Period`.
     *
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * The ending date of the `Period`.
     *
     * @param string $to
     *
     * @return Self_
     */
    public function setTo(string $to): self
    {
        $this->initialized['to'] = true;
        $this->to = $to;

        return $this;
    }
}
