<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class AccountLink extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     *
     *
     * @var Self_
     */
    protected $account;
    /**
     * If the `AccountLink` is the default for the scope
     *
     * @var bool
     */
    protected $default;
    /**
     * The id of the `AccountLink`
     *
     * @var int
     */
    protected $id;
    /**
     * Link to the scope of the `AccountLink`
     *
     * @var AccountLinkScope
     */
    protected $scope;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;

    /**
     *
     *
     * @return Self_
     */
    public function getAccount(): self
    {
        return $this->account;
    }

    /**
     *
     *
     * @param Self_ $account
     *
     * @return Self_
     */
    public function setAccount(self $account): self
    {
        $this->initialized['account'] = true;
        $this->account = $account;

        return $this;
    }

    /**
     * If the `AccountLink` is the default for the scope
     *
     * @return bool
     */
    public function getDefault(): bool
    {
        return $this->default;
    }

    /**
     * If the `AccountLink` is the default for the scope
     *
     * @param bool $default
     *
     * @return Self_
     */
    public function setDefault(bool $default): self
    {
        $this->initialized['default'] = true;
        $this->default = $default;

        return $this;
    }

    /**
     * The id of the `AccountLink`
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * The id of the `AccountLink`
     *
     * @param int $id
     *
     * @return Self_
     */
    public function setId(int $id): self
    {
        $this->initialized['id'] = true;
        $this->id = $id;

        return $this;
    }

    /**
     * Link to the scope of the `AccountLink`
     *
     * @return AccountLinkScope
     */
    public function getScope(): AccountLinkScope
    {
        return $this->scope;
    }

    /**
     * Link to the scope of the `AccountLink`
     *
     * @param AccountLinkScope $scope
     *
     * @return Self_
     */
    public function setScope(AccountLinkScope $scope): self
    {
        $this->initialized['scope'] = true;
        $this->scope = $scope;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }
}
