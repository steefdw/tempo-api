<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class Permission extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The key of the `Permission`
     *
     * @var string
     */
    protected $key;

    /**
     * The key of the `Permission`
     *
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * The key of the `Permission`
     *
     * @param string $key
     *
     * @return Self_
     */
    public function setKey(string $key): self
    {
        $this->initialized['key'] = true;
        $this->key = $key;

        return $this;
    }
}
