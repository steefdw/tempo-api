<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class PageableWithoutSelfLink extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     *
     *
     * @var PageableMetadata
     */
    protected $metadata;
    /**
     *
     *
     * @var GenericResource[]
     */
    protected $results;

    /**
     *
     *
     * @return PageableMetadata
     */
    public function getMetadata(): PageableMetadata
    {
        return $this->metadata;
    }

    /**
     *
     *
     * @param PageableMetadata $metadata
     *
     * @return Self_
     */
    public function setMetadata(PageableMetadata $metadata): self
    {
        $this->initialized['metadata'] = true;
        $this->metadata = $metadata;

        return $this;
    }

    /**
     *
     *
     * @return GenericResource[]
     */
    public function getResults(): array
    {
        return $this->results;
    }

    /**
     *
     *
     * @param GenericResource[] $results
     *
     * @return Self_
     */
    public function setResults(array $results): self
    {
        $this->initialized['results'] = true;
        $this->results = $results;

        return $this;
    }
}
