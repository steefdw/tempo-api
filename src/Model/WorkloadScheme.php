<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class WorkloadScheme extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The list of days and their required hours in this `WorkloadScheme`
     *
     * @var WorkloadSchemeDay[]
     */
    protected $days;
    /**
     * If this is the default `WorkloadScheme`
     *
     * @var bool
     */
    protected $defaultScheme;
    /**
     * The description `WorkloadScheme`
     *
     * @var string
     */
    protected $description;
    /**
     * The id of the `WorkloadScheme`
     *
     * @var int
     */
    protected $id;
    /**
     * The number of members in this `WorkloadScheme`
     *
     * @var int
     */
    protected $memberCount;
    /**
     * The name of the `WorkloadScheme`
     *
     * @var string
     */
    protected $name;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;

    /**
     * The list of days and their required hours in this `WorkloadScheme`
     *
     * @return WorkloadSchemeDay[]
     */
    public function getDays(): array
    {
        return $this->days;
    }

    /**
     * The list of days and their required hours in this `WorkloadScheme`
     *
     * @param WorkloadSchemeDay[] $days
     *
     * @return Self_
     */
    public function setDays(array $days): self
    {
        $this->initialized['days'] = true;
        $this->days = $days;

        return $this;
    }

    /**
     * If this is the default `WorkloadScheme`
     *
     * @return bool
     */
    public function getDefaultScheme(): bool
    {
        return $this->defaultScheme;
    }

    /**
     * If this is the default `WorkloadScheme`
     *
     * @param bool $defaultScheme
     *
     * @return Self_
     */
    public function setDefaultScheme(bool $defaultScheme): self
    {
        $this->initialized['defaultScheme'] = true;
        $this->defaultScheme = $defaultScheme;

        return $this;
    }

    /**
     * The description `WorkloadScheme`
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * The description `WorkloadScheme`
     *
     * @param string $description
     *
     * @return Self_
     */
    public function setDescription(string $description): self
    {
        $this->initialized['description'] = true;
        $this->description = $description;

        return $this;
    }

    /**
     * The id of the `WorkloadScheme`
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * The id of the `WorkloadScheme`
     *
     * @param int $id
     *
     * @return Self_
     */
    public function setId(int $id): self
    {
        $this->initialized['id'] = true;
        $this->id = $id;

        return $this;
    }

    /**
     * The number of members in this `WorkloadScheme`
     *
     * @return int
     */
    public function getMemberCount(): int
    {
        return $this->memberCount;
    }

    /**
     * The number of members in this `WorkloadScheme`
     *
     * @param int $memberCount
     *
     * @return Self_
     */
    public function setMemberCount(int $memberCount): self
    {
        $this->initialized['memberCount'] = true;
        $this->memberCount = $memberCount;

        return $this;
    }

    /**
     * The name of the `WorkloadScheme`
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * The name of the `WorkloadScheme`
     *
     * @param string $name
     *
     * @return Self_
     */
    public function setName(string $name): self
    {
        $this->initialized['name'] = true;
        $this->name = $name;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }
}
