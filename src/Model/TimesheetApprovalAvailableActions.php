<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class TimesheetApprovalAvailableActions extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * A permanent link to this resource
     *
     * @var SelfApiBean
     */
    protected $approve;
    /**
     * A permanent link to this resource
     *
     * @var SelfApiBean
     */
    protected $reject;
    /**
     * A permanent link to this resource
     *
     * @var SelfApiBean
     */
    protected $reopen;
    /**
     * A permanent link to this resource
     *
     * @var SelfApiBean
     */
    protected $submit;

    /**
     * A permanent link to this resource
     *
     * @return SelfApiBean
     */
    public function getApprove(): SelfApiBean
    {
        return $this->approve;
    }

    /**
     * A permanent link to this resource
     *
     * @param SelfApiBean $approve
     *
     * @return Self_
     */
    public function setApprove(SelfApiBean $approve): self
    {
        $this->initialized['approve'] = true;
        $this->approve = $approve;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return SelfApiBean
     */
    public function getReject(): SelfApiBean
    {
        return $this->reject;
    }

    /**
     * A permanent link to this resource
     *
     * @param SelfApiBean $reject
     *
     * @return Self_
     */
    public function setReject(SelfApiBean $reject): self
    {
        $this->initialized['reject'] = true;
        $this->reject = $reject;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return SelfApiBean
     */
    public function getReopen(): SelfApiBean
    {
        return $this->reopen;
    }

    /**
     * A permanent link to this resource
     *
     * @param SelfApiBean $reopen
     *
     * @return Self_
     */
    public function setReopen(SelfApiBean $reopen): self
    {
        $this->initialized['reopen'] = true;
        $this->reopen = $reopen;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return SelfApiBean
     */
    public function getSubmit(): SelfApiBean
    {
        return $this->submit;
    }

    /**
     * A permanent link to this resource
     *
     * @param SelfApiBean $submit
     *
     * @return Self_
     */
    public function setSubmit(SelfApiBean $submit): self
    {
        $this->initialized['submit'] = true;
        $this->submit = $submit;

        return $this;
    }
}
