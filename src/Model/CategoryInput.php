<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class CategoryInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * A set of characters that uniquely identify the category
     *
     * @var string
     */
    protected $key;
    /**
     * The name of the category
     *
     * @var string
     */
    protected $name;
    /**
     * The type of the category
     *
     * @var string
     */
    protected $typeName;

    /**
     * A set of characters that uniquely identify the category
     *
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * A set of characters that uniquely identify the category
     *
     * @param string $key
     *
     * @return Self_
     */
    public function setKey(string $key): self
    {
        $this->initialized['key'] = true;
        $this->key = $key;

        return $this;
    }

    /**
     * The name of the category
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * The name of the category
     *
     * @param string $name
     *
     * @return Self_
     */
    public function setName(string $name): self
    {
        $this->initialized['name'] = true;
        $this->name = $name;

        return $this;
    }

    /**
     * The type of the category
     *
     * @return string
     */
    public function getTypeName(): string
    {
        return $this->typeName;
    }

    /**
     * The type of the category
     *
     * @param string $typeName
     *
     * @return Self_
     */
    public function setTypeName(string $typeName): self
    {
        $this->initialized['typeName'] = true;
        $this->typeName = $typeName;

        return $this;
    }
}
