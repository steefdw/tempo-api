<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class Category extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The id of the `Category`
     *
     * @var int
     */
    protected $id;
    /**
     * The key of the `Category`
     *
     * @var string
     */
    protected $key;
    /**
     * The name of the `Category`
     *
     * @var string
     */
    protected $name;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;
    /**
     * The type of the `Category`
     *
     * @var CategoryType
     */
    protected $type;

    /**
     * The id of the `Category`
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * The id of the `Category`
     *
     * @param int $id
     *
     * @return Self_
     */
    public function setId(int $id): self
    {
        $this->initialized['id'] = true;
        $this->id = $id;

        return $this;
    }

    /**
     * The key of the `Category`
     *
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * The key of the `Category`
     *
     * @param string $key
     *
     * @return Self_
     */
    public function setKey(string $key): self
    {
        $this->initialized['key'] = true;
        $this->key = $key;

        return $this;
    }

    /**
     * The name of the `Category`
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * The name of the `Category`
     *
     * @param string $name
     *
     * @return Self_
     */
    public function setName(string $name): self
    {
        $this->initialized['name'] = true;
        $this->name = $name;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }

    /**
     * The type of the `Category`
     *
     * @return CategoryType
     */
    public function getType(): CategoryType
    {
        return $this->type;
    }

    /**
     * The type of the `Category`
     *
     * @param CategoryType $type
     *
     * @return Self_
     */
    public function setType(CategoryType $type): self
    {
        $this->initialized['type'] = true;
        $this->type = $type;

        return $this;
    }
}
