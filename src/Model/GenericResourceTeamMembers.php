<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class GenericResourceTeamMembers extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * Count of `Generic Resource Team Members` in this team
     *
     * @var int
     */
    protected $count;
    /**
     * A list of references to the the `Generic Resources` that are members of this team
     *
     * @var GenericResourceReference[]
     */
    protected $members;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;
    /**
     * A reference to the team this `Generic Resource Team Member` belongs to
     *
     * @var TeamReference
     */
    protected $team;

    /**
     * Count of `Generic Resource Team Members` in this team
     *
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * Count of `Generic Resource Team Members` in this team
     *
     * @param int $count
     *
     * @return Self_
     */
    public function setCount(int $count): self
    {
        $this->initialized['count'] = true;
        $this->count = $count;

        return $this;
    }

    /**
     * A list of references to the the `Generic Resources` that are members of this team
     *
     * @return GenericResourceReference[]
     */
    public function getMembers(): array
    {
        return $this->members;
    }

    /**
     * A list of references to the the `Generic Resources` that are members of this team
     *
     * @param GenericResourceReference[] $members
     *
     * @return Self_
     */
    public function setMembers(array $members): self
    {
        $this->initialized['members'] = true;
        $this->members = $members;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }

    /**
     * A reference to the team this `Generic Resource Team Member` belongs to
     *
     * @return TeamReference
     */
    public function getTeam(): TeamReference
    {
        return $this->team;
    }

    /**
     * A reference to the team this `Generic Resource Team Member` belongs to
     *
     * @param TeamReference $team
     *
     * @return Self_
     */
    public function setTeam(TeamReference $team): self
    {
        $this->initialized['team'] = true;
        $this->team = $team;

        return $this;
    }
}
