<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class PlanInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The id of the `Assignee` for the `Plan`.
     *
     * @var string
     */
    protected $assigneeId;
    /**
     * The type of the `Assignee` for the `Plan`.
     *
     * @var string
     */
    protected $assigneeType;
    /**
     * A description containing details of the `Plan`.
     *
     * @var string
     */
    protected $description;
    /**
     * The ending date of the `Plan`.
     *
     * @var string
     */
    protected $endDate;
    /**
     * Boolean value that informs whether non-working days are included in the `Plan`.
     *
     * @var bool
     */
    protected $includeNonWorkingDays;
    /**
     * Reference to an approval linked to the `Plan`.
     *
     * @var PlanApprovalInput
     */
    protected $planApproval;
    /**
     * The id of the `Plan Item` for the `Plan`.
     *
     * @var string
     */
    protected $planItemId;
    /**
     * The type of the `Plan Item` for the `Plan`.
     *
     * @var string
     */
    protected $planItemType;
    /**
     * The amount of seconds that will be invested daily on the `Plan`.
     *
     * @var int
     */
    protected $plannedSecondsPerDay;
    /**
     * End date for the recurrence of the `Plan`.
     *
     * @var string
     */
    protected $recurrenceEndDate;
    /**
     * Recurrence rule of the `Plan`.
     *
     * @var string
     */
    protected $rule;
    /**
     * The starting date of the `Plan`.
     *
     * @var string
     */
    protected $startDate;
    /**
     * The start time of the `Plan`.
     *
     * @var string
     */
    protected $startTime;

    /**
     * The id of the `Assignee` for the `Plan`.
     *
     * @return string
     */
    public function getAssigneeId(): string
    {
        return $this->assigneeId;
    }

    /**
     * The id of the `Assignee` for the `Plan`.
     *
     * @param string $assigneeId
     *
     * @return Self_
     */
    public function setAssigneeId(string $assigneeId): self
    {
        $this->initialized['assigneeId'] = true;
        $this->assigneeId = $assigneeId;

        return $this;
    }

    /**
     * The type of the `Assignee` for the `Plan`.
     *
     * @return string
     */
    public function getAssigneeType(): string
    {
        return $this->assigneeType;
    }

    /**
     * The type of the `Assignee` for the `Plan`.
     *
     * @param string $assigneeType
     *
     * @return Self_
     */
    public function setAssigneeType(string $assigneeType): self
    {
        $this->initialized['assigneeType'] = true;
        $this->assigneeType = $assigneeType;

        return $this;
    }

    /**
     * A description containing details of the `Plan`.
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * A description containing details of the `Plan`.
     *
     * @param string $description
     *
     * @return Self_
     */
    public function setDescription(string $description): self
    {
        $this->initialized['description'] = true;
        $this->description = $description;

        return $this;
    }

    /**
     * The ending date of the `Plan`.
     *
     * @return string
     */
    public function getEndDate(): string
    {
        return $this->endDate;
    }

    /**
     * The ending date of the `Plan`.
     *
     * @param string $endDate
     *
     * @return Self_
     */
    public function setEndDate(string $endDate): self
    {
        $this->initialized['endDate'] = true;
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Boolean value that informs whether non-working days are included in the `Plan`.
     *
     * @return bool
     */
    public function getIncludeNonWorkingDays(): bool
    {
        return $this->includeNonWorkingDays;
    }

    /**
     * Boolean value that informs whether non-working days are included in the `Plan`.
     *
     * @param bool $includeNonWorkingDays
     *
     * @return Self_
     */
    public function setIncludeNonWorkingDays(bool $includeNonWorkingDays): self
    {
        $this->initialized['includeNonWorkingDays'] = true;
        $this->includeNonWorkingDays = $includeNonWorkingDays;

        return $this;
    }

    /**
     * Reference to an approval linked to the `Plan`.
     *
     * @return PlanApprovalInput
     */
    public function getPlanApproval(): PlanApprovalInput
    {
        return $this->planApproval;
    }

    /**
     * Reference to an approval linked to the `Plan`.
     *
     * @param PlanApprovalInput $planApproval
     *
     * @return Self_
     */
    public function setPlanApproval(PlanApprovalInput $planApproval): self
    {
        $this->initialized['planApproval'] = true;
        $this->planApproval = $planApproval;

        return $this;
    }

    /**
     * The id of the `Plan Item` for the `Plan`.
     *
     * @return string
     */
    public function getPlanItemId(): string
    {
        return $this->planItemId;
    }

    /**
     * The id of the `Plan Item` for the `Plan`.
     *
     * @param string $planItemId
     *
     * @return Self_
     */
    public function setPlanItemId(string $planItemId): self
    {
        $this->initialized['planItemId'] = true;
        $this->planItemId = $planItemId;

        return $this;
    }

    /**
     * The type of the `Plan Item` for the `Plan`.
     *
     * @return string
     */
    public function getPlanItemType(): string
    {
        return $this->planItemType;
    }

    /**
     * The type of the `Plan Item` for the `Plan`.
     *
     * @param string $planItemType
     *
     * @return Self_
     */
    public function setPlanItemType(string $planItemType): self
    {
        $this->initialized['planItemType'] = true;
        $this->planItemType = $planItemType;

        return $this;
    }

    /**
     * The amount of seconds that will be invested daily on the `Plan`.
     *
     * @return int
     */
    public function getPlannedSecondsPerDay(): int
    {
        return $this->plannedSecondsPerDay;
    }

    /**
     * The amount of seconds that will be invested daily on the `Plan`.
     *
     * @param int $plannedSecondsPerDay
     *
     * @return Self_
     */
    public function setPlannedSecondsPerDay(int $plannedSecondsPerDay): self
    {
        $this->initialized['plannedSecondsPerDay'] = true;
        $this->plannedSecondsPerDay = $plannedSecondsPerDay;

        return $this;
    }

    /**
     * End date for the recurrence of the `Plan`.
     *
     * @return string
     */
    public function getRecurrenceEndDate(): string
    {
        return $this->recurrenceEndDate;
    }

    /**
     * End date for the recurrence of the `Plan`.
     *
     * @param string $recurrenceEndDate
     *
     * @return Self_
     */
    public function setRecurrenceEndDate(string $recurrenceEndDate): self
    {
        $this->initialized['recurrenceEndDate'] = true;
        $this->recurrenceEndDate = $recurrenceEndDate;

        return $this;
    }

    /**
     * Recurrence rule of the `Plan`.
     *
     * @return string
     */
    public function getRule(): string
    {
        return $this->rule;
    }

    /**
     * Recurrence rule of the `Plan`.
     *
     * @param string $rule
     *
     * @return Self_
     */
    public function setRule(string $rule): self
    {
        $this->initialized['rule'] = true;
        $this->rule = $rule;

        return $this;
    }

    /**
     * The starting date of the `Plan`.
     *
     * @return string
     */
    public function getStartDate(): string
    {
        return $this->startDate;
    }

    /**
     * The starting date of the `Plan`.
     *
     * @param string $startDate
     *
     * @return Self_
     */
    public function setStartDate(string $startDate): self
    {
        $this->initialized['startDate'] = true;
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * The start time of the `Plan`.
     *
     * @return string
     */
    public function getStartTime(): string
    {
        return $this->startTime;
    }

    /**
     * The start time of the `Plan`.
     *
     * @param string $startTime
     *
     * @return Self_
     */
    public function setStartTime(string $startTime): self
    {
        $this->initialized['startTime'] = true;
        $this->startTime = $startTime;

        return $this;
    }
}
