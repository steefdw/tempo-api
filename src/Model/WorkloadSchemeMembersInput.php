<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class WorkloadSchemeMembersInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The list of account ids in the workload scheme
     *
     * @var string[]
     */
    protected $accountIds;

    /**
     * The list of account ids in the workload scheme
     *
     * @return string[]
     */
    public function getAccountIds(): array
    {
        return $this->accountIds;
    }

    /**
     * The list of account ids in the workload scheme
     *
     * @param string[] $accountIds
     *
     * @return Self_
     */
    public function setAccountIds(array $accountIds): self
    {
        $this->initialized['accountIds'] = true;
        $this->accountIds = $accountIds;

        return $this;
    }
}
