<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class TeamLinkInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The id of the scope
     *
     * @var int
     */
    protected $scopeId;
    /**
     * The type of scope of the `Team Link`
     *
     * @var string
     */
    protected $scopeType;
    /**
     * The id of the `Team Link`
     *
     * @var int
     */
    protected $teamId;

    /**
     * The id of the scope
     *
     * @return int
     */
    public function getScopeId(): int
    {
        return $this->scopeId;
    }

    /**
     * The id of the scope
     *
     * @param int $scopeId
     *
     * @return Self_
     */
    public function setScopeId(int $scopeId): self
    {
        $this->initialized['scopeId'] = true;
        $this->scopeId = $scopeId;

        return $this;
    }

    /**
     * The type of scope of the `Team Link`
     *
     * @return string
     */
    public function getScopeType(): string
    {
        return $this->scopeType;
    }

    /**
     * The type of scope of the `Team Link`
     *
     * @param string $scopeType
     *
     * @return Self_
     */
    public function setScopeType(string $scopeType): self
    {
        $this->initialized['scopeType'] = true;
        $this->scopeType = $scopeType;

        return $this;
    }

    /**
     * The id of the `Team Link`
     *
     * @return int
     */
    public function getTeamId(): int
    {
        return $this->teamId;
    }

    /**
     * The id of the `Team Link`
     *
     * @param int $teamId
     *
     * @return Self_
     */
    public function setTeamId(int $teamId): self
    {
        $this->initialized['teamId'] = true;
        $this->teamId = $teamId;

        return $this;
    }
}
