<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class TimesheetApprovalStatus extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The person who is responsible for the current timesheet approval status
     *
     * @var TimesheetApprovalActor
     */
    protected $actor;
    /**
     * The comment to request timesheet approval
     *
     * @var string
     */
    protected $comment;
    /**
     *
     *
     * @var string
     */
    protected $key;
    /**
     * The mandatory seconds spent on this timesheet
     *
     * @var int
     */
    protected $requiredSecondsAtSubmit;
    /**
     * The total seconds spent on this timesheet
     *
     * @var int
     */
    protected $timeSpentSecondsAtSubmit;
    /**
     * The last date time when this timesheet was modified
     *
     * @var string
     */
    protected $updatedAt;

    /**
     * The person who is responsible for the current timesheet approval status
     *
     * @return TimesheetApprovalActor
     */
    public function getActor(): TimesheetApprovalActor
    {
        return $this->actor;
    }

    /**
     * The person who is responsible for the current timesheet approval status
     *
     * @param TimesheetApprovalActor $actor
     *
     * @return Self_
     */
    public function setActor(TimesheetApprovalActor $actor): self
    {
        $this->initialized['actor'] = true;
        $this->actor = $actor;

        return $this;
    }

    /**
     * The comment to request timesheet approval
     *
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * The comment to request timesheet approval
     *
     * @param string $comment
     *
     * @return Self_
     */
    public function setComment(string $comment): self
    {
        $this->initialized['comment'] = true;
        $this->comment = $comment;

        return $this;
    }

    /**
     *
     *
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     *
     *
     * @param string $key
     *
     * @return Self_
     */
    public function setKey(string $key): self
    {
        $this->initialized['key'] = true;
        $this->key = $key;

        return $this;
    }

    /**
     * The mandatory seconds spent on this timesheet
     *
     * @return int
     */
    public function getRequiredSecondsAtSubmit(): int
    {
        return $this->requiredSecondsAtSubmit;
    }

    /**
     * The mandatory seconds spent on this timesheet
     *
     * @param int $requiredSecondsAtSubmit
     *
     * @return Self_
     */
    public function setRequiredSecondsAtSubmit(int $requiredSecondsAtSubmit): self
    {
        $this->initialized['requiredSecondsAtSubmit'] = true;
        $this->requiredSecondsAtSubmit = $requiredSecondsAtSubmit;

        return $this;
    }

    /**
     * The total seconds spent on this timesheet
     *
     * @return int
     */
    public function getTimeSpentSecondsAtSubmit(): int
    {
        return $this->timeSpentSecondsAtSubmit;
    }

    /**
     * The total seconds spent on this timesheet
     *
     * @param int $timeSpentSecondsAtSubmit
     *
     * @return Self_
     */
    public function setTimeSpentSecondsAtSubmit(int $timeSpentSecondsAtSubmit): self
    {
        $this->initialized['timeSpentSecondsAtSubmit'] = true;
        $this->timeSpentSecondsAtSubmit = $timeSpentSecondsAtSubmit;

        return $this;
    }

    /**
     * The last date time when this timesheet was modified
     *
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }

    /**
     * The last date time when this timesheet was modified
     *
     * @param string $updatedAt
     *
     * @return Self_
     */
    public function setUpdatedAt(string $updatedAt): self
    {
        $this->initialized['updatedAt'] = true;
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
