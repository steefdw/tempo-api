<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class PlanApprovalInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * A unique identifier of the user in Jira who will review this `PlanApproval`.
     *
     * @var string
     */
    protected $reviewerId;
    /**
     * The status of the `PlanApproval`.
     *
     * @var string
     */
    protected $status;

    /**
     * A unique identifier of the user in Jira who will review this `PlanApproval`.
     *
     * @return string
     */
    public function getReviewerId(): string
    {
        return $this->reviewerId;
    }

    /**
     * A unique identifier of the user in Jira who will review this `PlanApproval`.
     *
     * @param string $reviewerId
     *
     * @return Self_
     */
    public function setReviewerId(string $reviewerId): self
    {
        $this->initialized['reviewerId'] = true;
        $this->reviewerId = $reviewerId;

        return $this;
    }

    /**
     * The status of the `PlanApproval`.
     *
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * The status of the `PlanApproval`.
     *
     * @param string $status
     *
     * @return Self_
     */
    public function setStatus(string $status): self
    {
        $this->initialized['status'] = true;
        $this->status = $status;

        return $this;
    }
}
