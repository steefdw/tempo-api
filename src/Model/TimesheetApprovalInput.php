<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class TimesheetApprovalInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The comment about timesheet to approval
     *
     * @var string
     */
    protected $comment;
    /**
     * The Account id of the reviewer
     *
     * @var string
     */
    protected $reviewerAccountId;

    /**
     * The comment about timesheet to approval
     *
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * The comment about timesheet to approval
     *
     * @param string $comment
     *
     * @return Self_
     */
    public function setComment(string $comment): self
    {
        $this->initialized['comment'] = true;
        $this->comment = $comment;

        return $this;
    }

    /**
     * The Account id of the reviewer
     *
     * @return string
     */
    public function getReviewerAccountId(): string
    {
        return $this->reviewerAccountId;
    }

    /**
     * The Account id of the reviewer
     *
     * @param string $reviewerAccountId
     *
     * @return Self_
     */
    public function setReviewerAccountId(string $reviewerAccountId): self
    {
        $this->initialized['reviewerAccountId'] = true;
        $this->reviewerAccountId = $reviewerAccountId;

        return $this;
    }
}
