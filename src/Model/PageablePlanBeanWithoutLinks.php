<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class PageablePlanBeanWithoutLinks extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     *
     *
     * @var PageableMetadataBeanWithoutLinks
     */
    protected $metadata;
    /**
     *
     *
     * @var Plan[]
     */
    protected $results;

    /**
     *
     *
     * @return PageableMetadataBeanWithoutLinks
     */
    public function getMetadata(): PageableMetadataBeanWithoutLinks
    {
        return $this->metadata;
    }

    /**
     *
     *
     * @param PageableMetadataBeanWithoutLinks $metadata
     *
     * @return Self_
     */
    public function setMetadata(PageableMetadataBeanWithoutLinks $metadata): self
    {
        $this->initialized['metadata'] = true;
        $this->metadata = $metadata;

        return $this;
    }

    /**
     *
     *
     * @return Plan[]
     */
    public function getResults(): array
    {
        return $this->results;
    }

    /**
     *
     *
     * @param Plan[] $results
     *
     * @return Self_
     */
    public function setResults(array $results): self
    {
        $this->initialized['results'] = true;
        $this->results = $results;

        return $this;
    }
}
