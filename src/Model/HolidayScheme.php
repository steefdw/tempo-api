<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class HolidayScheme extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * Boolean to define if this is the default scheme
     *
     * @var bool
     */
    protected $defaultScheme;
    /**
     * The description of the `Holiday Scheme`
     *
     * @var string
     */
    protected $description;
    /**
     * The id of the `Holiday Scheme`
     *
     * @var int
     */
    protected $id;
    /**
     * The amount of people working under this `Holiday Scheme`
     *
     * @var int
     */
    protected $memberCount;
    /**
     * The name of the `Holiday Scheme`
     *
     * @var string
     */
    protected $name;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;

    /**
     * Boolean to define if this is the default scheme
     *
     * @return bool
     */
    public function getDefaultScheme(): bool
    {
        return $this->defaultScheme;
    }

    /**
     * Boolean to define if this is the default scheme
     *
     * @param bool $defaultScheme
     *
     * @return Self_
     */
    public function setDefaultScheme(bool $defaultScheme): self
    {
        $this->initialized['defaultScheme'] = true;
        $this->defaultScheme = $defaultScheme;

        return $this;
    }

    /**
     * The description of the `Holiday Scheme`
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * The description of the `Holiday Scheme`
     *
     * @param string $description
     *
     * @return Self_
     */
    public function setDescription(string $description): self
    {
        $this->initialized['description'] = true;
        $this->description = $description;

        return $this;
    }

    /**
     * The id of the `Holiday Scheme`
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * The id of the `Holiday Scheme`
     *
     * @param int $id
     *
     * @return Self_
     */
    public function setId(int $id): self
    {
        $this->initialized['id'] = true;
        $this->id = $id;

        return $this;
    }

    /**
     * The amount of people working under this `Holiday Scheme`
     *
     * @return int
     */
    public function getMemberCount(): int
    {
        return $this->memberCount;
    }

    /**
     * The amount of people working under this `Holiday Scheme`
     *
     * @param int $memberCount
     *
     * @return Self_
     */
    public function setMemberCount(int $memberCount): self
    {
        $this->initialized['memberCount'] = true;
        $this->memberCount = $memberCount;

        return $this;
    }

    /**
     * The name of the `Holiday Scheme`
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * The name of the `Holiday Scheme`
     *
     * @param string $name
     *
     * @return Self_
     */
    public function setName(string $name): self
    {
        $this->initialized['name'] = true;
        $this->name = $name;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }
}
