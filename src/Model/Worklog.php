<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class Worklog extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The list of work attribute of this `Worklog`
     *
     * @var SelfListWorkAttributeValue
     */
    protected $attributes;
    /**
     * The author of this `worklog`
     *
     * @var User
     */
    protected $author;
    /**
     * The amount of seconds billable
     *
     * @var int
     */
    protected $billableSeconds;
    /**
     * The date and time when this `Worklog` was created
     *
     * @var string
     */
    protected $createdAt;
    /**
     * The description of the `Worklog`
     *
     * @var string
     */
    protected $description;
    /**
     * The Issue associated to this worklog
     *
     * @var Issue
     */
    protected $issue;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;
    /**
     * The start date of the `Worklog`
     *
     * @var \DateTime
     */
    protected $startDate;
    /**
     * The start time of the `Worklog`
     *
     * @var string
     */
    protected $startTime;
    /**
     * The worklog Id in the tempo app
     *
     * @var int
     */
    protected $tempoWorklogId;
    /**
     * The total amount of time spent in seconds`
     *
     * @var int
     */
    protected $timeSpentSeconds;
    /**
     * The last date and time when this `Worklog` was updated
     *
     * @var string
     */
    protected $updatedAt;

    /**
     * The list of work attribute of this `Worklog`
     *
     * @return SelfListWorkAttributeValue
     */
    public function getAttributes(): SelfListWorkAttributeValue
    {
        return $this->attributes;
    }

    /**
     * The list of work attribute of this `Worklog`
     *
     * @param SelfListWorkAttributeValue $attributes
     *
     * @return Self_
     */
    public function setAttributes(SelfListWorkAttributeValue $attributes): self
    {
        $this->initialized['attributes'] = true;
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * The author of this `worklog`
     *
     * @return User
     */
    public function getAuthor(): User
    {
        return $this->author;
    }

    /**
     * The author of this `worklog`
     *
     * @param User $author
     *
     * @return Self_
     */
    public function setAuthor(User $author): self
    {
        $this->initialized['author'] = true;
        $this->author = $author;

        return $this;
    }

    /**
     * The amount of seconds billable
     *
     * @return int
     */
    public function getBillableSeconds(): int
    {
        return $this->billableSeconds;
    }

    /**
     * The amount of seconds billable
     *
     * @param int $billableSeconds
     *
     * @return Self_
     */
    public function setBillableSeconds(int $billableSeconds): self
    {
        $this->initialized['billableSeconds'] = true;
        $this->billableSeconds = $billableSeconds;

        return $this;
    }

    /**
     * The date and time when this `Worklog` was created
     *
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    /**
     * The date and time when this `Worklog` was created
     *
     * @param string $createdAt
     *
     * @return Self_
     */
    public function setCreatedAt(string $createdAt): self
    {
        $this->initialized['createdAt'] = true;
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * The description of the `Worklog`
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * The description of the `Worklog`
     *
     * @param string $description
     *
     * @return Self_
     */
    public function setDescription(string $description): self
    {
        $this->initialized['description'] = true;
        $this->description = $description;

        return $this;
    }

    /**
     * The Issue associated to this worklog
     *
     * @return Issue
     */
    public function getIssue(): Issue
    {
        return $this->issue;
    }

    /**
     * The Issue associated to this worklog
     *
     * @param Issue $issue
     *
     * @return Self_
     */
    public function setIssue(Issue $issue): self
    {
        $this->initialized['issue'] = true;
        $this->issue = $issue;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }

    /**
     * The start date of the `Worklog`
     *
     * @return \DateTime
     */
    public function getStartDate(): \DateTime
    {
        return $this->startDate;
    }

    /**
     * The start date of the `Worklog`
     *
     * @param \DateTime $startDate
     *
     * @return Self_
     */
    public function setStartDate(\DateTime $startDate): self
    {
        $this->initialized['startDate'] = true;
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * The start time of the `Worklog`
     *
     * @return string
     */
    public function getStartTime(): string
    {
        return $this->startTime;
    }

    /**
     * The start time of the `Worklog`
     *
     * @param string $startTime
     *
     * @return Self_
     */
    public function setStartTime(string $startTime): self
    {
        $this->initialized['startTime'] = true;
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * The worklog Id in the tempo app
     *
     * @return int
     */
    public function getTempoWorklogId(): int
    {
        return $this->tempoWorklogId;
    }

    /**
     * The worklog Id in the tempo app
     *
     * @param int $tempoWorklogId
     *
     * @return Self_
     */
    public function setTempoWorklogId(int $tempoWorklogId): self
    {
        $this->initialized['tempoWorklogId'] = true;
        $this->tempoWorklogId = $tempoWorklogId;

        return $this;
    }

    /**
     * The total amount of time spent in seconds`
     *
     * @return int
     */
    public function getTimeSpentSeconds(): int
    {
        return $this->timeSpentSeconds;
    }

    /**
     * The total amount of time spent in seconds`
     *
     * @param int $timeSpentSeconds
     *
     * @return Self_
     */
    public function setTimeSpentSeconds(int $timeSpentSeconds): self
    {
        $this->initialized['timeSpentSeconds'] = true;
        $this->timeSpentSeconds = $timeSpentSeconds;

        return $this;
    }

    /**
     * The last date and time when this `Worklog` was updated
     *
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }

    /**
     * The last date and time when this `Worklog` was updated
     *
     * @param string $updatedAt
     *
     * @return Self_
     */
    public function setUpdatedAt(string $updatedAt): self
    {
        $this->initialized['updatedAt'] = true;
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
