<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class AccountLinkInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * A set of character that uniquely identify an `Account`
     *
     * @var string
     */
    protected $accountKey;
    /**
     * An optional boolean to indicate if the account link is the default for the project
     *
     * @var bool
     */
    protected $default;
    /**
     * The id of the scope
     *
     * @var int
     */
    protected $scopeId;
    /**
     * The type of scope the `Account` is linked to
     *
     * @var string
     */
    protected $scopeType;

    /**
     * A set of character that uniquely identify an `Account`
     *
     * @return string
     */
    public function getAccountKey(): string
    {
        return $this->accountKey;
    }

    /**
     * A set of character that uniquely identify an `Account`
     *
     * @param string $accountKey
     *
     * @return Self_
     */
    public function setAccountKey(string $accountKey): self
    {
        $this->initialized['accountKey'] = true;
        $this->accountKey = $accountKey;

        return $this;
    }

    /**
     * An optional boolean to indicate if the account link is the default for the project
     *
     * @return bool
     */
    public function getDefault(): bool
    {
        return $this->default;
    }

    /**
     * An optional boolean to indicate if the account link is the default for the project
     *
     * @param bool $default
     *
     * @return Self_
     */
    public function setDefault(bool $default): self
    {
        $this->initialized['default'] = true;
        $this->default = $default;

        return $this;
    }

    /**
     * The id of the scope
     *
     * @return int
     */
    public function getScopeId(): int
    {
        return $this->scopeId;
    }

    /**
     * The id of the scope
     *
     * @param int $scopeId
     *
     * @return Self_
     */
    public function setScopeId(int $scopeId): self
    {
        $this->initialized['scopeId'] = true;
        $this->scopeId = $scopeId;

        return $this;
    }

    /**
     * The type of scope the `Account` is linked to
     *
     * @return string
     */
    public function getScopeType(): string
    {
        return $this->scopeType;
    }

    /**
     * The type of scope the `Account` is linked to
     *
     * @param string $scopeType
     *
     * @return Self_
     */
    public function setScopeType(string $scopeType): self
    {
        $this->initialized['scopeType'] = true;
        $this->scopeType = $scopeType;

        return $this;
    }
}
