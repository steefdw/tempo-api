<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class TeamMembershipInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The account id of the `Member`
     *
     * @var string
     */
    protected $accountId;
    /**
     * The percentage of the `Commitment`
     *
     * @var int
     */
    protected $commitmentPercent;
    /**
     * The start date of the `Membership`
     *
     * @var \DateTime
     */
    protected $from;
    /**
     * The role id of the `Member`
     *
     * @var int
     */
    protected $roleId;
    /**
     * The id of the `Team`
     *
     * @var int
     */
    protected $teamId;
    /**
     * The end date of the `Membership`
     *
     * @var \DateTime
     */
    protected $to;

    /**
     * The account id of the `Member`
     *
     * @return string
     */
    public function getAccountId(): string
    {
        return $this->accountId;
    }

    /**
     * The account id of the `Member`
     *
     * @param string $accountId
     *
     * @return Self_
     */
    public function setAccountId(string $accountId): self
    {
        $this->initialized['accountId'] = true;
        $this->accountId = $accountId;

        return $this;
    }

    /**
     * The percentage of the `Commitment`
     *
     * @return int
     */
    public function getCommitmentPercent(): int
    {
        return $this->commitmentPercent;
    }

    /**
     * The percentage of the `Commitment`
     *
     * @param int $commitmentPercent
     *
     * @return Self_
     */
    public function setCommitmentPercent(int $commitmentPercent): self
    {
        $this->initialized['commitmentPercent'] = true;
        $this->commitmentPercent = $commitmentPercent;

        return $this;
    }

    /**
     * The start date of the `Membership`
     *
     * @return \DateTime
     */
    public function getFrom(): \DateTime
    {
        return $this->from;
    }

    /**
     * The start date of the `Membership`
     *
     * @param \DateTime $from
     *
     * @return Self_
     */
    public function setFrom(\DateTime $from): self
    {
        $this->initialized['from'] = true;
        $this->from = $from;

        return $this;
    }

    /**
     * The role id of the `Member`
     *
     * @return int
     */
    public function getRoleId(): int
    {
        return $this->roleId;
    }

    /**
     * The role id of the `Member`
     *
     * @param int $roleId
     *
     * @return Self_
     */
    public function setRoleId(int $roleId): self
    {
        $this->initialized['roleId'] = true;
        $this->roleId = $roleId;

        return $this;
    }

    /**
     * The id of the `Team`
     *
     * @return int
     */
    public function getTeamId(): int
    {
        return $this->teamId;
    }

    /**
     * The id of the `Team`
     *
     * @param int $teamId
     *
     * @return Self_
     */
    public function setTeamId(int $teamId): self
    {
        $this->initialized['teamId'] = true;
        $this->teamId = $teamId;

        return $this;
    }

    /**
     * The end date of the `Membership`
     *
     * @return \DateTime
     */
    public function getTo(): \DateTime
    {
        return $this->to;
    }

    /**
     * The end date of the `Membership`
     *
     * @param \DateTime $to
     *
     * @return Self_
     */
    public function setTo(\DateTime $to): self
    {
        $this->initialized['to'] = true;
        $this->to = $to;

        return $this;
    }
}
