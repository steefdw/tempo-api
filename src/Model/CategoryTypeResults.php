<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class CategoryTypeResults extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     *
     *
     * @var ResultsMetadata
     */
    protected $metadata;
    /**
     *
     *
     * @var CategoryType[]
     */
    protected $results;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;

    /**
     *
     *
     * @return ResultsMetadata
     */
    public function getMetadata(): ResultsMetadata
    {
        return $this->metadata;
    }

    /**
     *
     *
     * @param ResultsMetadata $metadata
     *
     * @return Self_
     */
    public function setMetadata(ResultsMetadata $metadata): self
    {
        $this->initialized['metadata'] = true;
        $this->metadata = $metadata;

        return $this;
    }

    /**
     *
     *
     * @return CategoryType[]
     */
    public function getResults(): array
    {
        return $this->results;
    }

    /**
     *
     *
     * @param CategoryType[] $results
     *
     * @return Self_
     */
    public function setResults(array $results): self
    {
        $this->initialized['results'] = true;
        $this->results = $results;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }
}
