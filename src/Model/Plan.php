<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class Plan extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * Reference to Assignee of the `Plan`.
     *
     * @var PlanAssignee
     */
    protected $assignee;
    /**
     * Date and time the `Plan` was created`.
     *
     * @var string
     */
    protected $createdAt;
    /**
     * A description containing details of the `Plan`.
     *
     * @var string
     */
    protected $description;
    /**
     * The ending date of the `Plan`.
     *
     * @var string
     */
    protected $endDate;
    /**
     * The id of the `Plan`.
     *
     * @var int
     */
    protected $id;
    /**
     * Boolean value that informs whether non-working days are included in the `Plan`.
     *
     * @var bool
     */
    protected $includeNonWorkingDays;
    /**
     * Reference to an approval linked to the `Plan`.
     *
     * @var PlanApproval
     */
    protected $planApproval;
    /**
     * Reference to `Plan Item` of the `Plan`.
     *
     * @var PlanItem
     */
    protected $planItem;
    /**
     * The amount of seconds that will be invested daily in the `Plan`.
     *
     * @var int
     */
    protected $plannedSecondsPerDay;
    /**
     * Includes the total planned time in scope and how it is broken down by days (and by periods if the `Plan` is a recurring plan).The daily and period breakdown of the plan is omitted by default when fetching multiple plans but can be included by specifying what breakdowns are desired.
     *
     * @var PlannedTime
     */
    protected $plannedTime;
    /**
     * End date for recurrence of the `Plan`.
     *
     * @var string
     */
    protected $recurrenceEndDate;
    /**
     * Recurrence rule of the `Plan`.
     *
     * @var string
     */
    protected $rule;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;
    /**
     * The starting date of the `Plan`.
     *
     * @var string
     */
    protected $startDate;
    /**
     * The start time of the `Plan`
     *
     * @var string
     */
    protected $startTime;
    /**
     * The amount of seconds planned for on the entire `Plan` within the scope of the supplied `from` and `to` dates.
     *
     * @var int
     */
    protected $totalPlannedSecondsInScope;
    /**
     * Date and time the `Plan` was last updated.
     *
     * @var string
     */
    protected $updatedAt;

    /**
     * Reference to Assignee of the `Plan`.
     *
     * @return PlanAssignee
     */
    public function getAssignee(): PlanAssignee
    {
        return $this->assignee;
    }

    /**
     * Reference to Assignee of the `Plan`.
     *
     * @param PlanAssignee $assignee
     *
     * @return Self_
     */
    public function setAssignee(PlanAssignee $assignee): self
    {
        $this->initialized['assignee'] = true;
        $this->assignee = $assignee;

        return $this;
    }

    /**
     * Date and time the `Plan` was created`.
     *
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    /**
     * Date and time the `Plan` was created`.
     *
     * @param string $createdAt
     *
     * @return Self_
     */
    public function setCreatedAt(string $createdAt): self
    {
        $this->initialized['createdAt'] = true;
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * A description containing details of the `Plan`.
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * A description containing details of the `Plan`.
     *
     * @param string $description
     *
     * @return Self_
     */
    public function setDescription(string $description): self
    {
        $this->initialized['description'] = true;
        $this->description = $description;

        return $this;
    }

    /**
     * The ending date of the `Plan`.
     *
     * @return string
     */
    public function getEndDate(): string
    {
        return $this->endDate;
    }

    /**
     * The ending date of the `Plan`.
     *
     * @param string $endDate
     *
     * @return Self_
     */
    public function setEndDate(string $endDate): self
    {
        $this->initialized['endDate'] = true;
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * The id of the `Plan`.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * The id of the `Plan`.
     *
     * @param int $id
     *
     * @return Self_
     */
    public function setId(int $id): self
    {
        $this->initialized['id'] = true;
        $this->id = $id;

        return $this;
    }

    /**
     * Boolean value that informs whether non-working days are included in the `Plan`.
     *
     * @return bool
     */
    public function getIncludeNonWorkingDays(): bool
    {
        return $this->includeNonWorkingDays;
    }

    /**
     * Boolean value that informs whether non-working days are included in the `Plan`.
     *
     * @param bool $includeNonWorkingDays
     *
     * @return Self_
     */
    public function setIncludeNonWorkingDays(bool $includeNonWorkingDays): self
    {
        $this->initialized['includeNonWorkingDays'] = true;
        $this->includeNonWorkingDays = $includeNonWorkingDays;

        return $this;
    }

    /**
     * Reference to an approval linked to the `Plan`.
     *
     * @return PlanApproval
     */
    public function getPlanApproval(): PlanApproval
    {
        return $this->planApproval;
    }

    /**
     * Reference to an approval linked to the `Plan`.
     *
     * @param PlanApproval $planApproval
     *
     * @return Self_
     */
    public function setPlanApproval(PlanApproval $planApproval): self
    {
        $this->initialized['planApproval'] = true;
        $this->planApproval = $planApproval;

        return $this;
    }

    /**
     * Reference to `Plan Item` of the `Plan`.
     *
     * @return PlanItem
     */
    public function getPlanItem(): PlanItem
    {
        return $this->planItem;
    }

    /**
     * Reference to `Plan Item` of the `Plan`.
     *
     * @param PlanItem $planItem
     *
     * @return Self_
     */
    public function setPlanItem(PlanItem $planItem): self
    {
        $this->initialized['planItem'] = true;
        $this->planItem = $planItem;

        return $this;
    }

    /**
     * The amount of seconds that will be invested daily in the `Plan`.
     *
     * @return int
     */
    public function getPlannedSecondsPerDay(): int
    {
        return $this->plannedSecondsPerDay;
    }

    /**
     * The amount of seconds that will be invested daily in the `Plan`.
     *
     * @param int $plannedSecondsPerDay
     *
     * @return Self_
     */
    public function setPlannedSecondsPerDay(int $plannedSecondsPerDay): self
    {
        $this->initialized['plannedSecondsPerDay'] = true;
        $this->plannedSecondsPerDay = $plannedSecondsPerDay;

        return $this;
    }

    /**
     * Includes the total planned time in scope and how it is broken down by days (and by periods if the `Plan` is a recurring plan).The daily and period breakdown of the plan is omitted by default when fetching multiple plans but can be included by specifying what breakdowns are desired.
     *
     * @return PlannedTime
     */
    public function getPlannedTime(): PlannedTime
    {
        return $this->plannedTime;
    }

    /**
     * Includes the total planned time in scope and how it is broken down by days (and by periods if the `Plan` is a recurring plan).The daily and period breakdown of the plan is omitted by default when fetching multiple plans but can be included by specifying what breakdowns are desired.
     *
     * @param PlannedTime $plannedTime
     *
     * @return Self_
     */
    public function setPlannedTime(PlannedTime $plannedTime): self
    {
        $this->initialized['plannedTime'] = true;
        $this->plannedTime = $plannedTime;

        return $this;
    }

    /**
     * End date for recurrence of the `Plan`.
     *
     * @return string
     */
    public function getRecurrenceEndDate(): string
    {
        return $this->recurrenceEndDate;
    }

    /**
     * End date for recurrence of the `Plan`.
     *
     * @param string $recurrenceEndDate
     *
     * @return Self_
     */
    public function setRecurrenceEndDate(string $recurrenceEndDate): self
    {
        $this->initialized['recurrenceEndDate'] = true;
        $this->recurrenceEndDate = $recurrenceEndDate;

        return $this;
    }

    /**
     * Recurrence rule of the `Plan`.
     *
     * @return string
     */
    public function getRule(): string
    {
        return $this->rule;
    }

    /**
     * Recurrence rule of the `Plan`.
     *
     * @param string $rule
     *
     * @return Self_
     */
    public function setRule(string $rule): self
    {
        $this->initialized['rule'] = true;
        $this->rule = $rule;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }

    /**
     * The starting date of the `Plan`.
     *
     * @return string
     */
    public function getStartDate(): string
    {
        return $this->startDate;
    }

    /**
     * The starting date of the `Plan`.
     *
     * @param string $startDate
     *
     * @return Self_
     */
    public function setStartDate(string $startDate): self
    {
        $this->initialized['startDate'] = true;
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * The start time of the `Plan`
     *
     * @return string
     */
    public function getStartTime(): string
    {
        return $this->startTime;
    }

    /**
     * The start time of the `Plan`
     *
     * @param string $startTime
     *
     * @return Self_
     */
    public function setStartTime(string $startTime): self
    {
        $this->initialized['startTime'] = true;
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * The amount of seconds planned for on the entire `Plan` within the scope of the supplied `from` and `to` dates.
     *
     * @return int
     */
    public function getTotalPlannedSecondsInScope(): int
    {
        return $this->totalPlannedSecondsInScope;
    }

    /**
     * The amount of seconds planned for on the entire `Plan` within the scope of the supplied `from` and `to` dates.
     *
     * @param int $totalPlannedSecondsInScope
     *
     * @return Self_
     */
    public function setTotalPlannedSecondsInScope(int $totalPlannedSecondsInScope): self
    {
        $this->initialized['totalPlannedSecondsInScope'] = true;
        $this->totalPlannedSecondsInScope = $totalPlannedSecondsInScope;

        return $this;
    }

    /**
     * Date and time the `Plan` was last updated.
     *
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }

    /**
     * Date and time the `Plan` was last updated.
     *
     * @param string $updatedAt
     *
     * @return Self_
     */
    public function setUpdatedAt(string $updatedAt): self
    {
        $this->initialized['updatedAt'] = true;
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
