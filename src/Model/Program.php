<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class Program extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The id of the `Program`
     *
     * @var int
     */
    protected $id;
    /**
     * The author of this `worklog`
     *
     * @var User
     */
    protected $manager;
    /**
     * The name of the `Program`
     *
     * @var string
     */
    protected $name;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;
    /**
     * A link to another Program
     *
     * @var SelfListTeamRef
     */
    protected $teams;

    /**
     * The id of the `Program`
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * The id of the `Program`
     *
     * @param int $id
     *
     * @return Self_
     */
    public function setId(int $id): self
    {
        $this->initialized['id'] = true;
        $this->id = $id;

        return $this;
    }

    /**
     * The author of this `worklog`
     *
     * @return User
     */
    public function getManager(): User
    {
        return $this->manager;
    }

    /**
     * The author of this `worklog`
     *
     * @param User $manager
     *
     * @return Self_
     */
    public function setManager(User $manager): self
    {
        $this->initialized['manager'] = true;
        $this->manager = $manager;

        return $this;
    }

    /**
     * The name of the `Program`
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * The name of the `Program`
     *
     * @param string $name
     *
     * @return Self_
     */
    public function setName(string $name): self
    {
        $this->initialized['name'] = true;
        $this->name = $name;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }

    /**
     * A link to another Program
     *
     * @return SelfListTeamRef
     */
    public function getTeams(): SelfListTeamRef
    {
        return $this->teams;
    }

    /**
     * A link to another Program
     *
     * @param SelfListTeamRef $teams
     *
     * @return Self_
     */
    public function setTeams(SelfListTeamRef $teams): self
    {
        $this->initialized['teams'] = true;
        $this->teams = $teams;

        return $this;
    }
}
