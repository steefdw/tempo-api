<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class Membership extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     *
     *
     * @var int
     */
    protected $commitmentPercent;
    /**
     *
     *
     * @var string
     */
    protected $from;
    /**
     * The id of the `Membership`
     *
     * @var int
     */
    protected $id;
    /**
     *
     *
     * @var Self_
     */
    protected $member;
    /**
     * The Role of the `Membership`
     *
     * @var RoleReference
     */
    protected $role;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;
    /**
     *
     *
     * @var Self_
     */
    protected $team;
    /**
     *
     *
     * @var string
     */
    protected $to;

    /**
     *
     *
     * @return int
     */
    public function getCommitmentPercent(): int
    {
        return $this->commitmentPercent;
    }

    /**
     *
     *
     * @param int $commitmentPercent
     *
     * @return Self_
     */
    public function setCommitmentPercent(int $commitmentPercent): self
    {
        $this->initialized['commitmentPercent'] = true;
        $this->commitmentPercent = $commitmentPercent;

        return $this;
    }

    /**
     *
     *
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     *
     *
     * @param string $from
     *
     * @return Self_
     */
    public function setFrom(string $from): self
    {
        $this->initialized['from'] = true;
        $this->from = $from;

        return $this;
    }

    /**
     * The id of the `Membership`
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * The id of the `Membership`
     *
     * @param int $id
     *
     * @return Self_
     */
    public function setId(int $id): self
    {
        $this->initialized['id'] = true;
        $this->id = $id;

        return $this;
    }

    /**
     *
     *
     * @return Self_
     */
    public function getMember(): self
    {
        return $this->member;
    }

    /**
     *
     *
     * @param Self_ $member
     *
     * @return Self_
     */
    public function setMember(self $member): self
    {
        $this->initialized['member'] = true;
        $this->member = $member;

        return $this;
    }

    /**
     * The Role of the `Membership`
     *
     * @return RoleReference
     */
    public function getRole(): RoleReference
    {
        return $this->role;
    }

    /**
     * The Role of the `Membership`
     *
     * @param RoleReference $role
     *
     * @return Self_
     */
    public function setRole(RoleReference $role): self
    {
        $this->initialized['role'] = true;
        $this->role = $role;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }

    /**
     *
     *
     * @return Self_
     */
    public function getTeam(): self
    {
        return $this->team;
    }

    /**
     *
     *
     * @param Self_ $team
     *
     * @return Self_
     */
    public function setTeam(self $team): self
    {
        $this->initialized['team'] = true;
        $this->team = $team;

        return $this;
    }

    /**
     *
     *
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     *
     *
     * @param string $to
     *
     * @return Self_
     */
    public function setTo(string $to): self
    {
        $this->initialized['to'] = true;
        $this->to = $to;

        return $this;
    }
}
