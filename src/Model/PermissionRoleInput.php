<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class PermissionRoleInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * A list of access entity ids
     *
     * @var int[]
     */
    protected $accessEntityIds;
    /**
     * The type of the `PermissionRole` which can be `TEAM` or `GLOBAL`. `GLOBAL` permission roles don't have entities.
     *
     * @var string
     */
    protected $accessType;
    /**
     * Name of the permission role
     *
     * @var string
     */
    protected $name;
    /**
     * A list of permission keys
     *
     * @var string[]
     */
    protected $permissionKeys;
    /**
     * A list of permitted account ids
     *
     * @var string[]
     */
    protected $permittedAccountIds;

    /**
     * A list of access entity ids
     *
     * @return int[]
     */
    public function getAccessEntityIds(): array
    {
        return $this->accessEntityIds;
    }

    /**
     * A list of access entity ids
     *
     * @param int[] $accessEntityIds
     *
     * @return Self_
     */
    public function setAccessEntityIds(array $accessEntityIds): self
    {
        $this->initialized['accessEntityIds'] = true;
        $this->accessEntityIds = $accessEntityIds;

        return $this;
    }

    /**
     * The type of the `PermissionRole` which can be `TEAM` or `GLOBAL`. `GLOBAL` permission roles don't have entities.
     *
     * @return string
     */
    public function getAccessType(): string
    {
        return $this->accessType;
    }

    /**
     * The type of the `PermissionRole` which can be `TEAM` or `GLOBAL`. `GLOBAL` permission roles don't have entities.
     *
     * @param string $accessType
     *
     * @return Self_
     */
    public function setAccessType(string $accessType): self
    {
        $this->initialized['accessType'] = true;
        $this->accessType = $accessType;

        return $this;
    }

    /**
     * Name of the permission role
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Name of the permission role
     *
     * @param string $name
     *
     * @return Self_
     */
    public function setName(string $name): self
    {
        $this->initialized['name'] = true;
        $this->name = $name;

        return $this;
    }

    /**
     * A list of permission keys
     *
     * @return string[]
     */
    public function getPermissionKeys(): array
    {
        return $this->permissionKeys;
    }

    /**
     * A list of permission keys
     *
     * @param string[] $permissionKeys
     *
     * @return Self_
     */
    public function setPermissionKeys(array $permissionKeys): self
    {
        $this->initialized['permissionKeys'] = true;
        $this->permissionKeys = $permissionKeys;

        return $this;
    }

    /**
     * A list of permitted account ids
     *
     * @return string[]
     */
    public function getPermittedAccountIds(): array
    {
        return $this->permittedAccountIds;
    }

    /**
     * A list of permitted account ids
     *
     * @param string[] $permittedAccountIds
     *
     * @return Self_
     */
    public function setPermittedAccountIds(array $permittedAccountIds): self
    {
        $this->initialized['permittedAccountIds'] = true;
        $this->permittedAccountIds = $permittedAccountIds;

        return $this;
    }
}
