<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class PlannedTime extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * Planned time of the `Plan` broken down by days.
     *
     * @var PlannedTimeValuesPlanDay
     */
    protected $days;
    /**
     * A reference to the metadata of this object.
     *
     * @var PlanMetadata
     */
    protected $metadata;
    /**
     * Planned time of the `Plan` broken down by periods.
     *
     * @var PlannedTimeValuesPlanPeriod
     */
    protected $periods;

    /**
     * Planned time of the `Plan` broken down by days.
     *
     * @return PlannedTimeValuesPlanDay
     */
    public function getDays(): PlannedTimeValuesPlanDay
    {
        return $this->days;
    }

    /**
     * Planned time of the `Plan` broken down by days.
     *
     * @param PlannedTimeValuesPlanDay $days
     *
     * @return Self_
     */
    public function setDays(PlannedTimeValuesPlanDay $days): self
    {
        $this->initialized['days'] = true;
        $this->days = $days;

        return $this;
    }

    /**
     * A reference to the metadata of this object.
     *
     * @return PlanMetadata
     */
    public function getMetadata(): PlanMetadata
    {
        return $this->metadata;
    }

    /**
     * A reference to the metadata of this object.
     *
     * @param PlanMetadata $metadata
     *
     * @return Self_
     */
    public function setMetadata(PlanMetadata $metadata): self
    {
        $this->initialized['metadata'] = true;
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * Planned time of the `Plan` broken down by periods.
     *
     * @return PlannedTimeValuesPlanPeriod
     */
    public function getPeriods(): PlannedTimeValuesPlanPeriod
    {
        return $this->periods;
    }

    /**
     * Planned time of the `Plan` broken down by periods.
     *
     * @param PlannedTimeValuesPlanPeriod $periods
     *
     * @return Self_
     */
    public function setPeriods(PlannedTimeValuesPlanPeriod $periods): self
    {
        $this->initialized['periods'] = true;
        $this->periods = $periods;

        return $this;
    }
}
