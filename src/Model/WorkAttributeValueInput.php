<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class WorkAttributeValueInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The key of the `WorkAttributeValue`
     *
     * @var string
     */
    protected $key;
    /**
     * The value of the `WorkAttributeValue`
     *
     * @var string
     */
    protected $value;

    /**
     * The key of the `WorkAttributeValue`
     *
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * The key of the `WorkAttributeValue`
     *
     * @param string $key
     *
     * @return Self_
     */
    public function setKey(string $key): self
    {
        $this->initialized['key'] = true;
        $this->key = $key;

        return $this;
    }

    /**
     * The value of the `WorkAttributeValue`
     *
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * The value of the `WorkAttributeValue`
     *
     * @param string $value
     *
     * @return Self_
     */
    public function setValue(string $value): self
    {
        $this->initialized['value'] = true;
        $this->value = $value;

        return $this;
    }
}
