<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class ActiveMemberships extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     *
     *
     * @var Membership
     */
    protected $active;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;

    /**
     *
     *
     * @return Membership
     */
    public function getActive(): Membership
    {
        return $this->active;
    }

    /**
     *
     *
     * @param Membership $active
     *
     * @return Self_
     */
    public function setActive(Membership $active): self
    {
        $this->initialized['active'] = true;
        $this->active = $active;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }
}
