<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class WorkAttributeValuesInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The list of work attribute
     *
     * @var WorkAttributeValueInput[]
     */
    protected $attributeValues;
    /**
     * The worklog id in tempo app`
     *
     * @var int
     */
    protected $tempoWorklogId;

    /**
     * The list of work attribute
     *
     * @return WorkAttributeValueInput[]
     */
    public function getAttributeValues(): array
    {
        return $this->attributeValues;
    }

    /**
     * The list of work attribute
     *
     * @param WorkAttributeValueInput[] $attributeValues
     *
     * @return Self_
     */
    public function setAttributeValues(array $attributeValues): self
    {
        $this->initialized['attributeValues'] = true;
        $this->attributeValues = $attributeValues;

        return $this;
    }

    /**
     * The worklog id in tempo app`
     *
     * @return int
     */
    public function getTempoWorklogId(): int
    {
        return $this->tempoWorklogId;
    }

    /**
     * The worklog id in tempo app`
     *
     * @param int $tempoWorklogId
     *
     * @return Self_
     */
    public function setTempoWorklogId(int $tempoWorklogId): self
    {
        $this->initialized['tempoWorklogId'] = true;
        $this->tempoWorklogId = $tempoWorklogId;

        return $this;
    }
}
