<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class PlannedTimeValuesPlanDay extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The number of results returned.
     *
     * @var int
     */
    protected $count;
    /**
     * List of days/periods that are included in this plan within the scope of the given date range.
     *
     * @var PlanDay[]
     */
    protected $values;

    /**
     * The number of results returned.
     *
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * The number of results returned.
     *
     * @param int $count
     *
     * @return Self_
     */
    public function setCount(int $count): self
    {
        $this->initialized['count'] = true;
        $this->count = $count;

        return $this;
    }

    /**
     * List of days/periods that are included in this plan within the scope of the given date range.
     *
     * @return PlanDay[]
     */
    public function getValues(): array
    {
        return $this->values;
    }

    /**
     * List of days/periods that are included in this plan within the scope of the given date range.
     *
     * @param PlanDay[] $values
     *
     * @return Self_
     */
    public function setValues(array $values): self
    {
        $this->initialized['values'] = true;
        $this->values = $values;

        return $this;
    }
}
