<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class ProgramInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The key of the Manager Account
     *
     * @var string
     */
    protected $managerAccountId;
    /**
     * Name of the Program
     *
     * @var string
     */
    protected $name;
    /**
     * The list of keys of Teams associated to the `Program`
     *
     * @var int[]
     */
    protected $teamIds;

    /**
     * The key of the Manager Account
     *
     * @return string
     */
    public function getManagerAccountId(): string
    {
        return $this->managerAccountId;
    }

    /**
     * The key of the Manager Account
     *
     * @param string $managerAccountId
     *
     * @return Self_
     */
    public function setManagerAccountId(string $managerAccountId): self
    {
        $this->initialized['managerAccountId'] = true;
        $this->managerAccountId = $managerAccountId;

        return $this;
    }

    /**
     * Name of the Program
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Name of the Program
     *
     * @param string $name
     *
     * @return Self_
     */
    public function setName(string $name): self
    {
        $this->initialized['name'] = true;
        $this->name = $name;

        return $this;
    }

    /**
     * The list of keys of Teams associated to the `Program`
     *
     * @return int[]
     */
    public function getTeamIds(): array
    {
        return $this->teamIds;
    }

    /**
     * The list of keys of Teams associated to the `Program`
     *
     * @param int[] $teamIds
     *
     * @return Self_
     */
    public function setTeamIds(array $teamIds): self
    {
        $this->initialized['teamIds'] = true;
        $this->teamIds = $teamIds;

        return $this;
    }
}
