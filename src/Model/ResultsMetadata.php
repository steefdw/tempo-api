<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class ResultsMetadata extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The number of results returned
     *
     * @var int
     */
    protected $count;

    /**
     * The number of results returned
     *
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * The number of results returned
     *
     * @param int $count
     *
     * @return Self_
     */
    public function setCount(int $count): self
    {
        $this->initialized['count'] = true;
        $this->count = $count;

        return $this;
    }
}
