<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class Account extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The category of the `Account`
     *
     * @var Category
     */
    protected $category;
    /**
     * The contact of the `Account`
     *
     * @var UserContact
     */
    protected $contact;
    /**
     *
     *
     * @var Customer
     */
    protected $customer;
    /**
     * Is the `Account` a global account or not
     *
     * @var bool
     */
    protected $global;
    /**
     * The id of the `Account`
     *
     * @var int
     */
    protected $id;
    /**
     * The key of the `Account`
     *
     * @var string
     */
    protected $key;
    /**
     * The lead of the `Account`
     *
     * @var AccountUser
     */
    protected $lead;
    /**
     *
     *
     * @var Self_
     */
    protected $links;
    /**
     * The estimated budget for the month
     *
     * @var int
     */
    protected $monthlyBudget;
    /**
     * The name of the `Account`
     *
     * @var string
     */
    protected $name;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;
    /**
     * The status of the `Account`
     *
     * @var string
     */
    protected $status;

    /**
     * The category of the `Account`
     *
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * The category of the `Account`
     *
     * @param Category $category
     *
     * @return Self_
     */
    public function setCategory(Category $category): self
    {
        $this->initialized['category'] = true;
        $this->category = $category;

        return $this;
    }

    /**
     * The contact of the `Account`
     *
     * @return UserContact
     */
    public function getContact(): UserContact
    {
        return $this->contact;
    }

    /**
     * The contact of the `Account`
     *
     * @param UserContact $contact
     *
     * @return Self_
     */
    public function setContact(UserContact $contact): self
    {
        $this->initialized['contact'] = true;
        $this->contact = $contact;

        return $this;
    }

    /**
     *
     *
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     *
     *
     * @param Customer $customer
     *
     * @return Self_
     */
    public function setCustomer(Customer $customer): self
    {
        $this->initialized['customer'] = true;
        $this->customer = $customer;

        return $this;
    }

    /**
     * Is the `Account` a global account or not
     *
     * @return bool
     */
    public function getGlobal(): bool
    {
        return $this->global;
    }

    /**
     * Is the `Account` a global account or not
     *
     * @param bool $global
     *
     * @return Self_
     */
    public function setGlobal(bool $global): self
    {
        $this->initialized['global'] = true;
        $this->global = $global;

        return $this;
    }

    /**
     * The id of the `Account`
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * The id of the `Account`
     *
     * @param int $id
     *
     * @return Self_
     */
    public function setId(int $id): self
    {
        $this->initialized['id'] = true;
        $this->id = $id;

        return $this;
    }

    /**
     * The key of the `Account`
     *
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * The key of the `Account`
     *
     * @param string $key
     *
     * @return Self_
     */
    public function setKey(string $key): self
    {
        $this->initialized['key'] = true;
        $this->key = $key;

        return $this;
    }

    /**
     * The lead of the `Account`
     *
     * @return AccountUser
     */
    public function getLead(): AccountUser
    {
        return $this->lead;
    }

    /**
     * The lead of the `Account`
     *
     * @param AccountUser $lead
     *
     * @return Self_
     */
    public function setLead(AccountUser $lead): self
    {
        $this->initialized['lead'] = true;
        $this->lead = $lead;

        return $this;
    }

    /**
     *
     *
     * @return Self_
     */
    public function getLinks(): self
    {
        return $this->links;
    }

    /**
     *
     *
     * @param Self_ $links
     *
     * @return Self_
     */
    public function setLinks(self $links): self
    {
        $this->initialized['links'] = true;
        $this->links = $links;

        return $this;
    }

    /**
     * The estimated budget for the month
     *
     * @return int
     */
    public function getMonthlyBudget(): int
    {
        return $this->monthlyBudget;
    }

    /**
     * The estimated budget for the month
     *
     * @param int $monthlyBudget
     *
     * @return Self_
     */
    public function setMonthlyBudget(int $monthlyBudget): self
    {
        $this->initialized['monthlyBudget'] = true;
        $this->monthlyBudget = $monthlyBudget;

        return $this;
    }

    /**
     * The name of the `Account`
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * The name of the `Account`
     *
     * @param string $name
     *
     * @return Self_
     */
    public function setName(string $name): self
    {
        $this->initialized['name'] = true;
        $this->name = $name;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }

    /**
     * The status of the `Account`
     *
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * The status of the `Account`
     *
     * @param string $status
     *
     * @return Self_
     */
    public function setStatus(string $status): self
    {
        $this->initialized['status'] = true;
        $this->status = $status;

        return $this;
    }
}
