<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class PlanApproval extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The author of this `worklog`
     *
     * @var User
     */
    protected $reviewer;
    /**
     * The status of the `PlanApproval`.
     *
     * @var string
     */
    protected $status;

    /**
     * The author of this `worklog`
     *
     * @return User
     */
    public function getReviewer(): User
    {
        return $this->reviewer;
    }

    /**
     * The author of this `worklog`
     *
     * @param User $reviewer
     *
     * @return Self_
     */
    public function setReviewer(User $reviewer): self
    {
        $this->initialized['reviewer'] = true;
        $this->reviewer = $reviewer;

        return $this;
    }

    /**
     * The status of the `PlanApproval`.
     *
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * The status of the `PlanApproval`.
     *
     * @param string $status
     *
     * @return Self_
     */
    public function setStatus(string $status): self
    {
        $this->initialized['status'] = true;
        $this->status = $status;

        return $this;
    }
}
