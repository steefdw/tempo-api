<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class AccountInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * A set of characters that uniquely identify a category
     *
     * @var string
     */
    protected $categoryKey;
    /**
     * 'accountId' of the contact, if the contact is a registered Jira user
     *
     * @var string
     */
    protected $contactAccountId;
    /**
     * A set of characters that uniquely identify a customer
     *
     * @var string
     */
    protected $customerKey;
    /**
     * Name of the contact, if the contact is not a registered Jira user
     *
     * @var string
     */
    protected $externalContactName;
    /**
     *
     *
     * @var bool
     */
    protected $global;
    /**
     * A set of character that uniquely identify the account
     *
     * @var string
     */
    protected $key;
    /**
     * 'accountId' of the lead on the account
     *
     * @var string
     */
    protected $leadAccountId;
    /**
     *
     *
     * @var int
     */
    protected $monthlyBudget;
    /**
     * Name of the account
     *
     * @var string
     */
    protected $name;
    /**
     * The status of the account
     *
     * @var string
     */
    protected $status;

    /**
     * A set of characters that uniquely identify a category
     *
     * @return string
     */
    public function getCategoryKey(): string
    {
        return $this->categoryKey;
    }

    /**
     * A set of characters that uniquely identify a category
     *
     * @param string $categoryKey
     *
     * @return Self_
     */
    public function setCategoryKey(string $categoryKey): self
    {
        $this->initialized['categoryKey'] = true;
        $this->categoryKey = $categoryKey;

        return $this;
    }

    /**
     * 'accountId' of the contact, if the contact is a registered Jira user
     *
     * @return string
     */
    public function getContactAccountId(): string
    {
        return $this->contactAccountId;
    }

    /**
     * 'accountId' of the contact, if the contact is a registered Jira user
     *
     * @param string $contactAccountId
     *
     * @return Self_
     */
    public function setContactAccountId(string $contactAccountId): self
    {
        $this->initialized['contactAccountId'] = true;
        $this->contactAccountId = $contactAccountId;

        return $this;
    }

    /**
     * A set of characters that uniquely identify a customer
     *
     * @return string
     */
    public function getCustomerKey(): string
    {
        return $this->customerKey;
    }

    /**
     * A set of characters that uniquely identify a customer
     *
     * @param string $customerKey
     *
     * @return Self_
     */
    public function setCustomerKey(string $customerKey): self
    {
        $this->initialized['customerKey'] = true;
        $this->customerKey = $customerKey;

        return $this;
    }

    /**
     * Name of the contact, if the contact is not a registered Jira user
     *
     * @return string
     */
    public function getExternalContactName(): string
    {
        return $this->externalContactName;
    }

    /**
     * Name of the contact, if the contact is not a registered Jira user
     *
     * @param string $externalContactName
     *
     * @return Self_
     */
    public function setExternalContactName(string $externalContactName): self
    {
        $this->initialized['externalContactName'] = true;
        $this->externalContactName = $externalContactName;

        return $this;
    }

    /**
     *
     *
     * @return bool
     */
    public function getGlobal(): bool
    {
        return $this->global;
    }

    /**
     *
     *
     * @param bool $global
     *
     * @return Self_
     */
    public function setGlobal(bool $global): self
    {
        $this->initialized['global'] = true;
        $this->global = $global;

        return $this;
    }

    /**
     * A set of character that uniquely identify the account
     *
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * A set of character that uniquely identify the account
     *
     * @param string $key
     *
     * @return Self_
     */
    public function setKey(string $key): self
    {
        $this->initialized['key'] = true;
        $this->key = $key;

        return $this;
    }

    /**
     * 'accountId' of the lead on the account
     *
     * @return string
     */
    public function getLeadAccountId(): string
    {
        return $this->leadAccountId;
    }

    /**
     * 'accountId' of the lead on the account
     *
     * @param string $leadAccountId
     *
     * @return Self_
     */
    public function setLeadAccountId(string $leadAccountId): self
    {
        $this->initialized['leadAccountId'] = true;
        $this->leadAccountId = $leadAccountId;

        return $this;
    }

    /**
     *
     *
     * @return int
     */
    public function getMonthlyBudget(): int
    {
        return $this->monthlyBudget;
    }

    /**
     *
     *
     * @param int $monthlyBudget
     *
     * @return Self_
     */
    public function setMonthlyBudget(int $monthlyBudget): self
    {
        $this->initialized['monthlyBudget'] = true;
        $this->monthlyBudget = $monthlyBudget;

        return $this;
    }

    /**
     * Name of the account
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Name of the account
     *
     * @param string $name
     *
     * @return Self_
     */
    public function setName(string $name): self
    {
        $this->initialized['name'] = true;
        $this->name = $name;

        return $this;
    }

    /**
     * The status of the account
     *
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * The status of the account
     *
     * @param string $status
     *
     * @return Self_
     */
    public function setStatus(string $status): self
    {
        $this->initialized['status'] = true;
        $this->status = $status;

        return $this;
    }
}
