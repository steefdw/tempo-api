<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class TimesheetApprovalPeriodsBean extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     *
     *
     * @var TimesheetApprovalPeriod[]
     */
    protected $periods;

    /**
     *
     *
     * @return TimesheetApprovalPeriod[]
     */
    public function getPeriods(): array
    {
        return $this->periods;
    }

    /**
     *
     *
     * @param TimesheetApprovalPeriod[] $periods
     *
     * @return Self_
     */
    public function setPeriods(array $periods): self
    {
        $this->initialized['periods'] = true;
        $this->periods = $periods;

        return $this;
    }
}
