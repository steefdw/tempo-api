<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class AccountLinkScope extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The id of the `AccountLinkScope`
     *
     * @var int
     */
    protected $id;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;
    /**
     * The type of the `AccountLinkScope`
     *
     * @var string
     */
    protected $type;

    /**
     * The id of the `AccountLinkScope`
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * The id of the `AccountLinkScope`
     *
     * @param int $id
     *
     * @return Self_
     */
    public function setId(int $id): self
    {
        $this->initialized['id'] = true;
        $this->id = $id;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }

    /**
     * The type of the `AccountLinkScope`
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * The type of the `AccountLinkScope`
     *
     * @param string $type
     *
     * @return Self_
     */
    public function setType(string $type): self
    {
        $this->initialized['type'] = true;
        $this->type = $type;

        return $this;
    }
}
