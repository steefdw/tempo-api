<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class PermissionRole extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * List of permission role access entity
     *
     * @var Entity[]
     */
    protected $accessEntities;
    /**
     * The type of the permission role which can be `TEAM` or `Global`. `GLOBAL` permission roles don't have entities.
     *
     * @var string
     */
    protected $accessType;
    /**
     * Editable roles are manually created with updatable members.
     *
     * @var bool
     */
    protected $editable;
    /**
     * The id of the permission role
     *
     * @var int
     */
    protected $id;
    /**
     * The name of the permission role
     *
     * @var string
     */
    protected $name;
    /**
     * List of the roles permissions
     *
     * @var Permission[]
     */
    protected $permissions;
    /**
     * List of users
     *
     * @var User[]
     */
    protected $permittedUsers;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;

    /**
     * List of permission role access entity
     *
     * @return Entity[]
     */
    public function getAccessEntities(): array
    {
        return $this->accessEntities;
    }

    /**
     * List of permission role access entity
     *
     * @param Entity[] $accessEntities
     *
     * @return Self_
     */
    public function setAccessEntities(array $accessEntities): self
    {
        $this->initialized['accessEntities'] = true;
        $this->accessEntities = $accessEntities;

        return $this;
    }

    /**
     * The type of the permission role which can be `TEAM` or `Global`. `GLOBAL` permission roles don't have entities.
     *
     * @return string
     */
    public function getAccessType(): string
    {
        return $this->accessType;
    }

    /**
     * The type of the permission role which can be `TEAM` or `Global`. `GLOBAL` permission roles don't have entities.
     *
     * @param string $accessType
     *
     * @return Self_
     */
    public function setAccessType(string $accessType): self
    {
        $this->initialized['accessType'] = true;
        $this->accessType = $accessType;

        return $this;
    }

    /**
     * Editable roles are manually created with updatable members.
     *
     * @return bool
     */
    public function getEditable(): bool
    {
        return $this->editable;
    }

    /**
     * Editable roles are manually created with updatable members.
     *
     * @param bool $editable
     *
     * @return Self_
     */
    public function setEditable(bool $editable): self
    {
        $this->initialized['editable'] = true;
        $this->editable = $editable;

        return $this;
    }

    /**
     * The id of the permission role
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * The id of the permission role
     *
     * @param int $id
     *
     * @return Self_
     */
    public function setId(int $id): self
    {
        $this->initialized['id'] = true;
        $this->id = $id;

        return $this;
    }

    /**
     * The name of the permission role
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * The name of the permission role
     *
     * @param string $name
     *
     * @return Self_
     */
    public function setName(string $name): self
    {
        $this->initialized['name'] = true;
        $this->name = $name;

        return $this;
    }

    /**
     * List of the roles permissions
     *
     * @return Permission[]
     */
    public function getPermissions(): array
    {
        return $this->permissions;
    }

    /**
     * List of the roles permissions
     *
     * @param Permission[] $permissions
     *
     * @return Self_
     */
    public function setPermissions(array $permissions): self
    {
        $this->initialized['permissions'] = true;
        $this->permissions = $permissions;

        return $this;
    }

    /**
     * List of users
     *
     * @return User[]
     */
    public function getPermittedUsers(): array
    {
        return $this->permittedUsers;
    }

    /**
     * List of users
     *
     * @param User[] $permittedUsers
     *
     * @return Self_
     */
    public function setPermittedUsers(array $permittedUsers): self
    {
        $this->initialized['permittedUsers'] = true;
        $this->permittedUsers = $permittedUsers;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }
}
