<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class HolidaySchemeMembersInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * A list of account ids
     *
     * @var string[]
     */
    protected $accountIds;

    /**
     * A list of account ids
     *
     * @return string[]
     */
    public function getAccountIds(): array
    {
        return $this->accountIds;
    }

    /**
     * A list of account ids
     *
     * @param string[] $accountIds
     *
     * @return Self_
     */
    public function setAccountIds(array $accountIds): self
    {
        $this->initialized['accountIds'] = true;
        $this->accountIds = $accountIds;

        return $this;
    }
}
