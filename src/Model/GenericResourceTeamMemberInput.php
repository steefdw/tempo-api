<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class GenericResourceTeamMemberInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The id of the `Generic Resource` itself
     *
     * @var int
     */
    protected $genericResourceId;

    /**
     * The id of the `Generic Resource` itself
     *
     * @return int
     */
    public function getGenericResourceId(): int
    {
        return $this->genericResourceId;
    }

    /**
     * The id of the `Generic Resource` itself
     *
     * @param int $genericResourceId
     *
     * @return Self_
     */
    public function setGenericResourceId(int $genericResourceId): self
    {
        $this->initialized['genericResourceId'] = true;
        $this->genericResourceId = $genericResourceId;

        return $this;
    }
}
