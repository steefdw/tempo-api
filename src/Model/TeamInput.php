<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class TeamInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * Determine if this is an administrative team
     *
     * @var bool
     */
    protected $administrative = false;
    /**
     * The id of Lead Account related to the `Team`
     *
     * @var string
     */
    protected $leadAccountId;
    /**
     * The `Team` name
     *
     * @var string
     */
    protected $name;
    /**
     * The id of program related to the `Team`
     *
     * @var int
     */
    protected $programId;
    /**
     * Determine if this entity could be public
     *
     * @var bool
     */
    protected $public = true;
    /**
     * The summary of the `Team`
     *
     * @var string
     */
    protected $summary;

    /**
     * Determine if this is an administrative team
     *
     * @return bool
     */
    public function getAdministrative(): bool
    {
        return $this->administrative;
    }

    /**
     * Determine if this is an administrative team
     *
     * @param bool $administrative
     *
     * @return Self_
     */
    public function setAdministrative(bool $administrative): self
    {
        $this->initialized['administrative'] = true;
        $this->administrative = $administrative;

        return $this;
    }

    /**
     * The id of Lead Account related to the `Team`
     *
     * @return string
     */
    public function getLeadAccountId(): string
    {
        return $this->leadAccountId;
    }

    /**
     * The id of Lead Account related to the `Team`
     *
     * @param string $leadAccountId
     *
     * @return Self_
     */
    public function setLeadAccountId(string $leadAccountId): self
    {
        $this->initialized['leadAccountId'] = true;
        $this->leadAccountId = $leadAccountId;

        return $this;
    }

    /**
     * The `Team` name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * The `Team` name
     *
     * @param string $name
     *
     * @return Self_
     */
    public function setName(string $name): self
    {
        $this->initialized['name'] = true;
        $this->name = $name;

        return $this;
    }

    /**
     * The id of program related to the `Team`
     *
     * @return int
     */
    public function getProgramId(): int
    {
        return $this->programId;
    }

    /**
     * The id of program related to the `Team`
     *
     * @param int $programId
     *
     * @return Self_
     */
    public function setProgramId(int $programId): self
    {
        $this->initialized['programId'] = true;
        $this->programId = $programId;

        return $this;
    }

    /**
     * Determine if this entity could be public
     *
     * @return bool
     */
    public function getPublic(): bool
    {
        return $this->public;
    }

    /**
     * Determine if this entity could be public
     *
     * @param bool $public
     *
     * @return Self_
     */
    public function setPublic(bool $public): self
    {
        $this->initialized['public'] = true;
        $this->public = $public;

        return $this;
    }

    /**
     * The summary of the `Team`
     *
     * @return string
     */
    public function getSummary(): string
    {
        return $this->summary;
    }

    /**
     * The summary of the `Team`
     *
     * @param string $summary
     *
     * @return Self_
     */
    public function setSummary(string $summary): self
    {
        $this->initialized['summary'] = true;
        $this->summary = $summary;

        return $this;
    }
}
