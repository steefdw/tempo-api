<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class WorklogIdMapper extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * A unique identifier of the Worklog in Jira
     *
     * @var int
     */
    protected $jiraWorklogId;
    /**
     * A unique identifier of the Worklog in Tempo
     *
     * @var int
     */
    protected $tempoWorklogId;

    /**
     * A unique identifier of the Worklog in Jira
     *
     * @return int
     */
    public function getJiraWorklogId(): int
    {
        return $this->jiraWorklogId;
    }

    /**
     * A unique identifier of the Worklog in Jira
     *
     * @param int $jiraWorklogId
     *
     * @return Self_
     */
    public function setJiraWorklogId(int $jiraWorklogId): self
    {
        $this->initialized['jiraWorklogId'] = true;
        $this->jiraWorklogId = $jiraWorklogId;

        return $this;
    }

    /**
     * A unique identifier of the Worklog in Tempo
     *
     * @return int
     */
    public function getTempoWorklogId(): int
    {
        return $this->tempoWorklogId;
    }

    /**
     * A unique identifier of the Worklog in Tempo
     *
     * @param int $tempoWorklogId
     *
     * @return Self_
     */
    public function setTempoWorklogId(int $tempoWorklogId): self
    {
        $this->initialized['tempoWorklogId'] = true;
        $this->tempoWorklogId = $tempoWorklogId;

        return $this;
    }
}
