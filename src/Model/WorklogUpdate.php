<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class WorklogUpdate extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The list of work attribute of this `Worklog`
     *
     * @var WorkAttributeValueInput[]
     */
    protected $attributes;
    /**
     * The Account id of the user author
     *
     * @var string
     */
    protected $authorAccountId;
    /**
     * The amount of seconds billable
     *
     * @var int
     */
    protected $billableSeconds;
    /**
     * The description of the `Worklog`
     *
     * @var string
     */
    protected $description;
    /**
     * The total amount of estimated remaining seconds`
     *
     * @var int
     */
    protected $remainingEstimateSeconds;
    /**
     * The start date of the `Worklog`
     *
     * @var \DateTime
     */
    protected $startDate;
    /**
     * The start time of the `Worklog`
     *
     * @var string
     */
    protected $startTime;
    /**
     * The total amount of time spent in seconds`
     *
     * @var int
     */
    protected $timeSpentSeconds;

    /**
     * The list of work attribute of this `Worklog`
     *
     * @return WorkAttributeValueInput[]
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * The list of work attribute of this `Worklog`
     *
     * @param WorkAttributeValueInput[] $attributes
     *
     * @return Self_
     */
    public function setAttributes(array $attributes): self
    {
        $this->initialized['attributes'] = true;
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * The Account id of the user author
     *
     * @return string
     */
    public function getAuthorAccountId(): string
    {
        return $this->authorAccountId;
    }

    /**
     * The Account id of the user author
     *
     * @param string $authorAccountId
     *
     * @return Self_
     */
    public function setAuthorAccountId(string $authorAccountId): self
    {
        $this->initialized['authorAccountId'] = true;
        $this->authorAccountId = $authorAccountId;

        return $this;
    }

    /**
     * The amount of seconds billable
     *
     * @return int
     */
    public function getBillableSeconds(): int
    {
        return $this->billableSeconds;
    }

    /**
     * The amount of seconds billable
     *
     * @param int $billableSeconds
     *
     * @return Self_
     */
    public function setBillableSeconds(int $billableSeconds): self
    {
        $this->initialized['billableSeconds'] = true;
        $this->billableSeconds = $billableSeconds;

        return $this;
    }

    /**
     * The description of the `Worklog`
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * The description of the `Worklog`
     *
     * @param string $description
     *
     * @return Self_
     */
    public function setDescription(string $description): self
    {
        $this->initialized['description'] = true;
        $this->description = $description;

        return $this;
    }

    /**
     * The total amount of estimated remaining seconds`
     *
     * @return int
     */
    public function getRemainingEstimateSeconds(): int
    {
        return $this->remainingEstimateSeconds;
    }

    /**
     * The total amount of estimated remaining seconds`
     *
     * @param int $remainingEstimateSeconds
     *
     * @return Self_
     */
    public function setRemainingEstimateSeconds(int $remainingEstimateSeconds): self
    {
        $this->initialized['remainingEstimateSeconds'] = true;
        $this->remainingEstimateSeconds = $remainingEstimateSeconds;

        return $this;
    }

    /**
     * The start date of the `Worklog`
     *
     * @return \DateTime
     */
    public function getStartDate(): \DateTime
    {
        return $this->startDate;
    }

    /**
     * The start date of the `Worklog`
     *
     * @param \DateTime $startDate
     *
     * @return Self_
     */
    public function setStartDate(\DateTime $startDate): self
    {
        $this->initialized['startDate'] = true;
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * The start time of the `Worklog`
     *
     * @return string
     */
    public function getStartTime(): string
    {
        return $this->startTime;
    }

    /**
     * The start time of the `Worklog`
     *
     * @param string $startTime
     *
     * @return Self_
     */
    public function setStartTime(string $startTime): self
    {
        $this->initialized['startTime'] = true;
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * The total amount of time spent in seconds`
     *
     * @return int
     */
    public function getTimeSpentSeconds(): int
    {
        return $this->timeSpentSeconds;
    }

    /**
     * The total amount of time spent in seconds`
     *
     * @param int $timeSpentSeconds
     *
     * @return Self_
     */
    public function setTimeSpentSeconds(int $timeSpentSeconds): self
    {
        $this->initialized['timeSpentSeconds'] = true;
        $this->timeSpentSeconds = $timeSpentSeconds;

        return $this;
    }
}
