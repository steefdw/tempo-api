<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class PlanSearchInput extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * Ids of the user assignees you want to search plans for.
     *
     * @var string[]
     */
    protected $accountIds;
    /**
     * Types of the assignees you want to search plans for.
     *
     * @var string[]
     */
    protected $assigneeTypes;
    /**
     * Retrieve plans starting with this date.
     *
     * @var string
     */
    protected $from;
    /**
     * Ids of the generic resources you want to search plans for.
     *
     * @var int[]
     */
    protected $genericResourceIds;
    /**
     * Maximum number of results on each page.
     *
     * @var int
     */
    protected $limit;
    /**
     * Number of skipped results.
     *
     * @var int
     */
    protected $offset;
    /**
     * Ids of the plans you want to search for.
     *
     * @var int[]
     */
    protected $planIds;
    /**
     * Ids of the items you want to search plans for.
     *
     * @var int[]
     */
    protected $planItemIds;
    /**
     * Types of the items you want to search plans for.
     *
     * @var string[]
     */
    protected $planItemTypes;
    /**
     * Defines how detailed you would like to see the breakdown of the planned time for each `Plan`.
     *
     * @var string[]
     */
    protected $plannedTimeBreakdown;
    /**
     * Retrieve plans that ends up to and including this date.
     *
     * @var string
     */
    protected $to;
    /**
     * Retrieve plans that have been updated from this date.
     *
     * @var string
     */
    protected $updatedFrom;

    /**
     * Ids of the user assignees you want to search plans for.
     *
     * @return string[]
     */
    public function getAccountIds(): array
    {
        return $this->accountIds;
    }

    /**
     * Ids of the user assignees you want to search plans for.
     *
     * @param string[] $accountIds
     *
     * @return Self_
     */
    public function setAccountIds(array $accountIds): self
    {
        $this->initialized['accountIds'] = true;
        $this->accountIds = $accountIds;

        return $this;
    }

    /**
     * Types of the assignees you want to search plans for.
     *
     * @return string[]
     */
    public function getAssigneeTypes(): array
    {
        return $this->assigneeTypes;
    }

    /**
     * Types of the assignees you want to search plans for.
     *
     * @param string[] $assigneeTypes
     *
     * @return Self_
     */
    public function setAssigneeTypes(array $assigneeTypes): self
    {
        $this->initialized['assigneeTypes'] = true;
        $this->assigneeTypes = $assigneeTypes;

        return $this;
    }

    /**
     * Retrieve plans starting with this date.
     *
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * Retrieve plans starting with this date.
     *
     * @param string $from
     *
     * @return Self_
     */
    public function setFrom(string $from): self
    {
        $this->initialized['from'] = true;
        $this->from = $from;

        return $this;
    }

    /**
     * Ids of the generic resources you want to search plans for.
     *
     * @return int[]
     */
    public function getGenericResourceIds(): array
    {
        return $this->genericResourceIds;
    }

    /**
     * Ids of the generic resources you want to search plans for.
     *
     * @param int[] $genericResourceIds
     *
     * @return Self_
     */
    public function setGenericResourceIds(array $genericResourceIds): self
    {
        $this->initialized['genericResourceIds'] = true;
        $this->genericResourceIds = $genericResourceIds;

        return $this;
    }

    /**
     * Maximum number of results on each page.
     *
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * Maximum number of results on each page.
     *
     * @param int $limit
     *
     * @return Self_
     */
    public function setLimit(int $limit): self
    {
        $this->initialized['limit'] = true;
        $this->limit = $limit;

        return $this;
    }

    /**
     * Number of skipped results.
     *
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * Number of skipped results.
     *
     * @param int $offset
     *
     * @return Self_
     */
    public function setOffset(int $offset): self
    {
        $this->initialized['offset'] = true;
        $this->offset = $offset;

        return $this;
    }

    /**
     * Ids of the plans you want to search for.
     *
     * @return int[]
     */
    public function getPlanIds(): array
    {
        return $this->planIds;
    }

    /**
     * Ids of the plans you want to search for.
     *
     * @param int[] $planIds
     *
     * @return Self_
     */
    public function setPlanIds(array $planIds): self
    {
        $this->initialized['planIds'] = true;
        $this->planIds = $planIds;

        return $this;
    }

    /**
     * Ids of the items you want to search plans for.
     *
     * @return int[]
     */
    public function getPlanItemIds(): array
    {
        return $this->planItemIds;
    }

    /**
     * Ids of the items you want to search plans for.
     *
     * @param int[] $planItemIds
     *
     * @return Self_
     */
    public function setPlanItemIds(array $planItemIds): self
    {
        $this->initialized['planItemIds'] = true;
        $this->planItemIds = $planItemIds;

        return $this;
    }

    /**
     * Types of the items you want to search plans for.
     *
     * @return string[]
     */
    public function getPlanItemTypes(): array
    {
        return $this->planItemTypes;
    }

    /**
     * Types of the items you want to search plans for.
     *
     * @param string[] $planItemTypes
     *
     * @return Self_
     */
    public function setPlanItemTypes(array $planItemTypes): self
    {
        $this->initialized['planItemTypes'] = true;
        $this->planItemTypes = $planItemTypes;

        return $this;
    }

    /**
     * Defines how detailed you would like to see the breakdown of the planned time for each `Plan`.
     *
     * @return string[]
     */
    public function getPlannedTimeBreakdown(): array
    {
        return $this->plannedTimeBreakdown;
    }

    /**
     * Defines how detailed you would like to see the breakdown of the planned time for each `Plan`.
     *
     * @param string[] $plannedTimeBreakdown
     *
     * @return Self_
     */
    public function setPlannedTimeBreakdown(array $plannedTimeBreakdown): self
    {
        $this->initialized['plannedTimeBreakdown'] = true;
        $this->plannedTimeBreakdown = $plannedTimeBreakdown;

        return $this;
    }

    /**
     * Retrieve plans that ends up to and including this date.
     *
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * Retrieve plans that ends up to and including this date.
     *
     * @param string $to
     *
     * @return Self_
     */
    public function setTo(string $to): self
    {
        $this->initialized['to'] = true;
        $this->to = $to;

        return $this;
    }

    /**
     * Retrieve plans that have been updated from this date.
     *
     * @return string
     */
    public function getUpdatedFrom(): string
    {
        return $this->updatedFrom;
    }

    /**
     * Retrieve plans that have been updated from this date.
     *
     * @param string $updatedFrom
     *
     * @return Self_
     */
    public function setUpdatedFrom(string $updatedFrom): self
    {
        $this->initialized['updatedFrom'] = true;
        $this->updatedFrom = $updatedFrom;

        return $this;
    }
}
