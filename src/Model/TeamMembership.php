<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class TeamMembership extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The commitment of the `Membership`
     *
     * @var int
     */
    protected $commitmentPercent;
    /**
     * The start date of the `Membership`
     *
     * @var \DateTime
     */
    protected $from;
    /**
     * The id of the `Membership`
     *
     * @var int
     */
    protected $id;
    /**
     * The user member of the `Membership`
     *
     * @var TeamMember
     */
    protected $member;
    /**
     * The Role of the `Membership`
     *
     * @var RoleReference
     */
    protected $role;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;
    /**
     * The Team of the `Membership`
     *
     * @var TeamRef
     */
    protected $team;
    /**
     * The end date of the `Membership`
     *
     * @var \DateTime
     */
    protected $to;

    /**
     * The commitment of the `Membership`
     *
     * @return int
     */
    public function getCommitmentPercent(): int
    {
        return $this->commitmentPercent;
    }

    /**
     * The commitment of the `Membership`
     *
     * @param int $commitmentPercent
     *
     * @return Self_
     */
    public function setCommitmentPercent(int $commitmentPercent): self
    {
        $this->initialized['commitmentPercent'] = true;
        $this->commitmentPercent = $commitmentPercent;

        return $this;
    }

    /**
     * The start date of the `Membership`
     *
     * @return \DateTime
     */
    public function getFrom(): \DateTime
    {
        return $this->from;
    }

    /**
     * The start date of the `Membership`
     *
     * @param \DateTime $from
     *
     * @return Self_
     */
    public function setFrom(\DateTime $from): self
    {
        $this->initialized['from'] = true;
        $this->from = $from;

        return $this;
    }

    /**
     * The id of the `Membership`
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * The id of the `Membership`
     *
     * @param int $id
     *
     * @return Self_
     */
    public function setId(int $id): self
    {
        $this->initialized['id'] = true;
        $this->id = $id;

        return $this;
    }

    /**
     * The user member of the `Membership`
     *
     * @return TeamMember
     */
    public function getMember(): TeamMember
    {
        return $this->member;
    }

    /**
     * The user member of the `Membership`
     *
     * @param TeamMember $member
     *
     * @return Self_
     */
    public function setMember(TeamMember $member): self
    {
        $this->initialized['member'] = true;
        $this->member = $member;

        return $this;
    }

    /**
     * The Role of the `Membership`
     *
     * @return RoleReference
     */
    public function getRole(): RoleReference
    {
        return $this->role;
    }

    /**
     * The Role of the `Membership`
     *
     * @param RoleReference $role
     *
     * @return Self_
     */
    public function setRole(RoleReference $role): self
    {
        $this->initialized['role'] = true;
        $this->role = $role;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }

    /**
     * The Team of the `Membership`
     *
     * @return TeamRef
     */
    public function getTeam(): TeamRef
    {
        return $this->team;
    }

    /**
     * The Team of the `Membership`
     *
     * @param TeamRef $team
     *
     * @return Self_
     */
    public function setTeam(TeamRef $team): self
    {
        $this->initialized['team'] = true;
        $this->team = $team;

        return $this;
    }

    /**
     * The end date of the `Membership`
     *
     * @return \DateTime
     */
    public function getTo(): \DateTime
    {
        return $this->to;
    }

    /**
     * The end date of the `Membership`
     *
     * @param \DateTime $to
     *
     * @return Self_
     */
    public function setTo(\DateTime $to): self
    {
        $this->initialized['to'] = true;
        $this->to = $to;

        return $this;
    }
}
