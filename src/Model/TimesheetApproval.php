<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class TimesheetApproval extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The Available actions to apply on this timesheet
     *
     * @var TimesheetApprovalAvailableActions
     */
    protected $actions;
    /**
     *
     *
     * @var TimesheetApprovalPeriod
     */
    protected $period;
    /**
     * The mandatory seconds spent on this timesheet
     *
     * @var int
     */
    protected $requiredSeconds;
    /**
     * The reviewer of this timesheet
     *
     * @var TimesheetApprovalReviewer
     */
    protected $reviewer;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;
    /**
     * The current status of this timesheet
     *
     * @var TimesheetApprovalStatus
     */
    protected $status;
    /**
     * The total seconds spent on this timesheet
     *
     * @var int
     */
    protected $timeSpentSeconds;
    /**
     * The user owner of this timesheet
     *
     * @var TimesheetApprovalUser
     */
    protected $user;
    /**
     * A permanent link to this resource
     *
     * @var SelfApiBean
     */
    protected $worklogs;

    /**
     * The Available actions to apply on this timesheet
     *
     * @return TimesheetApprovalAvailableActions
     */
    public function getActions(): TimesheetApprovalAvailableActions
    {
        return $this->actions;
    }

    /**
     * The Available actions to apply on this timesheet
     *
     * @param TimesheetApprovalAvailableActions $actions
     *
     * @return Self_
     */
    public function setActions(TimesheetApprovalAvailableActions $actions): self
    {
        $this->initialized['actions'] = true;
        $this->actions = $actions;

        return $this;
    }

    /**
     *
     *
     * @return TimesheetApprovalPeriod
     */
    public function getPeriod(): TimesheetApprovalPeriod
    {
        return $this->period;
    }

    /**
     *
     *
     * @param TimesheetApprovalPeriod $period
     *
     * @return Self_
     */
    public function setPeriod(TimesheetApprovalPeriod $period): self
    {
        $this->initialized['period'] = true;
        $this->period = $period;

        return $this;
    }

    /**
     * The mandatory seconds spent on this timesheet
     *
     * @return int
     */
    public function getRequiredSeconds(): int
    {
        return $this->requiredSeconds;
    }

    /**
     * The mandatory seconds spent on this timesheet
     *
     * @param int $requiredSeconds
     *
     * @return Self_
     */
    public function setRequiredSeconds(int $requiredSeconds): self
    {
        $this->initialized['requiredSeconds'] = true;
        $this->requiredSeconds = $requiredSeconds;

        return $this;
    }

    /**
     * The reviewer of this timesheet
     *
     * @return TimesheetApprovalReviewer
     */
    public function getReviewer(): TimesheetApprovalReviewer
    {
        return $this->reviewer;
    }

    /**
     * The reviewer of this timesheet
     *
     * @param TimesheetApprovalReviewer $reviewer
     *
     * @return Self_
     */
    public function setReviewer(TimesheetApprovalReviewer $reviewer): self
    {
        $this->initialized['reviewer'] = true;
        $this->reviewer = $reviewer;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }

    /**
     * The current status of this timesheet
     *
     * @return TimesheetApprovalStatus
     */
    public function getStatus(): TimesheetApprovalStatus
    {
        return $this->status;
    }

    /**
     * The current status of this timesheet
     *
     * @param TimesheetApprovalStatus $status
     *
     * @return Self_
     */
    public function setStatus(TimesheetApprovalStatus $status): self
    {
        $this->initialized['status'] = true;
        $this->status = $status;

        return $this;
    }

    /**
     * The total seconds spent on this timesheet
     *
     * @return int
     */
    public function getTimeSpentSeconds(): int
    {
        return $this->timeSpentSeconds;
    }

    /**
     * The total seconds spent on this timesheet
     *
     * @param int $timeSpentSeconds
     *
     * @return Self_
     */
    public function setTimeSpentSeconds(int $timeSpentSeconds): self
    {
        $this->initialized['timeSpentSeconds'] = true;
        $this->timeSpentSeconds = $timeSpentSeconds;

        return $this;
    }

    /**
     * The user owner of this timesheet
     *
     * @return TimesheetApprovalUser
     */
    public function getUser(): TimesheetApprovalUser
    {
        return $this->user;
    }

    /**
     * The user owner of this timesheet
     *
     * @param TimesheetApprovalUser $user
     *
     * @return Self_
     */
    public function setUser(TimesheetApprovalUser $user): self
    {
        $this->initialized['user'] = true;
        $this->user = $user;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return SelfApiBean
     */
    public function getWorklogs(): SelfApiBean
    {
        return $this->worklogs;
    }

    /**
     * A permanent link to this resource
     *
     * @param SelfApiBean $worklogs
     *
     * @return Self_
     */
    public function setWorklogs(SelfApiBean $worklogs): self
    {
        $this->initialized['worklogs'] = true;
        $this->worklogs = $worklogs;

        return $this;
    }
}
