<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class MemberMemberships extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The author of this `worklog`
     *
     * @var User
     */
    protected $member;
    /**
     *
     *
     * @var ActiveMemberships
     */
    protected $memberships;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;
    /**
     *
     *
     * @var Self_
     */
    protected $team;

    /**
     * The author of this `worklog`
     *
     * @return User
     */
    public function getMember(): User
    {
        return $this->member;
    }

    /**
     * The author of this `worklog`
     *
     * @param User $member
     *
     * @return Self_
     */
    public function setMember(User $member): self
    {
        $this->initialized['member'] = true;
        $this->member = $member;

        return $this;
    }

    /**
     *
     *
     * @return ActiveMemberships
     */
    public function getMemberships(): ActiveMemberships
    {
        return $this->memberships;
    }

    /**
     *
     *
     * @param ActiveMemberships $memberships
     *
     * @return Self_
     */
    public function setMemberships(ActiveMemberships $memberships): self
    {
        $this->initialized['memberships'] = true;
        $this->memberships = $memberships;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }

    /**
     *
     *
     * @return Self_
     */
    public function getTeam(): self
    {
        return $this->team;
    }

    /**
     *
     *
     * @param Self_ $team
     *
     * @return Self_
     */
    public function setTeam(self $team): self
    {
        $this->initialized['team'] = true;
        $this->team = $team;

        return $this;
    }
}
