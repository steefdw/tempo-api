<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class DaySchedule extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The date of the schedule
     *
     * @var \DateTime
     */
    protected $date;
    /**
     * The holiday linked to this schedule
     *
     * @var Holiday
     */
    protected $holiday;
    /**
     * The amount of seconds required in this schedule
     *
     * @var int
     */
    protected $requiredSeconds;
    /**
     * The type of schedule.
     *
     * @var string
     */
    protected $type;

    /**
     * The date of the schedule
     *
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * The date of the schedule
     *
     * @param \DateTime $date
     *
     * @return Self_
     */
    public function setDate(\DateTime $date): self
    {
        $this->initialized['date'] = true;
        $this->date = $date;

        return $this;
    }

    /**
     * The holiday linked to this schedule
     *
     * @return Holiday
     */
    public function getHoliday(): Holiday
    {
        return $this->holiday;
    }

    /**
     * The holiday linked to this schedule
     *
     * @param Holiday $holiday
     *
     * @return Self_
     */
    public function setHoliday(Holiday $holiday): self
    {
        $this->initialized['holiday'] = true;
        $this->holiday = $holiday;

        return $this;
    }

    /**
     * The amount of seconds required in this schedule
     *
     * @return int
     */
    public function getRequiredSeconds(): int
    {
        return $this->requiredSeconds;
    }

    /**
     * The amount of seconds required in this schedule
     *
     * @param int $requiredSeconds
     *
     * @return Self_
     */
    public function setRequiredSeconds(int $requiredSeconds): self
    {
        $this->initialized['requiredSeconds'] = true;
        $this->requiredSeconds = $requiredSeconds;

        return $this;
    }

    /**
     * The type of schedule.
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * The type of schedule.
     *
     * @param string $type
     *
     * @return Self_
     */
    public function setType(string $type): self
    {
        $this->initialized['type'] = true;
        $this->type = $type;

        return $this;
    }
}
