<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class SelfListWorkAttributeValue extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;
    /**
     * A group of links referencing this resource
     *
     * @var WorkAttributeValue[]
     */
    protected $values;

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }

    /**
     * A group of links referencing this resource
     *
     * @return WorkAttributeValue[]
     */
    public function getValues(): array
    {
        return $this->values;
    }

    /**
     * A group of links referencing this resource
     *
     * @param WorkAttributeValue[] $values
     *
     * @return Self_
     */
    public function setValues(array $values): self
    {
        $this->initialized['values'] = true;
        $this->values = $values;

        return $this;
    }
}
