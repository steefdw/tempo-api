<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class PageableMetadataBeanWithoutLinks extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * The number of results returned on this page
     *
     * @var int
     */
    protected $count;
    /**
     * Maximum number of results on each page
     *
     * @var int
     */
    protected $limit;
    /**
     * Number of skipped results
     *
     * @var int
     */
    protected $offset;

    /**
     * The number of results returned on this page
     *
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * The number of results returned on this page
     *
     * @param int $count
     *
     * @return Self_
     */
    public function setCount(int $count): self
    {
        $this->initialized['count'] = true;
        $this->count = $count;

        return $this;
    }

    /**
     * Maximum number of results on each page
     *
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * Maximum number of results on each page
     *
     * @param int $limit
     *
     * @return Self_
     */
    public function setLimit(int $limit): self
    {
        $this->initialized['limit'] = true;
        $this->limit = $limit;

        return $this;
    }

    /**
     * Number of skipped results
     *
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * Number of skipped results
     *
     * @param int $offset
     *
     * @return Self_
     */
    public function setOffset(int $offset): self
    {
        $this->initialized['offset'] = true;
        $this->offset = $offset;

        return $this;
    }
}
