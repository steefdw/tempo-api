<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class PlanMetadata extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     * A link to get this plan with all the days of the plan included.
     *
     * @var string
     */
    protected $all;

    /**
     * A link to get this plan with all the days of the plan included.
     *
     * @return string
     */
    public function getAll(): string
    {
        return $this->all;
    }

    /**
     * A link to get this plan with all the days of the plan included.
     *
     * @param string $all
     *
     * @return Self_
     */
    public function setAll(string $all): self
    {
        $this->initialized['all'] = true;
        $this->all = $all;

        return $this;
    }
}
