<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Model;

use ArrayObject;

class Pageable extends ArrayObject
{
    /**
     * @var array
     */
    protected $initialized = [];

    public function isInitialized($property): bool
    {
        return array_key_exists($property, $this->initialized);
    }
    /**
     *
     *
     * @var PageableMetadata
     */
    protected $metadata;
    /**
     *
     *
     * @var Worklog[]
     */
    protected $results;
    /**
     * A permanent link to this resource
     *
     * @var string
     */
    protected $self;

    /**
     *
     *
     * @return PageableMetadata
     */
    public function getMetadata(): PageableMetadata
    {
        return $this->metadata;
    }

    /**
     *
     *
     * @param PageableMetadata $metadata
     *
     * @return Self_
     */
    public function setMetadata(PageableMetadata $metadata): self
    {
        $this->initialized['metadata'] = true;
        $this->metadata = $metadata;

        return $this;
    }

    /**
     *
     *
     * @return Worklog[]
     */
    public function getResults(): array
    {
        return $this->results;
    }

    /**
     *
     *
     * @param Worklog[] $results
     *
     * @return Self_
     */
    public function setResults(array $results): self
    {
        $this->initialized['results'] = true;
        $this->results = $results;

        return $this;
    }

    /**
     * A permanent link to this resource
     *
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * A permanent link to this resource
     *
     * @param string $self
     *
     * @return Self_
     */
    public function setSelf(string $self): self
    {
        $this->initialized['self'] = true;
        $this->self = $self;

        return $this;
    }
}
