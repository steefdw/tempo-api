<?php

declare(strict_types=1);

namespace Steefdw\TempoApi;

use Steefdw\TempoApi\Runtime\Client\Client as ParentClient;

class Client extends ParentClient
{
    /**
     * Retrieves a Category for the given id, or retrieves a list of all Categories if id is not provided. If the Category with the given id is not found, an empty list is returned
     *
     * @param array $queryParameters {
     *     @var int $id
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\CategoryResults|\Psr\Http\Message\ResponseInterface
     */
    public function getCategories(array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetCategories($queryParameters), $fetch);
    }

    /**
     * Creates a new Category using the provided input and returns the newly created Category
     *
     * @param null|\Steefdw\TempoApi\Model\CategoryInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\CreateCategoryBadRequestException
     *
     * @return null|\Steefdw\TempoApi\Model\Category|\Psr\Http\Message\ResponseInterface
     */
    public function createCategory(?\Steefdw\TempoApi\Model\CategoryInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\CreateCategory($requestBody), $fetch);
    }

    /**
     * Deletes the Category for the given key
     *
     * @param string $key
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\DeleteCategoryBadRequestException
     * @throws \Steefdw\TempoApi\Exception\DeleteCategoryNotFoundException
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function deleteCategory(string $key, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\DeleteCategory($key), $fetch);
    }

    /**
     * Retrieves an existing Category for the given key
     *
     * @param string $key
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetCategoryByKeyBadRequestException
     * @throws \Steefdw\TempoApi\Exception\GetCategoryByKeyNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\Category|\Psr\Http\Message\ResponseInterface
     */
    public function getCategoryByKey(string $key, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetCategoryByKey($key), $fetch);
    }

    /**
     * Updates the Category for the given key using the provided input and returns the updated Category
     *
     * @param string $key A set of characters that uniquely identify the category to be updated
     * @param null|\Steefdw\TempoApi\Model\CategoryInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\UpdateCategoryBadRequestException
     * @throws \Steefdw\TempoApi\Exception\UpdateCategoryNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\Category|\Psr\Http\Message\ResponseInterface
     */
    public function updateCategory(string $key, ?\Steefdw\TempoApi\Model\CategoryInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\UpdateCategory($key, $requestBody), $fetch);
    }

    /**
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\CategoryTypeResults|\Psr\Http\Message\ResponseInterface
     */
    public function getTypes(string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetTypes(), $fetch);
    }

    /**
     * Creates a new Account Link using the provided input and returns the newly created Account Link
     *
     * @param null|\Steefdw\TempoApi\Model\AccountLinkInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\CreateLinkBadRequestException
     * @throws \Steefdw\TempoApi\Exception\CreateLinkNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\AccountLink|\Psr\Http\Message\ResponseInterface
     */
    public function createLink(?\Steefdw\TempoApi\Model\AccountLinkInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\CreateLink($requestBody), $fetch);
    }

    /**
     * Retrieves an existing Account Link for the given Project id
     *
     * @param int $id
     * @param array $queryParameters {
     *     @var bool $includeGlobalAccounts
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\AccountLinkResults|\Psr\Http\Message\ResponseInterface
     */
    public function getLinksByProject(int $id, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetLinksByProject($id, $queryParameters), $fetch);
    }

    /**
     * Deletes an existing Account Link for the given id
     *
     * @param int $id An integer uniquely identifying the `AccountLink`
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\DeleteLinkNotFoundException
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function deleteLink(int $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\DeleteLink($id), $fetch);
    }

    /**
     * Retrieves an existing Account Link for the given id
     *
     * @param int $id An integer uniquely identifying the `AccountLink`
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetLinkNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\AccountLink|\Psr\Http\Message\ResponseInterface
     */
    public function getLink(int $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetLink($id), $fetch);
    }

    /**
     * Sets an existing Account Link as the default for the project
     *
     * @param int $id An integer uniquely identifying the `AccountLink`
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\PatchLinkNotFoundException
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function patchLink(int $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\PatchLink($id), $fetch);
    }

    /**
     * Retrieves a list of OPEN and CLOSED Accounts
     *
     * @param array $queryParameters {
     *     @var int $offset Skip over a number of elements by specifying an offset value for the query
     *     @var int $limit Limit the number of elements on the response
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\PageableAccount|\Psr\Http\Message\ResponseInterface
     */
    public function getAccounts(array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetAccounts($queryParameters), $fetch);
    }

    /**
     * Creates a new Account using the provided input and returns the newly created Account
     *
     * @param null|\Steefdw\TempoApi\Model\AccountInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\CreateAccountBadRequestException
     * @throws \Steefdw\TempoApi\Exception\CreateAccountNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\Account|\Psr\Http\Message\ResponseInterface
     */
    public function createAccount(?\Steefdw\TempoApi\Model\AccountInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\CreateAccount($requestBody), $fetch);
    }

    /**
     * Retrieves a list of existing Accounts that matches the given search parameters
     *
     * @param null|\Steefdw\TempoApi\Model\AccountSearchInput $requestBody
     * @param array $queryParameters {
     *     @var int $offset Skip over a number of elements by specifying an offset value for the query
     *     @var int $limit Limit the number of elements on the response
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\PageableAccount|\Psr\Http\Message\ResponseInterface
     */
    public function searchAccounts(?\Steefdw\TempoApi\Model\AccountSearchInput $requestBody = null, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\SearchAccounts($requestBody, $queryParameters), $fetch);
    }

    /**
     * Retrieves an existing Account for the given id
     *
     * @param int $id An integer uniquely identifies the Account
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\Account|\Psr\Http\Message\ResponseInterface
     */
    public function getAccountById(int $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetAccountById($id), $fetch);
    }

    /**
     * Deletes an existing Account for the given key
     *
     * @param string $key A set of character that uniquely identify the Account
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function deleteAccount(string $key, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\DeleteAccount($key), $fetch);
    }

    /**
     * Updates an existing Account for the given key using the provided input and returns the updated Account
     *
     * @param string $key A set of character that uniquely identify the account to be updated
     * @param null|\Steefdw\TempoApi\Model\AccountInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\Account|\Psr\Http\Message\ResponseInterface
     */
    public function updateAccount(string $key, ?\Steefdw\TempoApi\Model\AccountInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\UpdateAccount($key, $requestBody), $fetch);
    }

    /**
     * Retrieves a list of all existing links associated to the provided Account
     *
     * @param string $key A set of character that uniquely identify the Account
     * @param array $queryParameters {
     *     @var int $offset Skip over a number of elements by specifying an offset value for the query
     *     @var int $limit Limit the number of elements on the response
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\PageableAccountLink|\Psr\Http\Message\ResponseInterface
     */
    public function getAccountLinks(string $key, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetAccountLinks($key, $queryParameters), $fetch);
    }

    /**
     * Retrieves a list of all Customers
     *
     * @param array $queryParameters {
     *     @var int $offset Skip over a number of elements by specifying an offset value for the query
     *     @var int $limit Limit the number of elements on the response
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\PageableCustomer|\Psr\Http\Message\ResponseInterface
     */
    public function getCustomers(array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetCustomers($queryParameters), $fetch);
    }

    /**
     * Creates a new Customer using the provided input and returns the newly created Customer
     *
     * @param null|\Steefdw\TempoApi\Model\CustomerInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\CreateCustomerBadRequestException
     *
     * @return null|\Steefdw\TempoApi\Model\Customer|\Psr\Http\Message\ResponseInterface
     */
    public function createCustomer(?\Steefdw\TempoApi\Model\CustomerInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\CreateCustomer($requestBody), $fetch);
    }

    /**
     * Retrieves an existing Customer for the given id
     *
     * @param int $id
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetCustomerByIdNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\Customer|\Psr\Http\Message\ResponseInterface
     */
    public function getCustomerById(int $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetCustomerById($id), $fetch);
    }

    /**
     * Retrieves a list of Accounts associated with the given Customer
     *
     * @param int $id An integer uniquely identifying the customer
     * @param array $queryParameters {
     *     @var int $offset Skip over a number of elements by specifying an offset value for the query
     *     @var int $limit Limit the number of elements on the response
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetCustomerAccountsBadRequestException
     * @throws \Steefdw\TempoApi\Exception\GetCustomerAccountsNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\PageableAccount|\Psr\Http\Message\ResponseInterface
     */
    public function getCustomerAccounts(int $id, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetCustomerAccounts($id, $queryParameters), $fetch);
    }

    /**
     * Deletes the Customer with the given key
     *
     * @param string $key
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\DeleteCustomerBadRequestException
     * @throws \Steefdw\TempoApi\Exception\DeleteCustomerNotFoundException
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function deleteCustomer(string $key, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\DeleteCustomer($key), $fetch);
    }

    /**
     * Updates the Customer with the given key using the provided input and returns the updated Customer
     *
     * @param string $key
     * @param null|\Steefdw\TempoApi\Model\CustomerInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\UpdateCustomerBadRequestException
     * @throws \Steefdw\TempoApi\Exception\UpdateCustomerNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\Customer|\Psr\Http\Message\ResponseInterface
     */
    public function updateCustomer(string $key, ?\Steefdw\TempoApi\Model\CustomerInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\UpdateCustomer($key, $requestBody), $fetch);
    }

    /**
     * Creates a new Generic Resource using the provided input and returns the newly created Generic Resource
     *
     * @param null|\Steefdw\TempoApi\Model\GenericResourceInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\CreateGenericResourceBadRequestException
     *
     * @return null|\Steefdw\TempoApi\Model\GenericResource|\Psr\Http\Message\ResponseInterface
     */
    public function createGenericResource(?\Steefdw\TempoApi\Model\GenericResourceInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\CreateGenericResource($requestBody), $fetch);
    }

    /**
     * Retrieves a list of existing Generic Resources that matches the search parameters
     *
     * @param null|\Steefdw\TempoApi\Model\GenericResourceSearchInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\PageableGenericResourceWithoutSelfLink|\Psr\Http\Message\ResponseInterface
     */
    public function searchGenericResources(?\Steefdw\TempoApi\Model\GenericResourceSearchInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\SearchGenericResources($requestBody), $fetch);
    }

    /**
     * Deletes an existing Generic Resource for the given id
     *
     * @param int $id An id that uniquely identifies the Generic Resource
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\DeleteGenericResourceNotFoundException
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function deleteGenericResource(int $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\DeleteGenericResource($id), $fetch);
    }

    /**
     * Retrieves an existing Generic Resource for the given id
     *
     * @param int $id An id that uniquely identifies the Generic Resource
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetGenericResourceNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\GenericResource|\Psr\Http\Message\ResponseInterface
     */
    public function getGenericResource(int $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetGenericResource($id), $fetch);
    }

    /**
     * Updates an existing Generic Resource for the given id using the provided input and returns the updated Generic Resource
     *
     * @param int $id An id that uniquely identifies the Generic Resource
     * @param null|\Steefdw\TempoApi\Model\GenericResourceInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\EditGenericResourceBadRequestException
     * @throws \Steefdw\TempoApi\Exception\EditGenericResourceNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\GenericResource|\Psr\Http\Message\ResponseInterface
     */
    public function editGenericResource(int $id, ?\Steefdw\TempoApi\Model\GenericResourceInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\EditGenericResource($id, $requestBody), $fetch);
    }

    /**
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\GlobalConfiguration|\Psr\Http\Message\ResponseInterface
     */
    public function getGlobalConfiguration(string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetGlobalConfiguration(), $fetch);
    }

    /**
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\HolidaySchemeResults|\Psr\Http\Message\ResponseInterface
     */
    public function getHolidaySchemes(string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetHolidaySchemes(), $fetch);
    }

    /**
     * Add holiday scheme of a given input
     *
     * @param null|\Steefdw\TempoApi\Model\HolidaySchemeInputBean $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\CreateHolidaySchemeBadRequestException
     *
     * @return null|\Steefdw\TempoApi\Model\HolidayScheme|\Psr\Http\Message\ResponseInterface
     */
    public function createHolidayScheme(?\Steefdw\TempoApi\Model\HolidaySchemeInputBean $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\CreateHolidayScheme($requestBody), $fetch);
    }

    /**
     * Get an user scheme with the given id
     *
     * @param string $accountId A unique identifier of the user in Jira
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\HolidayScheme|\Psr\Http\Message\ResponseInterface
     */
    public function getUserHolidayScheme(string $accountId, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetUserHolidayScheme($accountId), $fetch);
    }

    /**
     * Delete a holiday scheme with the given id
     *
     * @param string $schemeId The id of the Scheme
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\DeleteHolidaySchemeBadRequestException
     * @throws \Steefdw\TempoApi\Exception\DeleteHolidaySchemeNotFoundException
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function deleteHolidayScheme(string $schemeId, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\DeleteHolidayScheme($schemeId), $fetch);
    }

    /**
     * Get Holiday scheme of a given scheme id
     *
     * @param string $schemeId The id of the Scheme
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetHolidaySchemeNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\HolidayScheme|\Psr\Http\Message\ResponseInterface
     */
    public function getHolidayScheme(string $schemeId, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetHolidayScheme($schemeId), $fetch);
    }

    /**
     * Update a holiday scheme with the given id using the provided input
     *
     * @param string $schemeId The id of the Scheme
     * @param null|\Steefdw\TempoApi\Model\HolidaySchemeInputBean $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\UpdateHolidaySchemeBadRequestException
     * @throws \Steefdw\TempoApi\Exception\UpdateHolidaySchemeNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\HolidayScheme|\Psr\Http\Message\ResponseInterface
     */
    public function updateHolidayScheme(string $schemeId, ?\Steefdw\TempoApi\Model\HolidaySchemeInputBean $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\UpdateHolidayScheme($schemeId, $requestBody), $fetch);
    }

    /**
     * Set the default holiday scheme
     *
     * @param string $schemeId The id of the Scheme
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\SetDefaultSchemeNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\HolidayScheme|\Psr\Http\Message\ResponseInterface
     */
    public function setDefaultScheme(string $schemeId, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\SetDefaultScheme($schemeId), $fetch);
    }

    /**
     * List all holidays of a given scheme and year
     *
     * @param string $schemeId The id of the Scheme
     * @param array $queryParameters {
     *     @var int $year Year for holidays to be retrieved for. Returns holidays for current year if omitted.
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetHolidaysNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\HolidayResults|\Psr\Http\Message\ResponseInterface
     */
    public function getHolidays(string $schemeId, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetHolidays($schemeId, $queryParameters), $fetch);
    }

    /**
     * Add a holiday using the provided input
     *
     * @param string $schemeId The id of the Scheme
     * @param null|\Steefdw\TempoApi\Model\HolidayInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\CreateHolidayBadRequestException
     *
     * @return null|\Steefdw\TempoApi\Model\Holiday|\Psr\Http\Message\ResponseInterface
     */
    public function createHoliday(string $schemeId, ?\Steefdw\TempoApi\Model\HolidayInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\CreateHoliday($schemeId, $requestBody), $fetch);
    }

    /**
     * List all floating holidays of a given scheme
     *
     * @param string $schemeId The id of the Scheme
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetFloatingHolidaysNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\HolidayResults|\Psr\Http\Message\ResponseInterface
     */
    public function getFloatingHolidays(string $schemeId, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetFloatingHolidays($schemeId), $fetch);
    }

    /**
     * Delete a holiday with the given id
     *
     * @param string $schemeId The id of the Scheme
     * @param string $holidayId The id of the holiday
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\DeleteHolidayNotFoundException
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function deleteHoliday(string $schemeId, string $holidayId, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\DeleteHoliday($schemeId, $holidayId), $fetch);
    }

    /**
     * Get holiday information
     *
     * @param string $schemeId The id of the holiday scheme
     * @param string $holidayId The id of the holiday
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetHolidayNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\Holiday|\Psr\Http\Message\ResponseInterface
     */
    public function getHoliday(string $schemeId, string $holidayId, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetHoliday($schemeId, $holidayId), $fetch);
    }

    /**
     * Update a holiday with the given id using the provided input
     *
     * @param string $schemeId The id of the Scheme
     * @param string $holidayId The id of the holiday
     * @param null|\Steefdw\TempoApi\Model\HolidayInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\UpdateHolidayBadRequestException
     * @throws \Steefdw\TempoApi\Exception\UpdateHolidayNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\Holiday|\Psr\Http\Message\ResponseInterface
     */
    public function updateHoliday(string $schemeId, string $holidayId, ?\Steefdw\TempoApi\Model\HolidayInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\UpdateHoliday($schemeId, $holidayId, $requestBody), $fetch);
    }

    /**
     * Get holiday scheme members with the given id
     *
     * @param string $schemeId The id of the Scheme
     * @param array $queryParameters {
     *     @var int $offset Skip over a number of elements by specifying an offset value for the query
     *     @var int $limit Limit the number of elements on the response
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetWorkloadSchemeMembersNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\PageableUser|\Psr\Http\Message\ResponseInterface
     */
    public function getWorkloadSchemeMembers(string $schemeId, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetWorkloadSchemeMembers($schemeId, $queryParameters), $fetch);
    }

    /**
     * Assign a holiday scheme with the given id to members using the provided input
     *
     * @param string $schemeId The id of the Scheme
     * @param null|\Steefdw\TempoApi\Model\HolidaySchemeMembersInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\SetWorkloadSchemeMembershipBadRequestException
     * @throws \Steefdw\TempoApi\Exception\SetWorkloadSchemeMembershipNotFoundException
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function setWorkloadSchemeMembership(string $schemeId, ?\Steefdw\TempoApi\Model\HolidaySchemeMembersInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\SetWorkloadSchemeMembership($schemeId, $requestBody), $fetch);
    }

    /**
     * Retrieves a list of all Periods for the given date range
     *
     * @param array $queryParameters {
     *     @var string $from The start date in format yyyy-mm-dd
     *     @var string $to The end date in format yyyy-mm-dd
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\TimesheetApprovalPeriodsBean|\Psr\Http\Message\ResponseInterface
     */
    public function getPeriods(array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetPeriods($queryParameters), $fetch);
    }

    /**
     * Retrieves a list of the Permission Roles for a given Team if ID is informed, or retrieves a list of all Permission Roles
     *
     * @param array $queryParameters {
     *     @var int $offset Skip over a number of elements by specifying an offset value for the query
     *     @var int $limit Limit the number of elements on the response
     *     @var string $teamId An ID that uniquely identifies the Team for which you want to retrieve Permissions
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\PageablePermissionRole|\Psr\Http\Message\ResponseInterface
     */
    public function getPermissionRoles(array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetPermissionRoles($queryParameters), $fetch);
    }

    /**
     * Creates a new editable Permission Role using the provided input and returns the newly created Permission Role
     *
     * @param null|\Steefdw\TempoApi\Model\PermissionRoleInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\CreatePermissionGroupBadRequestException
     *
     * @return null|\Steefdw\TempoApi\Model\PermissionRole|\Psr\Http\Message\ResponseInterface
     */
    public function createPermissionGroup(?\Steefdw\TempoApi\Model\PermissionRoleInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\CreatePermissionGroup($requestBody), $fetch);
    }

    /**
     * Retrieves the Global Permission Roles
     *
     * @param array $queryParameters {
     *     @var int $offset Skip over a number of elements by specifying an offset value for the query
     *     @var int $limit Limit the number of elements on the response
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\PageablePermissionRole|\Psr\Http\Message\ResponseInterface
     */
    public function getGlobalPermissionRoles(array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetGlobalPermissionRoles($queryParameters), $fetch);
    }

    /**
     * Deletes an editable Permission Role.
     *
     * @param int $id
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\DeleteEditablePermissionGroupNotFoundException
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function deleteEditablePermissionGroup(int $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\DeleteEditablePermissionGroup($id), $fetch);
    }

    /**
     * Retrieves a Permission Role for the given id
     *
     * @param string $id ID of the permission role
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetPermissionRoleNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\PermissionRole|\Psr\Http\Message\ResponseInterface
     */
    public function getPermissionRole(string $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetPermissionRole($id), $fetch);
    }

    /**
     * Updates a Permission Role for the given id using the provided input and returns the updated Permission Role. Only editable roles can be updated.
     *
     * @param int $id
     * @param null|\Steefdw\TempoApi\Model\PermissionRoleInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\UpdatePermissionGroupBadRequestException
     * @throws \Steefdw\TempoApi\Exception\UpdatePermissionGroupNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\PermissionRole|\Psr\Http\Message\ResponseInterface
     */
    public function updatePermissionGroup(int $id, ?\Steefdw\TempoApi\Model\PermissionRoleInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\UpdatePermissionGroup($id, $requestBody), $fetch);
    }

    /**
     * Retrieves a list of existing Plans (Resource Allocations) that matches the given search parameters
     *
     * @param array $queryParameters {
     *     @var array $accountIds Ids of the user assignees you want to search plans for.
     *     @var array $assigneeTypes Types of the assignees you want to search plans for.
     *     @var string $from Retrieve plans starting with this date.
     *     @var array $genericResourceIds Ids of the generic resources you want to search plans for.
     *     @var int $limit Maximum number of results on each page.
     *     @var int $offset Number of skipped results.
     *     @var array $planIds Ids of the plans you want to search for.
     *     @var array $planItemIds Ids of the items you want to search plans for.
     *     @var array $planItemTypes Types of the items you want to search plans for.
     *     @var array $plannedTimeBreakdown Defines how detailed you would like to see the breakdown of the planned time for each `Plan`.
     *     @var string $to Retrieve plans that ends up to and including this date.
     *     @var string $updatedFrom Retrieve plans that have been updated from this date.
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\PageablePlan|\Psr\Http\Message\ResponseInterface
     */
    public function getPlans(array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetPlans($queryParameters), $fetch);
    }

    /**
     * Creates a new Plan (Resource Allocation) using the provided input and returns the newly created Plan
     *
     * @param null|\Steefdw\TempoApi\Model\PlanInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\CreatePlanBadRequestException
     *
     * @return null|\Steefdw\TempoApi\Model\Plan|\Psr\Http\Message\ResponseInterface
     */
    public function createPlan(?\Steefdw\TempoApi\Model\PlanInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\CreatePlan($requestBody), $fetch);
    }

    /**
     * Retrieves a list of existing Plans (Resource Allocations) for the given Generic Resource
     *
     * @param int $genericResourceId Id of the generic resource you want to search plans for
     * @param array $queryParameters {
     *     @var array $plannedTimeBreakdown Defines how detailed you would like to see the breakdown of the planned time for each plan.You can add one or both of these values as query params [DAILY, PERIOD] to see more detailed breakdown of planned time.
     *     @var string $from Retrieve plans starting with this date
     *     @var string $to Retrieve plans that ends up to and including this date
     *     @var string $updatedFrom Retrieve plans that have been updated from this date
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\PlanResults|\Psr\Http\Message\ResponseInterface
     */
    public function getPlansForGenericResource(int $genericResourceId, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetPlansForGenericResource($genericResourceId, $queryParameters), $fetch);
    }

    /**
     * Retrieves a list of existing Plans (Resource Allocations) that matches the given search parameters
     *
     * @param null|\Steefdw\TempoApi\Model\PlanSearchInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\PageablePlanBeanWithoutLinks|\Psr\Http\Message\ResponseInterface
     */
    public function searchPlans(?\Steefdw\TempoApi\Model\PlanSearchInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\SearchPlans($requestBody), $fetch);
    }

    /**
     * Retrieves a list of Plans (Resource Allocations) for the given User
     *
     * @param string $accountId Id of the user you want to search plans for
     * @param array $queryParameters {
     *     @var array $plannedTimeBreakdown Defines how detailed you would like to see the breakdown of the planned time for each plan.You can add one or both of these values as query params [DAILY, PERIOD] to see more detailed breakdown of planned time.
     *     @var string $from Retrieve plans starting with this date
     *     @var string $to Retrieve plans that ends up to and including this date
     *     @var string $updatedFrom Retrieve plans that have been updated from this date
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\PlanResults|\Psr\Http\Message\ResponseInterface
     */
    public function getPlansForUser(string $accountId, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetPlansForUser($accountId, $queryParameters), $fetch);
    }

    /**
     * Deletes an existing Plan (Resource Allocation) for the given id
     *
     * @param int $id An id that uniquely identifies the Plan
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\DeletePlanByIdNotFoundException
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function deletePlanById(int $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\DeletePlanById($id), $fetch);
    }

    /**
     * Retrieves an existing Plan (Resource Allocation) for the given id
     *
     * @param int $id An id that uniquely identifies the Plan
     * @param array $queryParameters {
     *     @var string $from
     *     @var string $to
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetPlanByIdNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\Plan|\Psr\Http\Message\ResponseInterface
     */
    public function getPlanById(int $id, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetPlanById($id, $queryParameters), $fetch);
    }

    /**
     * Updates an existing Plan (Resource Allocation) for the given id using the provided input and returns the updated Plan
     *
     * @param int $id An id that uniquely identifies the Plan
     * @param null|\Steefdw\TempoApi\Model\PlanInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\UpdatePlanBadRequestException
     * @throws \Steefdw\TempoApi\Exception\UpdatePlanNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\Plan|\Psr\Http\Message\ResponseInterface
     */
    public function updatePlan(int $id, ?\Steefdw\TempoApi\Model\PlanInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\UpdatePlan($id, $requestBody), $fetch);
    }

    /**
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\ProgramResults|\Psr\Http\Message\ResponseInterface
     */
    public function getPrograms(string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetPrograms(), $fetch);
    }

    /**
     * Creates a new Program using the provided input and returns the newly created Program
     *
     * @param null|\Steefdw\TempoApi\Model\ProgramInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\CreateProgramBadRequestException
     *
     * @return null|\Steefdw\TempoApi\Model\Program|\Psr\Http\Message\ResponseInterface
     */
    public function createProgram(?\Steefdw\TempoApi\Model\ProgramInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\CreateProgram($requestBody), $fetch);
    }

    /**
     * Deletes an existing Program for the given id
     *
     * @param string $id
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\DeleteProgramBadRequestException
     * @throws \Steefdw\TempoApi\Exception\DeleteProgramNotFoundException
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function deleteProgram(string $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\DeleteProgram($id), $fetch);
    }

    /**
     * Retrieves an existing Program for the given id
     *
     * @param string $id
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetProgramByIdNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\Program|\Psr\Http\Message\ResponseInterface
     */
    public function getProgramById(string $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetProgramById($id), $fetch);
    }

    /**
     * Updates an existing Program for the given id using the provided input and returns the updated Program
     *
     * @param string $id
     * @param null|\Steefdw\TempoApi\Model\ProgramInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\UpdateProgramBadRequestException
     * @throws \Steefdw\TempoApi\Exception\UpdateProgramNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\Program|\Psr\Http\Message\ResponseInterface
     */
    public function updateProgram(string $id, ?\Steefdw\TempoApi\Model\ProgramInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\UpdateProgram($id, $requestBody), $fetch);
    }

    /**
     * Retrieves a list of Teams associated with the given Program
     *
     * @param string $id
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetTeamsByProgramIdBadRequestException
     * @throws \Steefdw\TempoApi\Exception\GetTeamsByProgramIdNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\TeamRefResults|\Psr\Http\Message\ResponseInterface
     */
    public function getTeamsByProgramId(string $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetTeamsByProgramId($id), $fetch);
    }

    /**
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\RoleResults|\Psr\Http\Message\ResponseInterface
     */
    public function getAllRoles(string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetAllRoles(), $fetch);
    }

    /**
     * Creates a new Role and returns the newly created Role
     *
     * @param null|\Steefdw\TempoApi\Model\RoleInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\CreateRoleBadRequestException
     *
     * @return null|\Steefdw\TempoApi\Model\Role|\Psr\Http\Message\ResponseInterface
     */
    public function createRole(?\Steefdw\TempoApi\Model\RoleInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\CreateRole($requestBody), $fetch);
    }

    /**
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\Role|\Psr\Http\Message\ResponseInterface
     */
    public function getDefaultRole(string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetDefaultRole(), $fetch);
    }

    /**
     * Deletes an existing Role for the given id
     *
     * @param string $id An id that uniquely identifies the Role
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function deleteRole(string $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\DeleteRole($id), $fetch);
    }

    /**
     * Retrieves an existing Role for the given id
     *
     * @param string $id An id that uniquely identifies the Role
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetRoleByIdNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\Role|\Psr\Http\Message\ResponseInterface
     */
    public function getRoleById(string $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetRoleById($id), $fetch);
    }

    /**
     * Updates an existing Role for the given id with the given parameters
     *
     * @param string $id An id that uniquely identifies the Role
     * @param null|\Steefdw\TempoApi\Model\RoleInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\UpdateRoleBadRequestException
     *
     * @return null|\Steefdw\TempoApi\Model\Role|\Psr\Http\Message\ResponseInterface
     */
    public function updateRole(string $id, ?\Steefdw\TempoApi\Model\RoleInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\UpdateRole($id, $requestBody), $fetch);
    }

    /**
     * Assigns a list of Skills for the given Resource using the provided input and returns a list of the Resource's assigned Skills. A Resource can have up to 5 Skills assigned.
     *
     * @param null|\Steefdw\TempoApi\Model\SkillsAssignmentInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\AssignSkillsBadRequestException
     *
     * @return null|\Steefdw\TempoApi\Model\Skill[]|\Psr\Http\Message\ResponseInterface
     */
    public function assignSkills(?\Steefdw\TempoApi\Model\SkillsAssignmentInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\AssignSkills($requestBody), $fetch);
    }

    /**
     * Replaces Skills for the given Resource using the provided input and returns a list of the Resource's assigned Skills. A Resource can have up to 5 skills assigned.
     *
     * @param null|\Steefdw\TempoApi\Model\SkillsAssignmentInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\ReplaceSkillAssignmentsBadRequestException
     *
     * @return null|\Steefdw\TempoApi\Model\Skill[]|\Psr\Http\Message\ResponseInterface
     */
    public function replaceSkillAssignments(?\Steefdw\TempoApi\Model\SkillsAssignmentInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\ReplaceSkillAssignments($requestBody), $fetch);
    }

    /**
     * Retrieves a list of Skill Assignments for the given Resource
     *
     * @param string $assigneeId A string that uniquely identifies the resource
     * @param string $assigneeType A string that identifies the resource type, which can be USER or GENERIC
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetSkillAssignmentsBadRequestException
     *
     * @return null|\Steefdw\TempoApi\Model\Skill[]|\Psr\Http\Message\ResponseInterface
     */
    public function getSkillAssignments(string $assigneeId, string $assigneeType, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetSkillAssignments($assigneeId, $assigneeType), $fetch);
    }

    /**
     * Deletes an existing Skill for the given Resource
     *
     * @param string $assigneeId A string that uniquely identifies the resource
     * @param string $assigneeType A string that identifies the resource type, which can be USER or GENERIC
     * @param int $skillId An id that uniquely identify the Skill
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function removeSkillAssignment(string $assigneeId, string $assigneeType, int $skillId, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\RemoveSkillAssignment($assigneeId, $assigneeType, $skillId), $fetch);
    }

    /**
     * Retrieves a list of existing Skills that matches the given search parameters
     *
     * @param array $queryParameters {
     *     @var array $id Ids of the Skills you want to retrieve
     *     @var string $query A query to be used for string matching against name of Skills
     *     @var int $offset Skip over a number of elements by specifying an offset value for the query
     *     @var int $limit Limit the number of elements on the response
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\PageableSkill|\Psr\Http\Message\ResponseInterface
     */
    public function getSkills(array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetSkills($queryParameters), $fetch);
    }

    /**
     * Creates a new Skill using the provided input and returns the newly created Skill
     *
     * @param null|\Steefdw\TempoApi\Model\SkillInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\CreateSkillBadRequestException
     *
     * @return null|\Steefdw\TempoApi\Model\Skill|\Psr\Http\Message\ResponseInterface
     */
    public function createSkill(?\Steefdw\TempoApi\Model\SkillInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\CreateSkill($requestBody), $fetch);
    }

    /**
     * Deletes an existing Skill for the given id
     *
     * @param int $id An id that uniquely identifies the Skill
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\DeleteSkillNotFoundException
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function deleteSkill(int $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\DeleteSkill($id), $fetch);
    }

    /**
     * Retrieves an existing Skill for the given id
     *
     * @param int $id An id that uniquely identifies the Skill
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetSkillNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\Skill|\Psr\Http\Message\ResponseInterface
     */
    public function getSkill(int $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetSkill($id), $fetch);
    }

    /**
     * Updates an existing Skill for the given id using the provided input and returns the updated Skill
     *
     * @param int $id An id that uniquely identifies the Skill
     * @param null|\Steefdw\TempoApi\Model\SkillInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\UpdateSkillBadRequestException
     * @throws \Steefdw\TempoApi\Exception\UpdateSkillNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\Skill|\Psr\Http\Message\ResponseInterface
     */
    public function updateSkill(int $id, ?\Steefdw\TempoApi\Model\SkillInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\UpdateSkill($id, $requestBody), $fetch);
    }

    /**
     * Creates a new Team Link using the provided input and returns the newly created Team Link
     *
     * @param null|\Steefdw\TempoApi\Model\TeamLinkInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\CreateTeamLinkBadRequestException
     *
     * @return null|\Steefdw\TempoApi\Model\TeamLink|\Psr\Http\Message\ResponseInterface
     */
    public function createTeamLink(?\Steefdw\TempoApi\Model\TeamLinkInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\CreateTeamLink($requestBody), $fetch);
    }

    /**
     * Retrieves an existing Team Link for the given Project id
     *
     * @param int $id
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\TeamLinkResults|\Psr\Http\Message\ResponseInterface
     */
    public function getTeamLinkByProjectId(int $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetTeamLinkByProjectId($id), $fetch);
    }

    /**
     * Deletes an existing Team Link for the given id
     *
     * @param string $linkId
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\DeleteTeamLinkNotFoundException
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function deleteTeamLink(string $linkId, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\DeleteTeamLink($linkId), $fetch);
    }

    /**
     * Retrieves an existing Team Link for the given id
     *
     * @param string $linkId
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetTeamLinkNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\TeamLink|\Psr\Http\Message\ResponseInterface
     */
    public function getTeamLink(string $linkId, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetTeamLink($linkId), $fetch);
    }

    /**
     * Creates a new Membership using the provided input and returns the newly created Membership
     *
     * @param null|\Steefdw\TempoApi\Model\TeamMembershipInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\CreateMembershipBadRequestException
     *
     * @return null|\Steefdw\TempoApi\Model\TeamMembership|\Psr\Http\Message\ResponseInterface
     */
    public function createMembership(?\Steefdw\TempoApi\Model\TeamMembershipInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\CreateMembership($requestBody), $fetch);
    }

    /**
     * Retrieves a list of all the Memberships for the given Team
     *
     * @param string $teamId
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetAllMembershipsNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\TeamMembershipResults|\Psr\Http\Message\ResponseInterface
     */
    public function getAllMemberships(string $teamId, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetAllMemberships($teamId), $fetch);
    }

    /**
     * Deletes an existing Membership for the given id
     *
     * @param string $id
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\DeleteMembershipNotFoundException
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function deleteMembership(string $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\DeleteMembership($id), $fetch);
    }

    /**
     * Retrieves an existing Membership for the given id
     *
     * @param string $id
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetMembershipNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\TeamMembership|\Psr\Http\Message\ResponseInterface
     */
    public function getMembership(string $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetMembership($id), $fetch);
    }

    /**
     * Updates an existing Membership using the provided input and returns the updated Membership
     *
     * @param string $id
     * @param null|\Steefdw\TempoApi\Model\MembershipInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\UpdateMembershipBadRequestException
     * @throws \Steefdw\TempoApi\Exception\UpdateMembershipNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\TeamMembership|\Psr\Http\Message\ResponseInterface
     */
    public function updateMembership(string $id, ?\Steefdw\TempoApi\Model\MembershipInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\UpdateMembership($id, $requestBody), $fetch);
    }

    /**
     * Retrieves a list of all existing Teams
     *
     * @param array $queryParameters {
     *     @var int $offset Skip over a number of elements by specifying an offset value for the query.
     *     @var int $limit Limit the number of elements on the response.
     *     @var array $teamMembers Jira user account ids of the user team members you want to get teams for.
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\PageableTeam|\Psr\Http\Message\ResponseInterface
     */
    public function getTeams(array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetTeams($queryParameters), $fetch);
    }

    /**
     * Creates a new Team using the provided input and returns the newly created Team
     *
     * @param null|\Steefdw\TempoApi\Model\TeamInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\CreateTeamBadRequestException
     *
     * @return null|\Steefdw\TempoApi\Model\Team|\Psr\Http\Message\ResponseInterface
     */
    public function createTeam(?\Steefdw\TempoApi\Model\TeamInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\CreateTeam($requestBody), $fetch);
    }

    /**
     * Deletes the Team for the given id
     *
     * @param string $id An id that uniquely identifies the Team.
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\DeleteTeamBadRequestException
     * @throws \Steefdw\TempoApi\Exception\DeleteTeamNotFoundException
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function deleteTeam(string $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\DeleteTeam($id), $fetch);
    }

    /**
     * Retrieves an existing Team for the given id
     *
     * @param string $id An id that uniquely identifies the Team.
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetTeamByIdNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\Team|\Psr\Http\Message\ResponseInterface
     */
    public function getTeamById(string $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetTeamById($id), $fetch);
    }

    /**
     * Updates the Team for the given id using the provided input, and returns the updated Team
     *
     * @param string $id An id that uniquely identifies the Team.
     * @param null|\Steefdw\TempoApi\Model\TeamInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\UpdateTeamBadRequestException
     * @throws \Steefdw\TempoApi\Exception\UpdateTeamNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\Team|\Psr\Http\Message\ResponseInterface
     */
    public function updateTeam(string $id, ?\Steefdw\TempoApi\Model\TeamInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\UpdateTeam($id, $requestBody), $fetch);
    }

    /**
     * Retrieves a list of all Links for the given Team
     *
     * @param string $id An id that uniquely identifies the Team.
     * @param array $queryParameters {
     *     @var int $offset Skip over a number of elements by specifying an offset value for the query.
     *     @var int $limit Limit the number of elements on the response.
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetTeamLinksNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\PageableTeamLink|\Psr\Http\Message\ResponseInterface
     */
    public function getTeamLinks(string $id, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetTeamLinks($id, $queryParameters), $fetch);
    }

    /**
     * Retrieves a list of users with their active membership. Meaning that they are currently (today) members of this team via said membership
     *
     * @param int $id
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetTeamMembersNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\PageableMemberMemberships|\Psr\Http\Message\ResponseInterface
     */
    public function getTeamMembers(int $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetTeamMembers($id), $fetch);
    }

    /**
     * Retrieves a list of all Generic Resources that are members of the given Team
     *
     * @param int $teamId An id that uniquely identifies the Team from which generic resources are to be retrieved
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetResourcesInTeamNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\GenericResourceTeamMembers|\Psr\Http\Message\ResponseInterface
     */
    public function getResourcesInTeam(int $teamId, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetResourcesInTeam($teamId), $fetch);
    }

    /**
     * Adds Generic Resource to the given Team
     *
     * @param int $teamId An id that uniquely identifies the Team which the generic resource is to be a member of
     * @param null|\Steefdw\TempoApi\Model\GenericResourceTeamMemberInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\AddResourceToTeamBadRequestException
     * @throws \Steefdw\TempoApi\Exception\AddResourceToTeamNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\GenericResourceTeamMember|\Psr\Http\Message\ResponseInterface
     */
    public function addResourceToTeam(int $teamId, ?\Steefdw\TempoApi\Model\GenericResourceTeamMemberInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\AddResourceToTeam($teamId, $requestBody), $fetch);
    }

    /**
     * Deletes an existing Generic Resource from the given Team
     *
     * @param int $teamId An id that uniquely identifies the Team
     * @param int $resourceId An id that uniquely identifies the Generic Resource
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\RemoveGenericResourceFromTeamNotFoundException
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function removeGenericResourceFromTeam(int $teamId, int $resourceId, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\RemoveGenericResourceFromTeam($teamId, $resourceId), $fetch);
    }

    /**
     * Retrieves an existing Generic Resource from the given Team
     *
     * @param int $teamId An id that uniquely identifies the Team
     * @param int $resourceId An id that uniquely identifies the Generic Resource
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetGenericResourceTeamMemberNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\GenericResourceTeamMember|\Psr\Http\Message\ResponseInterface
     */
    public function getGenericResourceTeamMember(int $teamId, int $resourceId, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetGenericResourceTeamMember($teamId, $resourceId), $fetch);
    }

    /**
     * Retrieves the current Timesheet Approval for a Team in the given period
     *
     * @param string $teamId
     * @param array $queryParameters {
     *     @var string $from The Start Date in format yyyy-mm-dd
     *     @var string $to The End Date in format yyyy-mm-dd
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetTimesheetApprovalForTeamNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\TimesheetApprovalResults|\Psr\Http\Message\ResponseInterface
     */
    public function getTimesheetApprovalForTeam(string $teamId, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetTimesheetApprovalForTeam($teamId, $queryParameters), $fetch);
    }

    /**
     * Retrieves the current Timesheet approval for a User in the given period
     *
     * @param string $accountId
     * @param array $queryParameters {
     *     @var string $from The Start Date in format yyyy-mm-dd
     *     @var string $to The End Date in format yyyy-mm-dd
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetTimesheetApprovalForUserBadRequestException
     * @throws \Steefdw\TempoApi\Exception\GetTimesheetApprovalForUserNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\TimesheetApproval|\Psr\Http\Message\ResponseInterface
     */
    public function getTimesheetApprovalForUser(string $accountId, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetTimesheetApprovalForUser($accountId, $queryParameters), $fetch);
    }

    /**
     * Approves a Timesheet for the given User in the given period and returns the approved Timesheet
     *
     * @param string $accountId
     * @param null|\Steefdw\TempoApi\Model\TimesheetApprovalInput $requestBody
     * @param array $queryParameters {
     *     @var string $from The Start Date in format yyyy-mm-dd
     *     @var string $to The End Date in format yyyy-mm-dd
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\ApproveTimesheetForUserBadRequestException
     * @throws \Steefdw\TempoApi\Exception\ApproveTimesheetForUserNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\TimesheetApproval|\Psr\Http\Message\ResponseInterface
     */
    public function approveTimesheetForUser(string $accountId, ?\Steefdw\TempoApi\Model\TimesheetApprovalInput $requestBody = null, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\ApproveTimesheetForUser($accountId, $requestBody, $queryParameters), $fetch);
    }

    /**
     * Rejects a Timesheet for the given User in the given period and returns the rejected Timesheet
     *
     * @param string $accountId
     * @param null|\Steefdw\TempoApi\Model\TimesheetApprovalInput $requestBody
     * @param array $queryParameters {
     *     @var string $from The Start Date in format yyyy-mm-dd
     *     @var string $to The End Date in format yyyy-mm-dd
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\RejectTimesheetForUserBadRequestException
     * @throws \Steefdw\TempoApi\Exception\RejectTimesheetForUserNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\TimesheetApproval|\Psr\Http\Message\ResponseInterface
     */
    public function rejectTimesheetForUser(string $accountId, ?\Steefdw\TempoApi\Model\TimesheetApprovalInput $requestBody = null, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\RejectTimesheetForUser($accountId, $requestBody, $queryParameters), $fetch);
    }

    /**
     * Reopens a Timesheet for the given User in the given period and returns the reopened Timesheet
     *
     * @param string $accountId
     * @param null|\Steefdw\TempoApi\Model\TimesheetApprovalInput $requestBody
     * @param array $queryParameters {
     *     @var string $from The Start Date in format yyyy-mm-dd
     *     @var string $to The End Date in format yyyy-mm-dd
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\ReopenTimesheetForUserBadRequestException
     * @throws \Steefdw\TempoApi\Exception\ReopenTimesheetForUserNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\TimesheetApproval|\Psr\Http\Message\ResponseInterface
     */
    public function reopenTimesheetForUser(string $accountId, ?\Steefdw\TempoApi\Model\TimesheetApprovalInput $requestBody = null, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\ReopenTimesheetForUser($accountId, $requestBody, $queryParameters), $fetch);
    }

    /**
     * Retrieves a list of the Timesheet reviewers for the given User
     *
     * @param string $accountId
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetTimesheetReviewersForUserNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\UserResults|\Psr\Http\Message\ResponseInterface
     */
    public function getTimesheetReviewersForUser(string $accountId, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetTimesheetReviewersForUser($accountId), $fetch);
    }

    /**
     * Submits a Timesheet for the given User in the given period and returns the submitted Timesheet
     *
     * @param string $accountId
     * @param null|\Steefdw\TempoApi\Model\TimesheetApprovalInput $requestBody
     * @param array $queryParameters {
     *     @var string $from The Start Date in format yyyy-mm-dd
     *     @var string $to The End Date in format yyyy-mm-dd
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\SubmitTimesheetForUserBadRequestException
     * @throws \Steefdw\TempoApi\Exception\SubmitTimesheetForUserNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\TimesheetApproval|\Psr\Http\Message\ResponseInterface
     */
    public function submitTimesheetForUser(string $accountId, ?\Steefdw\TempoApi\Model\TimesheetApprovalInput $requestBody = null, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\SubmitTimesheetForUser($accountId, $requestBody, $queryParameters), $fetch);
    }

    /**
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\TimesheetApprovalResults|\Psr\Http\Message\ResponseInterface
     */
    public function getTimesheetApprovalsWaitingForApproval(string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetTimesheetApprovalsWaitingForApproval(), $fetch);
    }

    /**
     * Retrieves the Schedule of the logged-in User
     *
     * @param array $queryParameters {
     *     @var string $from The start date in format yyyy-mm-dd
     *     @var string $to The end date in format yyyy-mm-dd
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\DayScheduleResults|\Psr\Http\Message\ResponseInterface
     */
    public function getAuthenticatedUserSchedule(array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetAuthenticatedUserSchedule($queryParameters), $fetch);
    }

    /**
     * Retrieves Schedule for the given User
     *
     * @param string $accountId
     * @param array $queryParameters {
     *     @var string $from The start date in format yyyy-mm-dd
     *     @var string $to The end date in format yyyy-mm-dd
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\DayScheduleResults|\Psr\Http\Message\ResponseInterface
     */
    public function getUserSchedule(string $accountId, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetUserSchedule($accountId, $queryParameters), $fetch);
    }

    /**
     * Retrieves a list of all Work Attributes
     *
     * @param array $queryParameters {
     *     @var int $offset Skip over a number of elements by specifying an offset value for the query
     *     @var int $limit Limit the number of elements on the response
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\PageableWorkAttribute|\Psr\Http\Message\ResponseInterface
     */
    public function getWorkAttributes(array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetWorkAttributes($queryParameters), $fetch);
    }

    /**
     * Creates a new Work Attribute using the provided input and returns the newly created Work Attribute
     *
     * @param null|\Steefdw\TempoApi\Model\CreateWorkAttributeInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\CreateWorkAttributesBadRequestException
     *
     * @return null|\Steefdw\TempoApi\Model\WorkAttribute|\Psr\Http\Message\ResponseInterface
     */
    public function createWorkAttributes(?\Steefdw\TempoApi\Model\CreateWorkAttributeInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\CreateWorkAttributes($requestBody), $fetch);
    }

    /**
     * Deletes an existing Work Attribute for the given key
     *
     * @param string $key
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\DeleteWorkAttributeByKeyNotFoundException
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function deleteWorkAttributeByKey(string $key, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\DeleteWorkAttributeByKey($key), $fetch);
    }

    /**
     * Retrieves an existing Work Attribute for the given key
     *
     * @param string $key
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetWorkAttributeByKeyNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\WorkAttribute|\Psr\Http\Message\ResponseInterface
     */
    public function getWorkAttributeByKey(string $key, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetWorkAttributeByKey($key), $fetch);
    }

    /**
     * Updates an existing Work Attribute for the given key using the provided input and returns the updated Work Attribute
     *
     * @param string $key
     * @param null|\Steefdw\TempoApi\Model\UpdateWorkAttributeInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\UpdateWorkAttributeByKeyBadRequestException
     * @throws \Steefdw\TempoApi\Exception\UpdateWorkAttributeByKeyNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\WorkAttribute|\Psr\Http\Message\ResponseInterface
     */
    public function updateWorkAttributeByKey(string $key, ?\Steefdw\TempoApi\Model\UpdateWorkAttributeInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\UpdateWorkAttributeByKey($key, $requestBody), $fetch);
    }

    /**
     * Retrieves a list of all Workload Schemes
     *
     * @param array $queryParameters {
     *     @var int $offset Skip over a number of elements by specifying an offset value for the query
     *     @var int $limit Limit the number of elements on the response
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetWorkloadSchemesNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\PageableWorkloadScheme|\Psr\Http\Message\ResponseInterface
     */
    public function getWorkloadSchemes(array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetWorkloadSchemes($queryParameters), $fetch);
    }

    /**
     * Creates a new Workload Scheme using the provided input and returns the newly created Workload Scheme
     *
     * @param null|\Steefdw\TempoApi\Model\WorkloadSchemeInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\WorkloadScheme|\Psr\Http\Message\ResponseInterface
     */
    public function createWorkloadScheme(?\Steefdw\TempoApi\Model\WorkloadSchemeInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\CreateWorkloadScheme($requestBody), $fetch);
    }

    /**
     * Retrieves an existing Workload Scheme for the given User
     *
     * @param string $accountId A unique identifier of the user in Jira
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetUserWorkloadSchemeNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\WorkloadScheme|\Psr\Http\Message\ResponseInterface
     */
    public function getUserWorkloadScheme(string $accountId, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetUserWorkloadScheme($accountId), $fetch);
    }

    /**
     * Deletes a Workload Scheme for the given id
     *
     * @param string $id ID of the workload scheme
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\DeleteWorkloadSchemeNotFoundException
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function deleteWorkloadScheme(string $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\DeleteWorkloadScheme($id), $fetch);
    }

    /**
     * Retrieves an existing Workload Scheme for the given id
     *
     * @param string $id ID of the workload scheme
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetWorkloadSchemeByIdNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\WorkloadScheme|\Psr\Http\Message\ResponseInterface
     */
    public function getWorkloadSchemeById(string $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetWorkloadSchemeById($id), $fetch);
    }

    /**
     * Updates an existing Workload Scheme for the given id using the provided input and returns the updated Workload Scheme
     *
     * @param string $id ID of the workload scheme
     * @param null|\Steefdw\TempoApi\Model\WorkloadSchemeInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\UpdateWorkloadSchemeNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\WorkloadScheme|\Psr\Http\Message\ResponseInterface
     */
    public function updateWorkloadScheme(string $id, ?\Steefdw\TempoApi\Model\WorkloadSchemeInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\UpdateWorkloadScheme($id, $requestBody), $fetch);
    }

    /**
     * Sets the given Workload Scheme as default
     *
     * @param string $id ID of the workload scheme
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\SetDefaultWorkloadSchemeNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\WorkloadScheme|\Psr\Http\Message\ResponseInterface
     */
    public function setDefaultWorkloadScheme(string $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\SetDefaultWorkloadScheme($id), $fetch);
    }

    /**
     * Retrieves a list of all the Members for the given Workload Scheme
     *
     * @param string $id ID of the workload scheme
     * @param array $queryParameters {
     *     @var int $offset Skip over a number of elements by specifying an offset value for the query
     *     @var int $limit Limit the number of elements on the response
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetWorkloadSchemeMembers1NotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\PageableUser|\Psr\Http\Message\ResponseInterface
     */
    public function getWorkloadSchemeMembers1(string $id, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetWorkloadSchemeMembers1($id, $queryParameters), $fetch);
    }

    /**
     * Adds a list of Users to the given Workload Scheme using the provided input
     *
     * @param string $id ID of the workload scheme
     * @param null|\Steefdw\TempoApi\Model\WorkloadSchemeMembersInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\SetWorkloadSchemeForUsersNotFoundException
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function setWorkloadSchemeForUsers(string $id, ?\Steefdw\TempoApi\Model\WorkloadSchemeMembersInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\SetWorkloadSchemeForUsers($id, $requestBody), $fetch);
    }

    /**
     * Retrieves a list of Worklogs that matches the given search parameters
     *
     * @param array $queryParameters {
     *     @var array $projectId Retrieve only worklogs for the given project ids
     *     @var array $issueId Retrieve only worklogs for the given issue ids
     *     @var string $from Retrieve results starting with this date
     *     @var string $to Retrieve results up to and including this date
     *     @var string $updatedFrom Retrieve results that have been updated from this date
     *     @var int $offset Skip over a number of elements by specifying an offset value for the query
     *     @var int $limit Limit the number of elements on the response
     *     @var string $orderBy Order results by the specified field. If no order is specified, results will by default be ordered by START_DATE_TIME and ID
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetWorklogsNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\PageableWorklog|\Psr\Http\Message\ResponseInterface
     */
    public function getWorklogs(array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetWorklogs($queryParameters), $fetch);
    }

    /**
     * Creates a new Worklog using the provided input and returns the newly created Worklog
     *
     * @param null|\Steefdw\TempoApi\Model\WorklogInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\CreateWorklogBadRequestException
     *
     * @return null|\Steefdw\TempoApi\Model\Worklog|\Psr\Http\Message\ResponseInterface
     */
    public function createWorklog(?\Steefdw\TempoApi\Model\WorklogInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\CreateWorklog($requestBody), $fetch);
    }

    /**
     * Retrieves a list of all Worklogs associated to the given Account key that matches the search parameters
     *
     * @param string $accountKey
     * @param array $queryParameters {
     *     @var string $from Retrieve results starting with this date
     *     @var string $to Retrieve results up to and including this date
     *     @var string $updatedFrom Retrieve results that have been updated from this date
     *     @var int $offset Skip over a number of elements by specifying an offset value for the query
     *     @var int $limit Limit the number of elements on the response
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetWorklogsByAccountNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\PageableWorklog|\Psr\Http\Message\ResponseInterface
     */
    public function getWorklogsByAccount(string $accountKey, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetWorklogsByAccount($accountKey, $queryParameters), $fetch);
    }

    /**
     * Retrieves a list of all Worklogs associated to the given Issue id that matches the search parameters
     *
     * @param int $issueId
     * @param array $queryParameters {
     *     @var string $from Retrieve results starting with this date
     *     @var string $to Retrieve results up to and including this date
     *     @var string $updatedFrom Retrieve results that have been updated from this date
     *     @var int $offset Skip over a number of elements by specifying an offset value for the query
     *     @var int $limit Limit the number of elements on the response
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetWorklogsByIssueIdNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\PageableWorklog|\Psr\Http\Message\ResponseInterface
     */
    public function getWorklogsByIssueId(int $issueId, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetWorklogsByIssueId($issueId, $queryParameters), $fetch);
    }

    /**
     * Retrieves a list of existing Tempo Worklog ids that match the informed Jira Worklog ids. If a Tempo Worklog Id cannot be found, it will not be returned on the results
     *
     * @param null|\Steefdw\TempoApi\Model\JiraWorklogIdList $requestBody
     * @param array $queryParameters {
     *     @var int $offset Skip over a number of elements by specifying an offset value for the query
     *     @var int $limit Limit the number of elements on the response
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\PageableWorklogIdMapper|\Psr\Http\Message\ResponseInterface
     */
    public function getTempoWorklogIdsByJiraWorklogIds(?\Steefdw\TempoApi\Model\JiraWorklogIdList $requestBody = null, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetTempoWorklogIdsByJiraWorklogIds($requestBody, $queryParameters), $fetch);
    }

    /**
     * Retrieves a list of all Worklogs associated to the given projectId that matches the search parameters
     *
     * @param int $projectId Id of the project you want to retrieve Worklogs for
     * @param array $queryParameters {
     *     @var string $from Retrieve results starting with this date
     *     @var string $to Retrieve results up to and including this date
     *     @var string $updatedFrom Retrieve results that have been updated from this date
     *     @var int $offset Skip over a number of elements by specifying an offset value for the query
     *     @var int $limit Limit the number of elements on the response
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetWorklogsByProjectIdNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\PageableWorklog|\Psr\Http\Message\ResponseInterface
     */
    public function getWorklogsByProjectId(int $projectId, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetWorklogsByProjectId($projectId, $queryParameters), $fetch);
    }

    /**
     * Retrieves a list of existing Worklogs that matches the given search parameters
     *
     * @param null|\Steefdw\TempoApi\Model\WorklogSearchInput $requestBody
     * @param array $queryParameters {
     *     @var int $offset Skip over a number of elements by specifying an offset value for the query
     *     @var int $limit Limit the number of elements on the response
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\PageableWorklog|\Psr\Http\Message\ResponseInterface
     */
    public function searchWorklogs(?\Steefdw\TempoApi\Model\WorklogSearchInput $requestBody = null, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\SearchWorklogs($requestBody, $queryParameters), $fetch);
    }

    /**
     * Retrieve a list of all Worklogs associated to the given Team id that matches the search parameters
     *
     * @param string $teamId
     * @param array $queryParameters {
     *     @var string $from Retrieve results starting with this date
     *     @var string $to Retrieve results up to and including this date
     *     @var string $updatedFrom Retrieve results that have been updated from this date
     *     @var int $offset Skip over a number of elements by specifying an offset value for the query
     *     @var int $limit Limit the number of elements on the response
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetWorklogsByTeamNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\PageableWorklog|\Psr\Http\Message\ResponseInterface
     */
    public function getWorklogsByTeam(string $teamId, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetWorklogsByTeam($teamId, $queryParameters), $fetch);
    }

    /**
     * Retrieves a list of existing Jira Worklog ids that match the informed Tempo Worklog ids. If a Jira Worklog Id cannot be found, it will not be returned on the results
     *
     * @param null|\Steefdw\TempoApi\Model\TempoWorklogIdList $requestBody
     * @param array $queryParameters {
     *     @var int $offset Skip over a number of elements by specifying an offset value for the query
     *     @var int $limit Limit the number of elements on the response
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     *
     * @return null|\Steefdw\TempoApi\Model\PageableWorklogIdMapper|\Psr\Http\Message\ResponseInterface
     */
    public function getJiraWorklogIdsByTempoWorklogIds(?\Steefdw\TempoApi\Model\TempoWorklogIdList $requestBody = null, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetJiraWorklogIdsByTempoWorklogIds($requestBody, $queryParameters), $fetch);
    }

    /**
     * Retrieves a list of all Worklogs associated to the given User that matches the search parameters
     *
     * @param string $accountId
     * @param array $queryParameters {
     *     @var string $from Retrieve results starting with this date
     *     @var string $to Retrieve results up to and including this date
     *     @var string $updatedFrom Retrieve results that have been updated from this date
     *     @var int $offset Skip over a number of elements by specifying an offset value for the query
     *     @var int $limit Limit the number of elements on the response
     * }
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetWorklogsByUserNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\PageableWorklog|\Psr\Http\Message\ResponseInterface
     */
    public function getWorklogsByUser(string $accountId, array $queryParameters = [], string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetWorklogsByUser($accountId, $queryParameters), $fetch);
    }

    /**
     * Note: This API only supports creating new Work Attribute values. Work Attribute values can only be updated as part of the Worklog.
     *
     * @param null|\Steefdw\TempoApi\Model\WorkAttributeValuesInput[] $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\CreateWorkAttributeValuesForWorklogsBadRequestException
     * @throws \Steefdw\TempoApi\Exception\CreateWorkAttributeValuesForWorklogsNotFoundException
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function createWorkAttributeValuesForWorklogs(?array $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\CreateWorkAttributeValuesForWorklogs($requestBody), $fetch);
    }

    /**
     * Retrieves a list of existing Work Attribute values that matches the given worklog ids
     *
     * @param null|\Steefdw\TempoApi\Model\WorkAttributeSearchInput $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\SearchWorkAttributeValuesForWorklogsBadRequestException
     *
     * @return null|\Steefdw\TempoApi\Model\WorkAttributeValuesByWorklog[]|\Psr\Http\Message\ResponseInterface
     */
    public function searchWorkAttributeValuesForWorklogs(?\Steefdw\TempoApi\Model\WorkAttributeSearchInput $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\SearchWorkAttributeValuesForWorklogs($requestBody), $fetch);
    }

    /**
     * Deletes an existing Worklog for the given id
     *
     * @param string $id
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\DeleteWorklogNotFoundException
     *
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function deleteWorklog(string $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\DeleteWorklog($id), $fetch);
    }

    /**
     * Retrieves an existing Worklog for the given id
     *
     * @param string $id
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetWorklogByIdNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\Worklog|\Psr\Http\Message\ResponseInterface
     */
    public function getWorklogById(string $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetWorklogById($id), $fetch);
    }

    /**
     * Updates an existing Worklog for the given id using the provided input and returns the updated Worklog
     *
     * @param string $id
     * @param null|\Steefdw\TempoApi\Model\WorklogUpdate $requestBody
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\UpdateWorklogBadRequestException
     * @throws \Steefdw\TempoApi\Exception\UpdateWorklogNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\Worklog|\Psr\Http\Message\ResponseInterface
     */
    public function updateWorklog(string $id, ?\Steefdw\TempoApi\Model\WorklogUpdate $requestBody = null, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\UpdateWorklog($id, $requestBody), $fetch);
    }

    /**
     * Retrieves a list of all Work Attribute values for the given Worklog
     *
     * @param string $id
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetWorkAttributeValuesForWorklogNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\WorkAttributeValueResults|\Psr\Http\Message\ResponseInterface
     */
    public function getWorkAttributeValuesForWorklog(string $id, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetWorkAttributeValuesForWorklog($id), $fetch);
    }

    /**
     * Retrieves a specific Work Attribute value using the provided key, for the given Worklog
     *
     * @param string $id
     * @param string $key
     * @param string $fetch Fetch mode to use (can be OBJECT or RESPONSE)
     * @throws \Steefdw\TempoApi\Exception\GetWorkAttributeValueForWorklogNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\WorkAttributeValue|\Psr\Http\Message\ResponseInterface
     */
    public function getWorkAttributeValueForWorklog(string $id, string $key, string $fetch = self::FETCH_OBJECT)
    {
        return $this->executeEndpoint(new \Steefdw\TempoApi\Endpoint\GetWorkAttributeValueForWorklog($id, $key), $fetch);
    }

    public static function create($httpClient = null, array $additionalPlugins = [], array $additionalNormalizers = [])
    {
        if (null === $httpClient) {
            $httpClient = \Http\Discovery\Psr18ClientDiscovery::find();
            $plugins = [];
            $uri = \Http\Discovery\Psr17FactoryDiscovery::findUriFactory()->createUri('https://api.tempo.io/4');
            $plugins[] = new \Http\Client\Common\Plugin\AddHostPlugin($uri);
            $plugins[] = new \Http\Client\Common\Plugin\AddPathPlugin($uri);
            if (count($additionalPlugins) > 0) {
                $plugins = array_merge($plugins, $additionalPlugins);
            }
            $httpClient = new \Http\Client\Common\PluginClient($httpClient, $plugins);
        }
        $requestFactory = \Http\Discovery\Psr17FactoryDiscovery::findRequestFactory();
        $streamFactory = \Http\Discovery\Psr17FactoryDiscovery::findStreamFactory();
        $normalizers = [new \Symfony\Component\Serializer\Normalizer\ArrayDenormalizer(), new \Steefdw\TempoApi\Normalizer\JaneObjectNormalizer()];
        if (count($additionalNormalizers) > 0) {
            $normalizers = array_merge($normalizers, $additionalNormalizers);
        }
        $serializer = new \Symfony\Component\Serializer\Serializer($normalizers, [new \Symfony\Component\Serializer\Encoder\JsonEncoder(new \Symfony\Component\Serializer\Encoder\JsonEncode(), new \Symfony\Component\Serializer\Encoder\JsonDecode(['json_decode_associative' => true]))]);

        return new static($httpClient, $requestFactory, $serializer, $streamFactory);
    }
}
