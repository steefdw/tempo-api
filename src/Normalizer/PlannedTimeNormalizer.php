<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Normalizer;

use Jane\Component\JsonSchemaRuntime\Reference;
use Steefdw\TempoApi\Runtime\Normalizer\CheckArray;
use Steefdw\TempoApi\Runtime\Normalizer\ValidatorTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class PlannedTimeNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;
    use CheckArray;
    use ValidatorTrait;

    public function supportsDenormalization($data, $type, $format = null, array $context = []): bool
    {
        return $type === \Steefdw\TempoApi\Model\PlannedTime::class;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return is_object($data) && $data::class === \Steefdw\TempoApi\Model\PlannedTime::class;
    }

    /**
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        if (isset($data['$ref'])) {
            return new Reference($data['$ref'], $context['document-origin']);
        }
        if (isset($data['$recursiveRef'])) {
            return new Reference($data['$recursiveRef'], $context['document-origin']);
        }
        $object = new \Steefdw\TempoApi\Model\PlannedTime();
        if (null === $data || false === \is_array($data)) {
            return $object;
        }
        if (\array_key_exists('days', $data)) {
            $object->setDays($this->denormalizer->denormalize($data['days'], \Steefdw\TempoApi\Model\PlannedTimeValuesPlanDay::class, 'json', $context));
            unset($data['days']);
        }
        if (\array_key_exists('metadata', $data)) {
            $object->setMetadata($this->denormalizer->denormalize($data['metadata'], \Steefdw\TempoApi\Model\PlanMetadata::class, 'json', $context));
            unset($data['metadata']);
        }
        if (\array_key_exists('periods', $data)) {
            $object->setPeriods($this->denormalizer->denormalize($data['periods'], \Steefdw\TempoApi\Model\PlannedTimeValuesPlanPeriod::class, 'json', $context));
            unset($data['periods']);
        }
        foreach ($data as $key => $value) {
            if (preg_match('/.*/', (string) $key)) {
                $object[$key] = $value;
            }
        }

        return $object;
    }

    /**
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $data = [];
        if ($object->isInitialized('days') && null !== $object->getDays()) {
            $data['days'] = $this->normalizer->normalize($object->getDays(), 'json', $context);
        }
        if ($object->isInitialized('metadata') && null !== $object->getMetadata()) {
            $data['metadata'] = $this->normalizer->normalize($object->getMetadata(), 'json', $context);
        }
        if ($object->isInitialized('periods') && null !== $object->getPeriods()) {
            $data['periods'] = $this->normalizer->normalize($object->getPeriods(), 'json', $context);
        }
        foreach ($object as $key => $value) {
            if (preg_match('/.*/', (string) $key)) {
                $data[$key] = $value;
            }
        }

        return $data;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [\Steefdw\TempoApi\Model\PlannedTime::class => false];
    }
}
