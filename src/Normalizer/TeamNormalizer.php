<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Normalizer;

use Jane\Component\JsonSchemaRuntime\Reference;
use Steefdw\TempoApi\Runtime\Normalizer\CheckArray;
use Steefdw\TempoApi\Runtime\Normalizer\ValidatorTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class TeamNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;
    use CheckArray;
    use ValidatorTrait;

    public function supportsDenormalization($data, $type, $format = null, array $context = []): bool
    {
        return $type === \Steefdw\TempoApi\Model\Team::class;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return is_object($data) && $data::class === \Steefdw\TempoApi\Model\Team::class;
    }

    /**
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        if (isset($data['$ref'])) {
            return new Reference($data['$ref'], $context['document-origin']);
        }
        if (isset($data['$recursiveRef'])) {
            return new Reference($data['$recursiveRef'], $context['document-origin']);
        }
        $object = new \Steefdw\TempoApi\Model\Team();
        if (null === $data || false === \is_array($data)) {
            return $object;
        }
        if (\array_key_exists('administrative', $data)) {
            $object->setAdministrative($data['administrative']);
            unset($data['administrative']);
        }
        if (\array_key_exists('id', $data)) {
            $object->setId($data['id']);
            unset($data['id']);
        }
        if (\array_key_exists('lead', $data)) {
            $object->setLead($this->denormalizer->denormalize($data['lead'], \Steefdw\TempoApi\Model\TeamLead::class, 'json', $context));
            unset($data['lead']);
        }
        if (\array_key_exists('links', $data)) {
            $object->setLinks($this->denormalizer->denormalize($data['links'], \Steefdw\TempoApi\Model\SelfLink::class, 'json', $context));
            unset($data['links']);
        }
        if (\array_key_exists('members', $data)) {
            $object->setMembers($this->denormalizer->denormalize($data['members'], \Steefdw\TempoApi\Model\SelfMember::class, 'json', $context));
            unset($data['members']);
        }
        if (\array_key_exists('name', $data)) {
            $object->setName($data['name']);
            unset($data['name']);
        }
        if (\array_key_exists('permissions', $data)) {
            $object->setPermissions($this->denormalizer->denormalize($data['permissions'], \Steefdw\TempoApi\Model\Team::class, 'json', $context));
            unset($data['permissions']);
        }
        if (\array_key_exists('program', $data)) {
            $object->setProgram($this->denormalizer->denormalize($data['program'], \Steefdw\TempoApi\Model\ProgramReference::class, 'json', $context));
            unset($data['program']);
        }
        if (\array_key_exists('self', $data)) {
            $object->setSelf($data['self']);
            unset($data['self']);
        }
        if (\array_key_exists('summary', $data)) {
            $object->setSummary($data['summary']);
            unset($data['summary']);
        }
        foreach ($data as $key => $value) {
            if (preg_match('/.*/', (string) $key)) {
                $object[$key] = $value;
            }
        }

        return $object;
    }

    /**
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $data = [];
        $data['administrative'] = $object->getAdministrative();
        $data['id'] = $object->getId();
        if ($object->isInitialized('lead') && null !== $object->getLead()) {
            $data['lead'] = $this->normalizer->normalize($object->getLead(), 'json', $context);
        }
        $data['links'] = $this->normalizer->normalize($object->getLinks(), 'json', $context);
        $data['members'] = $this->normalizer->normalize($object->getMembers(), 'json', $context);
        $data['name'] = $object->getName();
        $data['permissions'] = $this->normalizer->normalize($object->getPermissions(), 'json', $context);
        if ($object->isInitialized('program') && null !== $object->getProgram()) {
            $data['program'] = $this->normalizer->normalize($object->getProgram(), 'json', $context);
        }
        $data['self'] = $object->getSelf();
        if ($object->isInitialized('summary') && null !== $object->getSummary()) {
            $data['summary'] = $object->getSummary();
        }
        foreach ($object as $key => $value) {
            if (preg_match('/.*/', (string) $key)) {
                $data[$key] = $value;
            }
        }

        return $data;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [\Steefdw\TempoApi\Model\Team::class => false];
    }
}
