<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Normalizer;

use Jane\Component\JsonSchemaRuntime\Reference;
use Steefdw\TempoApi\Runtime\Normalizer\CheckArray;
use Steefdw\TempoApi\Runtime\Normalizer\ValidatorTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class DayScheduleNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;
    use CheckArray;
    use ValidatorTrait;

    public function supportsDenormalization($data, $type, $format = null, array $context = []): bool
    {
        return $type === \Steefdw\TempoApi\Model\DaySchedule::class;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return is_object($data) && $data::class === \Steefdw\TempoApi\Model\DaySchedule::class;
    }

    /**
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        if (isset($data['$ref'])) {
            return new Reference($data['$ref'], $context['document-origin']);
        }
        if (isset($data['$recursiveRef'])) {
            return new Reference($data['$recursiveRef'], $context['document-origin']);
        }
        $object = new \Steefdw\TempoApi\Model\DaySchedule();
        if (null === $data || false === \is_array($data)) {
            return $object;
        }
        if (\array_key_exists('date', $data)) {
            $object->setDate(\DateTime::createFromFormat('Y-m-d', $data['date'])->setTime(0, 0, 0));
            unset($data['date']);
        }
        if (\array_key_exists('holiday', $data)) {
            $object->setHoliday($this->denormalizer->denormalize($data['holiday'], \Steefdw\TempoApi\Model\Holiday::class, 'json', $context));
            unset($data['holiday']);
        }
        if (\array_key_exists('requiredSeconds', $data)) {
            $object->setRequiredSeconds($data['requiredSeconds']);
            unset($data['requiredSeconds']);
        }
        if (\array_key_exists('type', $data)) {
            $object->setType($data['type']);
            unset($data['type']);
        }
        foreach ($data as $key => $value) {
            if (preg_match('/.*/', (string) $key)) {
                $object[$key] = $value;
            }
        }

        return $object;
    }

    /**
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $data = [];
        $data['date'] = $object->getDate()->format('Y-m-d');
        if ($object->isInitialized('holiday') && null !== $object->getHoliday()) {
            $data['holiday'] = $this->normalizer->normalize($object->getHoliday(), 'json', $context);
        }
        $data['requiredSeconds'] = $object->getRequiredSeconds();
        $data['type'] = $object->getType();
        foreach ($object as $key => $value) {
            if (preg_match('/.*/', (string) $key)) {
                $data[$key] = $value;
            }
        }

        return $data;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [\Steefdw\TempoApi\Model\DaySchedule::class => false];
    }
}
