<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Normalizer;

use Jane\Component\JsonSchemaRuntime\Reference;
use Steefdw\TempoApi\Runtime\Normalizer\CheckArray;
use Steefdw\TempoApi\Runtime\Normalizer\ValidatorTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class PlanNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;
    use CheckArray;
    use ValidatorTrait;

    public function supportsDenormalization($data, $type, $format = null, array $context = []): bool
    {
        return $type === \Steefdw\TempoApi\Model\Plan::class;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return is_object($data) && $data::class === \Steefdw\TempoApi\Model\Plan::class;
    }

    /**
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        if (isset($data['$ref'])) {
            return new Reference($data['$ref'], $context['document-origin']);
        }
        if (isset($data['$recursiveRef'])) {
            return new Reference($data['$recursiveRef'], $context['document-origin']);
        }
        $object = new \Steefdw\TempoApi\Model\Plan();
        if (null === $data || false === \is_array($data)) {
            return $object;
        }
        if (\array_key_exists('assignee', $data)) {
            $object->setAssignee($this->denormalizer->denormalize($data['assignee'], \Steefdw\TempoApi\Model\PlanAssignee::class, 'json', $context));
            unset($data['assignee']);
        }
        if (\array_key_exists('createdAt', $data)) {
            $object->setCreatedAt($data['createdAt']);
            unset($data['createdAt']);
        }
        if (\array_key_exists('description', $data)) {
            $object->setDescription($data['description']);
            unset($data['description']);
        }
        if (\array_key_exists('endDate', $data)) {
            $object->setEndDate($data['endDate']);
            unset($data['endDate']);
        }
        if (\array_key_exists('id', $data)) {
            $object->setId($data['id']);
            unset($data['id']);
        }
        if (\array_key_exists('includeNonWorkingDays', $data)) {
            $object->setIncludeNonWorkingDays($data['includeNonWorkingDays']);
            unset($data['includeNonWorkingDays']);
        }
        if (\array_key_exists('planApproval', $data)) {
            $object->setPlanApproval($this->denormalizer->denormalize($data['planApproval'], \Steefdw\TempoApi\Model\PlanApproval::class, 'json', $context));
            unset($data['planApproval']);
        }
        if (\array_key_exists('planItem', $data)) {
            $object->setPlanItem($this->denormalizer->denormalize($data['planItem'], \Steefdw\TempoApi\Model\PlanItem::class, 'json', $context));
            unset($data['planItem']);
        }
        if (\array_key_exists('plannedSecondsPerDay', $data)) {
            $object->setPlannedSecondsPerDay($data['plannedSecondsPerDay']);
            unset($data['plannedSecondsPerDay']);
        }
        if (\array_key_exists('plannedTime', $data)) {
            $object->setPlannedTime($this->denormalizer->denormalize($data['plannedTime'], \Steefdw\TempoApi\Model\PlannedTime::class, 'json', $context));
            unset($data['plannedTime']);
        }
        if (\array_key_exists('recurrenceEndDate', $data)) {
            $object->setRecurrenceEndDate($data['recurrenceEndDate']);
            unset($data['recurrenceEndDate']);
        }
        if (\array_key_exists('rule', $data)) {
            $object->setRule($data['rule']);
            unset($data['rule']);
        }
        if (\array_key_exists('self', $data)) {
            $object->setSelf($data['self']);
            unset($data['self']);
        }
        if (\array_key_exists('startDate', $data)) {
            $object->setStartDate($data['startDate']);
            unset($data['startDate']);
        }
        if (\array_key_exists('startTime', $data)) {
            $object->setStartTime($data['startTime']);
            unset($data['startTime']);
        }
        if (\array_key_exists('totalPlannedSecondsInScope', $data)) {
            $object->setTotalPlannedSecondsInScope($data['totalPlannedSecondsInScope']);
            unset($data['totalPlannedSecondsInScope']);
        }
        if (\array_key_exists('updatedAt', $data)) {
            $object->setUpdatedAt($data['updatedAt']);
            unset($data['updatedAt']);
        }
        foreach ($data as $key => $value) {
            if (preg_match('/.*/', (string) $key)) {
                $object[$key] = $value;
            }
        }

        return $object;
    }

    /**
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $data = [];
        if ($object->isInitialized('assignee') && null !== $object->getAssignee()) {
            $data['assignee'] = $this->normalizer->normalize($object->getAssignee(), 'json', $context);
        }
        if ($object->isInitialized('createdAt') && null !== $object->getCreatedAt()) {
            $data['createdAt'] = $object->getCreatedAt();
        }
        if ($object->isInitialized('description') && null !== $object->getDescription()) {
            $data['description'] = $object->getDescription();
        }
        if ($object->isInitialized('endDate') && null !== $object->getEndDate()) {
            $data['endDate'] = $object->getEndDate();
        }
        if ($object->isInitialized('id') && null !== $object->getId()) {
            $data['id'] = $object->getId();
        }
        if ($object->isInitialized('includeNonWorkingDays') && null !== $object->getIncludeNonWorkingDays()) {
            $data['includeNonWorkingDays'] = $object->getIncludeNonWorkingDays();
        }
        if ($object->isInitialized('planApproval') && null !== $object->getPlanApproval()) {
            $data['planApproval'] = $this->normalizer->normalize($object->getPlanApproval(), 'json', $context);
        }
        if ($object->isInitialized('planItem') && null !== $object->getPlanItem()) {
            $data['planItem'] = $this->normalizer->normalize($object->getPlanItem(), 'json', $context);
        }
        if ($object->isInitialized('plannedSecondsPerDay') && null !== $object->getPlannedSecondsPerDay()) {
            $data['plannedSecondsPerDay'] = $object->getPlannedSecondsPerDay();
        }
        if ($object->isInitialized('plannedTime') && null !== $object->getPlannedTime()) {
            $data['plannedTime'] = $this->normalizer->normalize($object->getPlannedTime(), 'json', $context);
        }
        if ($object->isInitialized('recurrenceEndDate') && null !== $object->getRecurrenceEndDate()) {
            $data['recurrenceEndDate'] = $object->getRecurrenceEndDate();
        }
        if ($object->isInitialized('rule') && null !== $object->getRule()) {
            $data['rule'] = $object->getRule();
        }
        $data['self'] = $object->getSelf();
        if ($object->isInitialized('startDate') && null !== $object->getStartDate()) {
            $data['startDate'] = $object->getStartDate();
        }
        if ($object->isInitialized('startTime') && null !== $object->getStartTime()) {
            $data['startTime'] = $object->getStartTime();
        }
        if ($object->isInitialized('totalPlannedSecondsInScope') && null !== $object->getTotalPlannedSecondsInScope()) {
            $data['totalPlannedSecondsInScope'] = $object->getTotalPlannedSecondsInScope();
        }
        if ($object->isInitialized('updatedAt') && null !== $object->getUpdatedAt()) {
            $data['updatedAt'] = $object->getUpdatedAt();
        }
        foreach ($object as $key => $value) {
            if (preg_match('/.*/', (string) $key)) {
                $data[$key] = $value;
            }
        }

        return $data;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [\Steefdw\TempoApi\Model\Plan::class => false];
    }
}
