<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Normalizer;

use Jane\Component\JsonSchemaRuntime\Reference;
use Steefdw\TempoApi\Runtime\Normalizer\CheckArray;
use Steefdw\TempoApi\Runtime\Normalizer\ValidatorTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class AccountSearchInputNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;
    use CheckArray;
    use ValidatorTrait;

    public function supportsDenormalization($data, $type, $format = null, array $context = []): bool
    {
        return $type === \Steefdw\TempoApi\Model\AccountSearchInput::class;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return is_object($data) && $data::class === \Steefdw\TempoApi\Model\AccountSearchInput::class;
    }

    /**
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        if (isset($data['$ref'])) {
            return new Reference($data['$ref'], $context['document-origin']);
        }
        if (isset($data['$recursiveRef'])) {
            return new Reference($data['$recursiveRef'], $context['document-origin']);
        }
        $object = new \Steefdw\TempoApi\Model\AccountSearchInput();
        if (null === $data || false === \is_array($data)) {
            return $object;
        }
        if (\array_key_exists('global', $data)) {
            $object->setGlobal($data['global']);
            unset($data['global']);
        }
        if (\array_key_exists('ids', $data)) {
            $values = [];
            foreach ($data['ids'] as $value) {
                $values[] = $value;
            }
            $object->setIds($values);
            unset($data['ids']);
        }
        if (\array_key_exists('keys', $data)) {
            $values_1 = [];
            foreach ($data['keys'] as $value_1) {
                $values_1[] = $value_1;
            }
            $object->setKeys($values_1);
            unset($data['keys']);
        }
        if (\array_key_exists('statuses', $data)) {
            $values_2 = [];
            foreach ($data['statuses'] as $value_2) {
                $values_2[] = $value_2;
            }
            $object->setStatuses($values_2);
            unset($data['statuses']);
        }
        foreach ($data as $key => $value_3) {
            if (preg_match('/.*/', (string) $key)) {
                $object[$key] = $value_3;
            }
        }

        return $object;
    }

    /**
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $data = [];
        if ($object->isInitialized('global') && null !== $object->getGlobal()) {
            $data['global'] = $object->getGlobal();
        }
        if ($object->isInitialized('ids') && null !== $object->getIds()) {
            $values = [];
            foreach ($object->getIds() as $value) {
                $values[] = $value;
            }
            $data['ids'] = $values;
        }
        if ($object->isInitialized('keys') && null !== $object->getKeys()) {
            $values_1 = [];
            foreach ($object->getKeys() as $value_1) {
                $values_1[] = $value_1;
            }
            $data['keys'] = $values_1;
        }
        if ($object->isInitialized('statuses') && null !== $object->getStatuses()) {
            $values_2 = [];
            foreach ($object->getStatuses() as $value_2) {
                $values_2[] = $value_2;
            }
            $data['statuses'] = $values_2;
        }
        foreach ($object as $key => $value_3) {
            if (preg_match('/.*/', (string) $key)) {
                $data[$key] = $value_3;
            }
        }

        return $data;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [\Steefdw\TempoApi\Model\AccountSearchInput::class => false];
    }
}
