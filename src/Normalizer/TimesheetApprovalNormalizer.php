<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Normalizer;

use Jane\Component\JsonSchemaRuntime\Reference;
use Steefdw\TempoApi\Runtime\Normalizer\CheckArray;
use Steefdw\TempoApi\Runtime\Normalizer\ValidatorTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class TimesheetApprovalNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;
    use CheckArray;
    use ValidatorTrait;

    public function supportsDenormalization($data, $type, $format = null, array $context = []): bool
    {
        return $type === \Steefdw\TempoApi\Model\TimesheetApproval::class;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return is_object($data) && $data::class === \Steefdw\TempoApi\Model\TimesheetApproval::class;
    }

    /**
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        if (isset($data['$ref'])) {
            return new Reference($data['$ref'], $context['document-origin']);
        }
        if (isset($data['$recursiveRef'])) {
            return new Reference($data['$recursiveRef'], $context['document-origin']);
        }
        $object = new \Steefdw\TempoApi\Model\TimesheetApproval();
        if (null === $data || false === \is_array($data)) {
            return $object;
        }
        if (\array_key_exists('actions', $data)) {
            $object->setActions($this->denormalizer->denormalize($data['actions'], \Steefdw\TempoApi\Model\TimesheetApprovalAvailableActions::class, 'json', $context));
            unset($data['actions']);
        }
        if (\array_key_exists('period', $data)) {
            $object->setPeriod($this->denormalizer->denormalize($data['period'], \Steefdw\TempoApi\Model\TimesheetApprovalPeriod::class, 'json', $context));
            unset($data['period']);
        }
        if (\array_key_exists('requiredSeconds', $data)) {
            $object->setRequiredSeconds($data['requiredSeconds']);
            unset($data['requiredSeconds']);
        }
        if (\array_key_exists('reviewer', $data)) {
            $object->setReviewer($this->denormalizer->denormalize($data['reviewer'], \Steefdw\TempoApi\Model\TimesheetApprovalReviewer::class, 'json', $context));
            unset($data['reviewer']);
        }
        if (\array_key_exists('self', $data)) {
            $object->setSelf($data['self']);
            unset($data['self']);
        }
        if (\array_key_exists('status', $data)) {
            $object->setStatus($this->denormalizer->denormalize($data['status'], \Steefdw\TempoApi\Model\TimesheetApprovalStatus::class, 'json', $context));
            unset($data['status']);
        }
        if (\array_key_exists('timeSpentSeconds', $data)) {
            $object->setTimeSpentSeconds($data['timeSpentSeconds']);
            unset($data['timeSpentSeconds']);
        }
        if (\array_key_exists('user', $data)) {
            $object->setUser($this->denormalizer->denormalize($data['user'], \Steefdw\TempoApi\Model\TimesheetApprovalUser::class, 'json', $context));
            unset($data['user']);
        }
        if (\array_key_exists('worklogs', $data)) {
            $object->setWorklogs($this->denormalizer->denormalize($data['worklogs'], \Steefdw\TempoApi\Model\SelfApiBean::class, 'json', $context));
            unset($data['worklogs']);
        }
        foreach ($data as $key => $value) {
            if (preg_match('/.*/', (string) $key)) {
                $object[$key] = $value;
            }
        }

        return $object;
    }

    /**
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $data = [];
        $data['actions'] = $this->normalizer->normalize($object->getActions(), 'json', $context);
        $data['period'] = $this->normalizer->normalize($object->getPeriod(), 'json', $context);
        $data['requiredSeconds'] = $object->getRequiredSeconds();
        if ($object->isInitialized('reviewer') && null !== $object->getReviewer()) {
            $data['reviewer'] = $this->normalizer->normalize($object->getReviewer(), 'json', $context);
        }
        $data['self'] = $object->getSelf();
        if ($object->isInitialized('status') && null !== $object->getStatus()) {
            $data['status'] = $this->normalizer->normalize($object->getStatus(), 'json', $context);
        }
        $data['timeSpentSeconds'] = $object->getTimeSpentSeconds();
        $data['user'] = $this->normalizer->normalize($object->getUser(), 'json', $context);
        $data['worklogs'] = $this->normalizer->normalize($object->getWorklogs(), 'json', $context);
        foreach ($object as $key => $value) {
            if (preg_match('/.*/', (string) $key)) {
                $data[$key] = $value;
            }
        }

        return $data;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [\Steefdw\TempoApi\Model\TimesheetApproval::class => false];
    }
}
