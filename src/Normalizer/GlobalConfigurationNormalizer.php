<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Normalizer;

use Jane\Component\JsonSchemaRuntime\Reference;
use Steefdw\TempoApi\Runtime\Normalizer\CheckArray;
use Steefdw\TempoApi\Runtime\Normalizer\ValidatorTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class GlobalConfigurationNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;
    use CheckArray;
    use ValidatorTrait;

    public function supportsDenormalization($data, $type, $format = null, array $context = []): bool
    {
        return $type === \Steefdw\TempoApi\Model\GlobalConfiguration::class;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return is_object($data) && $data::class === \Steefdw\TempoApi\Model\GlobalConfiguration::class;
    }

    /**
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        if (isset($data['$ref'])) {
            return new Reference($data['$ref'], $context['document-origin']);
        }
        if (isset($data['$recursiveRef'])) {
            return new Reference($data['$recursiveRef'], $context['document-origin']);
        }
        $object = new \Steefdw\TempoApi\Model\GlobalConfiguration();
        if (null === $data || false === \is_array($data)) {
            return $object;
        }
        if (\array_key_exists('allowLoggingOnClosedAccount', $data)) {
            $object->setAllowLoggingOnClosedAccount($data['allowLoggingOnClosedAccount']);
            unset($data['allowLoggingOnClosedAccount']);
        }
        if (\array_key_exists('approvalPeriod', $data)) {
            $object->setApprovalPeriod($data['approvalPeriod']);
            unset($data['approvalPeriod']);
        }
        if (\array_key_exists('approvalWeekStart', $data)) {
            $object->setApprovalWeekStart($data['approvalWeekStart']);
            unset($data['approvalWeekStart']);
        }
        if (\array_key_exists('maxHoursPerDayPerUser', $data)) {
            $object->setMaxHoursPerDayPerUser($data['maxHoursPerDayPerUser']);
            unset($data['maxHoursPerDayPerUser']);
        }
        if (\array_key_exists('numberOfDaysAllowedIntoFuture', $data)) {
            $object->setNumberOfDaysAllowedIntoFuture($data['numberOfDaysAllowedIntoFuture']);
            unset($data['numberOfDaysAllowedIntoFuture']);
        }
        if (\array_key_exists('planApprovalEnabled', $data)) {
            $object->setPlanApprovalEnabled($data['planApprovalEnabled']);
            unset($data['planApprovalEnabled']);
        }
        if (\array_key_exists('remainingEstimateOptional', $data)) {
            $object->setRemainingEstimateOptional($data['remainingEstimateOptional']);
            unset($data['remainingEstimateOptional']);
        }
        if (\array_key_exists('startAndEndTimesEnabled', $data)) {
            $object->setStartAndEndTimesEnabled($data['startAndEndTimesEnabled']);
            unset($data['startAndEndTimesEnabled']);
        }
        if (\array_key_exists('startAndEndTimesForPlanningEnabled', $data)) {
            $object->setStartAndEndTimesForPlanningEnabled($data['startAndEndTimesForPlanningEnabled']);
            unset($data['startAndEndTimesForPlanningEnabled']);
        }
        if (\array_key_exists('weekStart', $data)) {
            $object->setWeekStart($data['weekStart']);
            unset($data['weekStart']);
        }
        if (\array_key_exists('worklogDescriptionOptional', $data)) {
            $object->setWorklogDescriptionOptional($data['worklogDescriptionOptional']);
            unset($data['worklogDescriptionOptional']);
        }
        foreach ($data as $key => $value) {
            if (preg_match('/.*/', (string) $key)) {
                $object[$key] = $value;
            }
        }

        return $object;
    }

    /**
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $data = [];
        $data['allowLoggingOnClosedAccount'] = $object->getAllowLoggingOnClosedAccount();
        $data['approvalPeriod'] = $object->getApprovalPeriod();
        $data['approvalWeekStart'] = $object->getApprovalWeekStart();
        if ($object->isInitialized('maxHoursPerDayPerUser') && null !== $object->getMaxHoursPerDayPerUser()) {
            $data['maxHoursPerDayPerUser'] = $object->getMaxHoursPerDayPerUser();
        }
        $data['numberOfDaysAllowedIntoFuture'] = $object->getNumberOfDaysAllowedIntoFuture();
        $data['planApprovalEnabled'] = $object->getPlanApprovalEnabled();
        $data['remainingEstimateOptional'] = $object->getRemainingEstimateOptional();
        $data['startAndEndTimesEnabled'] = $object->getStartAndEndTimesEnabled();
        $data['startAndEndTimesForPlanningEnabled'] = $object->getStartAndEndTimesForPlanningEnabled();
        $data['weekStart'] = $object->getWeekStart();
        $data['worklogDescriptionOptional'] = $object->getWorklogDescriptionOptional();
        foreach ($object as $key => $value) {
            if (preg_match('/.*/', (string) $key)) {
                $data[$key] = $value;
            }
        }

        return $data;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [\Steefdw\TempoApi\Model\GlobalConfiguration::class => false];
    }
}
