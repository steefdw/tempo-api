<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Normalizer;

use Jane\Component\JsonSchemaRuntime\Reference;
use Steefdw\TempoApi\Runtime\Normalizer\CheckArray;
use Steefdw\TempoApi\Runtime\Normalizer\ValidatorTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class WorklogInputNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;
    use CheckArray;
    use ValidatorTrait;

    public function supportsDenormalization($data, $type, $format = null, array $context = []): bool
    {
        return $type === \Steefdw\TempoApi\Model\WorklogInput::class;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return is_object($data) && $data::class === \Steefdw\TempoApi\Model\WorklogInput::class;
    }

    /**
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        if (isset($data['$ref'])) {
            return new Reference($data['$ref'], $context['document-origin']);
        }
        if (isset($data['$recursiveRef'])) {
            return new Reference($data['$recursiveRef'], $context['document-origin']);
        }
        $object = new \Steefdw\TempoApi\Model\WorklogInput();
        if (null === $data || false === \is_array($data)) {
            return $object;
        }
        if (\array_key_exists('attributes', $data)) {
            $values = [];
            foreach ($data['attributes'] as $value) {
                $values[] = $this->denormalizer->denormalize($value, \Steefdw\TempoApi\Model\WorkAttributeValueInput::class, 'json', $context);
            }
            $object->setAttributes($values);
            unset($data['attributes']);
        }
        if (\array_key_exists('authorAccountId', $data)) {
            $object->setAuthorAccountId($data['authorAccountId']);
            unset($data['authorAccountId']);
        }
        if (\array_key_exists('billableSeconds', $data)) {
            $object->setBillableSeconds($data['billableSeconds']);
            unset($data['billableSeconds']);
        }
        if (\array_key_exists('description', $data)) {
            $object->setDescription($data['description']);
            unset($data['description']);
        }
        if (\array_key_exists('issueId', $data)) {
            $object->setIssueId($data['issueId']);
            unset($data['issueId']);
        }
        if (\array_key_exists('remainingEstimateSeconds', $data)) {
            $object->setRemainingEstimateSeconds($data['remainingEstimateSeconds']);
            unset($data['remainingEstimateSeconds']);
        }
        if (\array_key_exists('startDate', $data)) {
            $object->setStartDate(\DateTime::createFromFormat('Y-m-d', $data['startDate'])->setTime(0, 0, 0));
            unset($data['startDate']);
        }
        if (\array_key_exists('startTime', $data)) {
            $object->setStartTime($data['startTime']);
            unset($data['startTime']);
        }
        if (\array_key_exists('timeSpentSeconds', $data)) {
            $object->setTimeSpentSeconds($data['timeSpentSeconds']);
            unset($data['timeSpentSeconds']);
        }
        foreach ($data as $key => $value_1) {
            if (preg_match('/.*/', (string) $key)) {
                $object[$key] = $value_1;
            }
        }

        return $object;
    }

    /**
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $data = [];
        if ($object->isInitialized('attributes') && null !== $object->getAttributes()) {
            $values = [];
            foreach ($object->getAttributes() as $value) {
                $values[] = $this->normalizer->normalize($value, 'json', $context);
            }
            $data['attributes'] = $values;
        }
        $data['authorAccountId'] = $object->getAuthorAccountId();
        if ($object->isInitialized('billableSeconds') && null !== $object->getBillableSeconds()) {
            $data['billableSeconds'] = $object->getBillableSeconds();
        }
        if ($object->isInitialized('description') && null !== $object->getDescription()) {
            $data['description'] = $object->getDescription();
        }
        $data['issueId'] = $object->getIssueId();
        if ($object->isInitialized('remainingEstimateSeconds') && null !== $object->getRemainingEstimateSeconds()) {
            $data['remainingEstimateSeconds'] = $object->getRemainingEstimateSeconds();
        }
        $data['startDate'] = $object->getStartDate()->format('Y-m-d');
        if ($object->isInitialized('startTime') && null !== $object->getStartTime()) {
            $data['startTime'] = $object->getStartTime();
        }
        $data['timeSpentSeconds'] = $object->getTimeSpentSeconds();
        foreach ($object as $key => $value_1) {
            if (preg_match('/.*/', (string) $key)) {
                $data[$key] = $value_1;
            }
        }

        return $data;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [\Steefdw\TempoApi\Model\WorklogInput::class => false];
    }
}
