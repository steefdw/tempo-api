<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Normalizer;

use Jane\Component\JsonSchemaRuntime\Reference;
use Steefdw\TempoApi\Runtime\Normalizer\CheckArray;
use Steefdw\TempoApi\Runtime\Normalizer\ValidatorTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class GenericResourceTeamMemberNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;
    use CheckArray;
    use ValidatorTrait;

    public function supportsDenormalization($data, $type, $format = null, array $context = []): bool
    {
        return $type === \Steefdw\TempoApi\Model\GenericResourceTeamMember::class;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return is_object($data) && $data::class === \Steefdw\TempoApi\Model\GenericResourceTeamMember::class;
    }

    /**
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        if (isset($data['$ref'])) {
            return new Reference($data['$ref'], $context['document-origin']);
        }
        if (isset($data['$recursiveRef'])) {
            return new Reference($data['$recursiveRef'], $context['document-origin']);
        }
        $object = new \Steefdw\TempoApi\Model\GenericResourceTeamMember();
        if (null === $data || false === \is_array($data)) {
            return $object;
        }
        if (\array_key_exists('assignedToTeamAt', $data)) {
            $object->setAssignedToTeamAt($data['assignedToTeamAt']);
            unset($data['assignedToTeamAt']);
        }
        if (\array_key_exists('assignedToTeamBy', $data)) {
            $object->setAssignedToTeamBy($this->denormalizer->denormalize($data['assignedToTeamBy'], \Steefdw\TempoApi\Model\UserRefBean::class, 'json', $context));
            unset($data['assignedToTeamBy']);
        }
        if (\array_key_exists('member', $data)) {
            $object->setMember($this->denormalizer->denormalize($data['member'], \Steefdw\TempoApi\Model\GenericResourceReference::class, 'json', $context));
            unset($data['member']);
        }
        if (\array_key_exists('self', $data)) {
            $object->setSelf($data['self']);
            unset($data['self']);
        }
        if (\array_key_exists('team', $data)) {
            $object->setTeam($this->denormalizer->denormalize($data['team'], \Steefdw\TempoApi\Model\TeamReference::class, 'json', $context));
            unset($data['team']);
        }
        foreach ($data as $key => $value) {
            if (preg_match('/.*/', (string) $key)) {
                $object[$key] = $value;
            }
        }

        return $object;
    }

    /**
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $data = [];
        $data['assignedToTeamAt'] = $object->getAssignedToTeamAt();
        $data['assignedToTeamBy'] = $this->normalizer->normalize($object->getAssignedToTeamBy(), 'json', $context);
        $data['member'] = $this->normalizer->normalize($object->getMember(), 'json', $context);
        $data['self'] = $object->getSelf();
        $data['team'] = $this->normalizer->normalize($object->getTeam(), 'json', $context);
        foreach ($object as $key => $value) {
            if (preg_match('/.*/', (string) $key)) {
                $data[$key] = $value;
            }
        }

        return $data;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [\Steefdw\TempoApi\Model\GenericResourceTeamMember::class => false];
    }
}
