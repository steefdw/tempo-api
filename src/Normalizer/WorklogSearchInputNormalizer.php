<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Normalizer;

use Jane\Component\JsonSchemaRuntime\Reference;
use Steefdw\TempoApi\Runtime\Normalizer\CheckArray;
use Steefdw\TempoApi\Runtime\Normalizer\ValidatorTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class WorklogSearchInputNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;
    use CheckArray;
    use ValidatorTrait;

    public function supportsDenormalization($data, $type, $format = null, array $context = []): bool
    {
        return $type === \Steefdw\TempoApi\Model\WorklogSearchInput::class;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return is_object($data) && $data::class === \Steefdw\TempoApi\Model\WorklogSearchInput::class;
    }

    /**
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        if (isset($data['$ref'])) {
            return new Reference($data['$ref'], $context['document-origin']);
        }
        if (isset($data['$recursiveRef'])) {
            return new Reference($data['$recursiveRef'], $context['document-origin']);
        }
        $object = new \Steefdw\TempoApi\Model\WorklogSearchInput();
        if (null === $data || false === \is_array($data)) {
            return $object;
        }
        if (\array_key_exists('authorIds', $data)) {
            $values = [];
            foreach ($data['authorIds'] as $value) {
                $values[] = $value;
            }
            $object->setAuthorIds($values);
            unset($data['authorIds']);
        }
        if (\array_key_exists('from', $data)) {
            $object->setFrom($data['from']);
            unset($data['from']);
        }
        if (\array_key_exists('issueIds', $data)) {
            $values_1 = [];
            foreach ($data['issueIds'] as $value_1) {
                $values_1[] = $value_1;
            }
            $object->setIssueIds($values_1);
            unset($data['issueIds']);
        }
        if (\array_key_exists('projectIds', $data)) {
            $values_2 = [];
            foreach ($data['projectIds'] as $value_2) {
                $values_2[] = $value_2;
            }
            $object->setProjectIds($values_2);
            unset($data['projectIds']);
        }
        if (\array_key_exists('to', $data)) {
            $object->setTo($data['to']);
            unset($data['to']);
        }
        if (\array_key_exists('updatedFrom', $data)) {
            $object->setUpdatedFrom($data['updatedFrom']);
            unset($data['updatedFrom']);
        }
        foreach ($data as $key => $value_3) {
            if (preg_match('/.*/', (string) $key)) {
                $object[$key] = $value_3;
            }
        }

        return $object;
    }

    /**
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $data = [];
        if ($object->isInitialized('authorIds') && null !== $object->getAuthorIds()) {
            $values = [];
            foreach ($object->getAuthorIds() as $value) {
                $values[] = $value;
            }
            $data['authorIds'] = $values;
        }
        if ($object->isInitialized('from') && null !== $object->getFrom()) {
            $data['from'] = $object->getFrom();
        }
        if ($object->isInitialized('issueIds') && null !== $object->getIssueIds()) {
            $values_1 = [];
            foreach ($object->getIssueIds() as $value_1) {
                $values_1[] = $value_1;
            }
            $data['issueIds'] = $values_1;
        }
        if ($object->isInitialized('projectIds') && null !== $object->getProjectIds()) {
            $values_2 = [];
            foreach ($object->getProjectIds() as $value_2) {
                $values_2[] = $value_2;
            }
            $data['projectIds'] = $values_2;
        }
        if ($object->isInitialized('to') && null !== $object->getTo()) {
            $data['to'] = $object->getTo();
        }
        if ($object->isInitialized('updatedFrom') && null !== $object->getUpdatedFrom()) {
            $data['updatedFrom'] = $object->getUpdatedFrom();
        }
        foreach ($object as $key => $value_3) {
            if (preg_match('/.*/', (string) $key)) {
                $data[$key] = $value_3;
            }
        }

        return $data;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [\Steefdw\TempoApi\Model\WorklogSearchInput::class => false];
    }
}
