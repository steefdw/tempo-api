<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Normalizer;

use Jane\Component\JsonSchemaRuntime\Reference;
use Steefdw\TempoApi\Runtime\Normalizer\CheckArray;
use Steefdw\TempoApi\Runtime\Normalizer\ValidatorTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class PlanInputNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;
    use CheckArray;
    use ValidatorTrait;

    public function supportsDenormalization($data, $type, $format = null, array $context = []): bool
    {
        return $type === \Steefdw\TempoApi\Model\PlanInput::class;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return is_object($data) && $data::class === \Steefdw\TempoApi\Model\PlanInput::class;
    }

    /**
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        if (isset($data['$ref'])) {
            return new Reference($data['$ref'], $context['document-origin']);
        }
        if (isset($data['$recursiveRef'])) {
            return new Reference($data['$recursiveRef'], $context['document-origin']);
        }
        $object = new \Steefdw\TempoApi\Model\PlanInput();
        if (null === $data || false === \is_array($data)) {
            return $object;
        }
        if (\array_key_exists('assigneeId', $data)) {
            $object->setAssigneeId($data['assigneeId']);
            unset($data['assigneeId']);
        }
        if (\array_key_exists('assigneeType', $data)) {
            $object->setAssigneeType($data['assigneeType']);
            unset($data['assigneeType']);
        }
        if (\array_key_exists('description', $data)) {
            $object->setDescription($data['description']);
            unset($data['description']);
        }
        if (\array_key_exists('endDate', $data)) {
            $object->setEndDate($data['endDate']);
            unset($data['endDate']);
        }
        if (\array_key_exists('includeNonWorkingDays', $data)) {
            $object->setIncludeNonWorkingDays($data['includeNonWorkingDays']);
            unset($data['includeNonWorkingDays']);
        }
        if (\array_key_exists('planApproval', $data)) {
            $object->setPlanApproval($this->denormalizer->denormalize($data['planApproval'], \Steefdw\TempoApi\Model\PlanApprovalInput::class, 'json', $context));
            unset($data['planApproval']);
        }
        if (\array_key_exists('planItemId', $data)) {
            $object->setPlanItemId($data['planItemId']);
            unset($data['planItemId']);
        }
        if (\array_key_exists('planItemType', $data)) {
            $object->setPlanItemType($data['planItemType']);
            unset($data['planItemType']);
        }
        if (\array_key_exists('plannedSecondsPerDay', $data)) {
            $object->setPlannedSecondsPerDay($data['plannedSecondsPerDay']);
            unset($data['plannedSecondsPerDay']);
        }
        if (\array_key_exists('recurrenceEndDate', $data)) {
            $object->setRecurrenceEndDate($data['recurrenceEndDate']);
            unset($data['recurrenceEndDate']);
        }
        if (\array_key_exists('rule', $data)) {
            $object->setRule($data['rule']);
            unset($data['rule']);
        }
        if (\array_key_exists('startDate', $data)) {
            $object->setStartDate($data['startDate']);
            unset($data['startDate']);
        }
        if (\array_key_exists('startTime', $data)) {
            $object->setStartTime($data['startTime']);
            unset($data['startTime']);
        }
        foreach ($data as $key => $value) {
            if (preg_match('/.*/', (string) $key)) {
                $object[$key] = $value;
            }
        }

        return $object;
    }

    /**
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $data = [];
        $data['assigneeId'] = $object->getAssigneeId();
        $data['assigneeType'] = $object->getAssigneeType();
        if ($object->isInitialized('description') && null !== $object->getDescription()) {
            $data['description'] = $object->getDescription();
        }
        $data['endDate'] = $object->getEndDate();
        if ($object->isInitialized('includeNonWorkingDays') && null !== $object->getIncludeNonWorkingDays()) {
            $data['includeNonWorkingDays'] = $object->getIncludeNonWorkingDays();
        }
        if ($object->isInitialized('planApproval') && null !== $object->getPlanApproval()) {
            $data['planApproval'] = $this->normalizer->normalize($object->getPlanApproval(), 'json', $context);
        }
        $data['planItemId'] = $object->getPlanItemId();
        $data['planItemType'] = $object->getPlanItemType();
        $data['plannedSecondsPerDay'] = $object->getPlannedSecondsPerDay();
        if ($object->isInitialized('recurrenceEndDate') && null !== $object->getRecurrenceEndDate()) {
            $data['recurrenceEndDate'] = $object->getRecurrenceEndDate();
        }
        if ($object->isInitialized('rule') && null !== $object->getRule()) {
            $data['rule'] = $object->getRule();
        }
        $data['startDate'] = $object->getStartDate();
        if ($object->isInitialized('startTime') && null !== $object->getStartTime()) {
            $data['startTime'] = $object->getStartTime();
        }
        foreach ($object as $key => $value) {
            if (preg_match('/.*/', (string) $key)) {
                $data[$key] = $value;
            }
        }

        return $data;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [\Steefdw\TempoApi\Model\PlanInput::class => false];
    }
}
