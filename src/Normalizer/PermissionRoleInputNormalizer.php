<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Normalizer;

use Jane\Component\JsonSchemaRuntime\Reference;
use Steefdw\TempoApi\Runtime\Normalizer\CheckArray;
use Steefdw\TempoApi\Runtime\Normalizer\ValidatorTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class PermissionRoleInputNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;
    use CheckArray;
    use ValidatorTrait;

    public function supportsDenormalization($data, $type, $format = null, array $context = []): bool
    {
        return $type === \Steefdw\TempoApi\Model\PermissionRoleInput::class;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return is_object($data) && $data::class === \Steefdw\TempoApi\Model\PermissionRoleInput::class;
    }

    /**
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        if (isset($data['$ref'])) {
            return new Reference($data['$ref'], $context['document-origin']);
        }
        if (isset($data['$recursiveRef'])) {
            return new Reference($data['$recursiveRef'], $context['document-origin']);
        }
        $object = new \Steefdw\TempoApi\Model\PermissionRoleInput();
        if (null === $data || false === \is_array($data)) {
            return $object;
        }
        if (\array_key_exists('accessEntityIds', $data)) {
            $values = [];
            foreach ($data['accessEntityIds'] as $value) {
                $values[] = $value;
            }
            $object->setAccessEntityIds($values);
            unset($data['accessEntityIds']);
        }
        if (\array_key_exists('accessType', $data)) {
            $object->setAccessType($data['accessType']);
            unset($data['accessType']);
        }
        if (\array_key_exists('name', $data)) {
            $object->setName($data['name']);
            unset($data['name']);
        }
        if (\array_key_exists('permissionKeys', $data)) {
            $values_1 = [];
            foreach ($data['permissionKeys'] as $value_1) {
                $values_1[] = $value_1;
            }
            $object->setPermissionKeys($values_1);
            unset($data['permissionKeys']);
        }
        if (\array_key_exists('permittedAccountIds', $data)) {
            $values_2 = [];
            foreach ($data['permittedAccountIds'] as $value_2) {
                $values_2[] = $value_2;
            }
            $object->setPermittedAccountIds($values_2);
            unset($data['permittedAccountIds']);
        }
        foreach ($data as $key => $value_3) {
            if (preg_match('/.*/', (string) $key)) {
                $object[$key] = $value_3;
            }
        }

        return $object;
    }

    /**
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $data = [];
        if ($object->isInitialized('accessEntityIds') && null !== $object->getAccessEntityIds()) {
            $values = [];
            foreach ($object->getAccessEntityIds() as $value) {
                $values[] = $value;
            }
            $data['accessEntityIds'] = $values;
        }
        $data['accessType'] = $object->getAccessType();
        $data['name'] = $object->getName();
        if ($object->isInitialized('permissionKeys') && null !== $object->getPermissionKeys()) {
            $values_1 = [];
            foreach ($object->getPermissionKeys() as $value_1) {
                $values_1[] = $value_1;
            }
            $data['permissionKeys'] = $values_1;
        }
        if ($object->isInitialized('permittedAccountIds') && null !== $object->getPermittedAccountIds()) {
            $values_2 = [];
            foreach ($object->getPermittedAccountIds() as $value_2) {
                $values_2[] = $value_2;
            }
            $data['permittedAccountIds'] = $values_2;
        }
        foreach ($object as $key => $value_3) {
            if (preg_match('/.*/', (string) $key)) {
                $data[$key] = $value_3;
            }
        }

        return $data;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [\Steefdw\TempoApi\Model\PermissionRoleInput::class => false];
    }
}
