<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Normalizer;

use Jane\Component\JsonSchemaRuntime\Reference;
use Steefdw\TempoApi\Runtime\Normalizer\CheckArray;
use Steefdw\TempoApi\Runtime\Normalizer\ValidatorTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class WorkAttributeValuesByWorklogNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;
    use CheckArray;
    use ValidatorTrait;

    public function supportsDenormalization($data, $type, $format = null, array $context = []): bool
    {
        return $type === \Steefdw\TempoApi\Model\WorkAttributeValuesByWorklog::class;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return is_object($data) && $data::class === \Steefdw\TempoApi\Model\WorkAttributeValuesByWorklog::class;
    }

    /**
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        if (isset($data['$ref'])) {
            return new Reference($data['$ref'], $context['document-origin']);
        }
        if (isset($data['$recursiveRef'])) {
            return new Reference($data['$recursiveRef'], $context['document-origin']);
        }
        $object = new \Steefdw\TempoApi\Model\WorkAttributeValuesByWorklog();
        if (null === $data || false === \is_array($data)) {
            return $object;
        }
        if (\array_key_exists('tempoWorklogId', $data)) {
            $object->setTempoWorklogId($data['tempoWorklogId']);
            unset($data['tempoWorklogId']);
        }
        if (\array_key_exists('workAttributeValues', $data)) {
            $values = [];
            foreach ($data['workAttributeValues'] as $value) {
                $values[] = $this->denormalizer->denormalize($value, \Steefdw\TempoApi\Model\WorkAttributeValue::class, 'json', $context);
            }
            $object->setWorkAttributeValues($values);
            unset($data['workAttributeValues']);
        }
        foreach ($data as $key => $value_1) {
            if (preg_match('/.*/', (string) $key)) {
                $object[$key] = $value_1;
            }
        }

        return $object;
    }

    /**
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $data = [];
        if ($object->isInitialized('tempoWorklogId') && null !== $object->getTempoWorklogId()) {
            $data['tempoWorklogId'] = $object->getTempoWorklogId();
        }
        if ($object->isInitialized('workAttributeValues') && null !== $object->getWorkAttributeValues()) {
            $values = [];
            foreach ($object->getWorkAttributeValues() as $value) {
                $values[] = $this->normalizer->normalize($value, 'json', $context);
            }
            $data['workAttributeValues'] = $values;
        }
        foreach ($object as $key => $value_1) {
            if (preg_match('/.*/', (string) $key)) {
                $data[$key] = $value_1;
            }
        }

        return $data;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [\Steefdw\TempoApi\Model\WorkAttributeValuesByWorklog::class => false];
    }
}
