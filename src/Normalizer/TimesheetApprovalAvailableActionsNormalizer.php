<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Normalizer;

use Jane\Component\JsonSchemaRuntime\Reference;
use Steefdw\TempoApi\Runtime\Normalizer\CheckArray;
use Steefdw\TempoApi\Runtime\Normalizer\ValidatorTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class TimesheetApprovalAvailableActionsNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;
    use CheckArray;
    use ValidatorTrait;

    public function supportsDenormalization($data, $type, $format = null, array $context = []): bool
    {
        return $type === \Steefdw\TempoApi\Model\TimesheetApprovalAvailableActions::class;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return is_object($data) && $data::class === \Steefdw\TempoApi\Model\TimesheetApprovalAvailableActions::class;
    }

    /**
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        if (isset($data['$ref'])) {
            return new Reference($data['$ref'], $context['document-origin']);
        }
        if (isset($data['$recursiveRef'])) {
            return new Reference($data['$recursiveRef'], $context['document-origin']);
        }
        $object = new \Steefdw\TempoApi\Model\TimesheetApprovalAvailableActions();
        if (null === $data || false === \is_array($data)) {
            return $object;
        }
        if (\array_key_exists('approve', $data)) {
            $object->setApprove($this->denormalizer->denormalize($data['approve'], \Steefdw\TempoApi\Model\SelfApiBean::class, 'json', $context));
            unset($data['approve']);
        }
        if (\array_key_exists('reject', $data)) {
            $object->setReject($this->denormalizer->denormalize($data['reject'], \Steefdw\TempoApi\Model\SelfApiBean::class, 'json', $context));
            unset($data['reject']);
        }
        if (\array_key_exists('reopen', $data)) {
            $object->setReopen($this->denormalizer->denormalize($data['reopen'], \Steefdw\TempoApi\Model\SelfApiBean::class, 'json', $context));
            unset($data['reopen']);
        }
        if (\array_key_exists('submit', $data)) {
            $object->setSubmit($this->denormalizer->denormalize($data['submit'], \Steefdw\TempoApi\Model\SelfApiBean::class, 'json', $context));
            unset($data['submit']);
        }
        foreach ($data as $key => $value) {
            if (preg_match('/.*/', (string) $key)) {
                $object[$key] = $value;
            }
        }

        return $object;
    }

    /**
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $data = [];
        if ($object->isInitialized('approve') && null !== $object->getApprove()) {
            $data['approve'] = $this->normalizer->normalize($object->getApprove(), 'json', $context);
        }
        if ($object->isInitialized('reject') && null !== $object->getReject()) {
            $data['reject'] = $this->normalizer->normalize($object->getReject(), 'json', $context);
        }
        if ($object->isInitialized('reopen') && null !== $object->getReopen()) {
            $data['reopen'] = $this->normalizer->normalize($object->getReopen(), 'json', $context);
        }
        if ($object->isInitialized('submit') && null !== $object->getSubmit()) {
            $data['submit'] = $this->normalizer->normalize($object->getSubmit(), 'json', $context);
        }
        foreach ($object as $key => $value) {
            if (preg_match('/.*/', (string) $key)) {
                $data[$key] = $value;
            }
        }

        return $data;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [\Steefdw\TempoApi\Model\TimesheetApprovalAvailableActions::class => false];
    }
}
