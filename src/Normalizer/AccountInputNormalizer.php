<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Normalizer;

use Jane\Component\JsonSchemaRuntime\Reference;
use Steefdw\TempoApi\Runtime\Normalizer\CheckArray;
use Steefdw\TempoApi\Runtime\Normalizer\ValidatorTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class AccountInputNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;
    use CheckArray;
    use ValidatorTrait;

    public function supportsDenormalization($data, $type, $format = null, array $context = []): bool
    {
        return $type === \Steefdw\TempoApi\Model\AccountInput::class;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return is_object($data) && $data::class === \Steefdw\TempoApi\Model\AccountInput::class;
    }

    /**
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        if (isset($data['$ref'])) {
            return new Reference($data['$ref'], $context['document-origin']);
        }
        if (isset($data['$recursiveRef'])) {
            return new Reference($data['$recursiveRef'], $context['document-origin']);
        }
        $object = new \Steefdw\TempoApi\Model\AccountInput();
        if (null === $data || false === \is_array($data)) {
            return $object;
        }
        if (\array_key_exists('categoryKey', $data)) {
            $object->setCategoryKey($data['categoryKey']);
            unset($data['categoryKey']);
        }
        if (\array_key_exists('contactAccountId', $data)) {
            $object->setContactAccountId($data['contactAccountId']);
            unset($data['contactAccountId']);
        }
        if (\array_key_exists('customerKey', $data)) {
            $object->setCustomerKey($data['customerKey']);
            unset($data['customerKey']);
        }
        if (\array_key_exists('externalContactName', $data)) {
            $object->setExternalContactName($data['externalContactName']);
            unset($data['externalContactName']);
        }
        if (\array_key_exists('global', $data)) {
            $object->setGlobal($data['global']);
            unset($data['global']);
        }
        if (\array_key_exists('key', $data)) {
            $object->setKey($data['key']);
            unset($data['key']);
        }
        if (\array_key_exists('leadAccountId', $data)) {
            $object->setLeadAccountId($data['leadAccountId']);
            unset($data['leadAccountId']);
        }
        if (\array_key_exists('monthlyBudget', $data)) {
            $object->setMonthlyBudget($data['monthlyBudget']);
            unset($data['monthlyBudget']);
        }
        if (\array_key_exists('name', $data)) {
            $object->setName($data['name']);
            unset($data['name']);
        }
        if (\array_key_exists('status', $data)) {
            $object->setStatus($data['status']);
            unset($data['status']);
        }
        foreach ($data as $key => $value) {
            if (preg_match('/.*/', (string) $key)) {
                $object[$key] = $value;
            }
        }

        return $object;
    }

    /**
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $data = [];
        if ($object->isInitialized('categoryKey') && null !== $object->getCategoryKey()) {
            $data['categoryKey'] = $object->getCategoryKey();
        }
        if ($object->isInitialized('contactAccountId') && null !== $object->getContactAccountId()) {
            $data['contactAccountId'] = $object->getContactAccountId();
        }
        if ($object->isInitialized('customerKey') && null !== $object->getCustomerKey()) {
            $data['customerKey'] = $object->getCustomerKey();
        }
        if ($object->isInitialized('externalContactName') && null !== $object->getExternalContactName()) {
            $data['externalContactName'] = $object->getExternalContactName();
        }
        if ($object->isInitialized('global') && null !== $object->getGlobal()) {
            $data['global'] = $object->getGlobal();
        }
        $data['key'] = $object->getKey();
        $data['leadAccountId'] = $object->getLeadAccountId();
        if ($object->isInitialized('monthlyBudget') && null !== $object->getMonthlyBudget()) {
            $data['monthlyBudget'] = $object->getMonthlyBudget();
        }
        $data['name'] = $object->getName();
        $data['status'] = $object->getStatus();
        foreach ($object as $key => $value) {
            if (preg_match('/.*/', (string) $key)) {
                $data[$key] = $value;
            }
        }

        return $data;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [\Steefdw\TempoApi\Model\AccountInput::class => false];
    }
}
