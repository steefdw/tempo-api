<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Normalizer;

use Jane\Component\JsonSchemaRuntime\Reference;
use Steefdw\TempoApi\Runtime\Normalizer\CheckArray;
use Steefdw\TempoApi\Runtime\Normalizer\ValidatorTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class WorklogNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;
    use CheckArray;
    use ValidatorTrait;

    public function supportsDenormalization($data, $type, $format = null, array $context = []): bool
    {
        return $type === \Steefdw\TempoApi\Model\Worklog::class;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return is_object($data) && $data::class === \Steefdw\TempoApi\Model\Worklog::class;
    }

    /**
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        if (isset($data['$ref'])) {
            return new Reference($data['$ref'], $context['document-origin']);
        }
        if (isset($data['$recursiveRef'])) {
            return new Reference($data['$recursiveRef'], $context['document-origin']);
        }
        $object = new \Steefdw\TempoApi\Model\Worklog();
        if (null === $data || false === \is_array($data)) {
            return $object;
        }
        if (\array_key_exists('attributes', $data)) {
            $object->setAttributes($this->denormalizer->denormalize($data['attributes'], \Steefdw\TempoApi\Model\SelfListWorkAttributeValue::class, 'json', $context));
            unset($data['attributes']);
        }
        if (\array_key_exists('author', $data)) {
            $object->setAuthor($this->denormalizer->denormalize($data['author'], \Steefdw\TempoApi\Model\User::class, 'json', $context));
            unset($data['author']);
        }
        if (\array_key_exists('billableSeconds', $data)) {
            $object->setBillableSeconds($data['billableSeconds']);
            unset($data['billableSeconds']);
        }
        if (\array_key_exists('createdAt', $data)) {
            $object->setCreatedAt($data['createdAt']);
            unset($data['createdAt']);
        }
        if (\array_key_exists('description', $data)) {
            $object->setDescription($data['description']);
            unset($data['description']);
        }
        if (\array_key_exists('issue', $data)) {
            $object->setIssue($this->denormalizer->denormalize($data['issue'], \Steefdw\TempoApi\Model\Issue::class, 'json', $context));
            unset($data['issue']);
        }
        if (\array_key_exists('self', $data)) {
            $object->setSelf($data['self']);
            unset($data['self']);
        }
        if (\array_key_exists('startDate', $data)) {
            $object->setStartDate(\DateTime::createFromFormat('Y-m-d', $data['startDate'])->setTime(0, 0, 0));
            unset($data['startDate']);
        }
        if (\array_key_exists('startTime', $data)) {
            $object->setStartTime($data['startTime']);
            unset($data['startTime']);
        }
        if (\array_key_exists('tempoWorklogId', $data)) {
            $object->setTempoWorklogId($data['tempoWorklogId']);
            unset($data['tempoWorklogId']);
        }
        if (\array_key_exists('timeSpentSeconds', $data)) {
            $object->setTimeSpentSeconds($data['timeSpentSeconds']);
            unset($data['timeSpentSeconds']);
        }
        if (\array_key_exists('updatedAt', $data)) {
            $object->setUpdatedAt($data['updatedAt']);
            unset($data['updatedAt']);
        }
        foreach ($data as $key => $value) {
            if (preg_match('/.*/', (string) $key)) {
                $object[$key] = $value;
            }
        }

        return $object;
    }

    /**
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $data = [];
        $data['attributes'] = $this->normalizer->normalize($object->getAttributes(), 'json', $context);
        $data['author'] = $this->normalizer->normalize($object->getAuthor(), 'json', $context);
        if ($object->isInitialized('billableSeconds') && null !== $object->getBillableSeconds()) {
            $data['billableSeconds'] = $object->getBillableSeconds();
        }
        $data['createdAt'] = $object->getCreatedAt();
        if ($object->isInitialized('description') && null !== $object->getDescription()) {
            $data['description'] = $object->getDescription();
        }
        $data['issue'] = $this->normalizer->normalize($object->getIssue(), 'json', $context);
        $data['self'] = $object->getSelf();
        $data['startDate'] = $object->getStartDate()->format('Y-m-d');
        if ($object->isInitialized('startTime') && null !== $object->getStartTime()) {
            $data['startTime'] = $object->getStartTime();
        }
        $data['tempoWorklogId'] = $object->getTempoWorklogId();
        $data['timeSpentSeconds'] = $object->getTimeSpentSeconds();
        $data['updatedAt'] = $object->getUpdatedAt();
        foreach ($object as $key => $value) {
            if (preg_match('/.*/', (string) $key)) {
                $data[$key] = $value;
            }
        }

        return $data;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [\Steefdw\TempoApi\Model\Worklog::class => false];
    }
}
