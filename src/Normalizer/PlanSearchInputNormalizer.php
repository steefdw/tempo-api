<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Normalizer;

use Jane\Component\JsonSchemaRuntime\Reference;
use Steefdw\TempoApi\Runtime\Normalizer\CheckArray;
use Steefdw\TempoApi\Runtime\Normalizer\ValidatorTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class PlanSearchInputNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;
    use CheckArray;
    use ValidatorTrait;

    public function supportsDenormalization($data, $type, $format = null, array $context = []): bool
    {
        return $type === \Steefdw\TempoApi\Model\PlanSearchInput::class;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return is_object($data) && $data::class === \Steefdw\TempoApi\Model\PlanSearchInput::class;
    }

    /**
     * @return mixed
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        if (isset($data['$ref'])) {
            return new Reference($data['$ref'], $context['document-origin']);
        }
        if (isset($data['$recursiveRef'])) {
            return new Reference($data['$recursiveRef'], $context['document-origin']);
        }
        $object = new \Steefdw\TempoApi\Model\PlanSearchInput();
        if (null === $data || false === \is_array($data)) {
            return $object;
        }
        if (\array_key_exists('accountIds', $data)) {
            $values = [];
            foreach ($data['accountIds'] as $value) {
                $values[] = $value;
            }
            $object->setAccountIds($values);
            unset($data['accountIds']);
        }
        if (\array_key_exists('assigneeTypes', $data)) {
            $values_1 = [];
            foreach ($data['assigneeTypes'] as $value_1) {
                $values_1[] = $value_1;
            }
            $object->setAssigneeTypes($values_1);
            unset($data['assigneeTypes']);
        }
        if (\array_key_exists('from', $data)) {
            $object->setFrom($data['from']);
            unset($data['from']);
        }
        if (\array_key_exists('genericResourceIds', $data)) {
            $values_2 = [];
            foreach ($data['genericResourceIds'] as $value_2) {
                $values_2[] = $value_2;
            }
            $object->setGenericResourceIds($values_2);
            unset($data['genericResourceIds']);
        }
        if (\array_key_exists('limit', $data)) {
            $object->setLimit($data['limit']);
            unset($data['limit']);
        }
        if (\array_key_exists('offset', $data)) {
            $object->setOffset($data['offset']);
            unset($data['offset']);
        }
        if (\array_key_exists('planIds', $data)) {
            $values_3 = [];
            foreach ($data['planIds'] as $value_3) {
                $values_3[] = $value_3;
            }
            $object->setPlanIds($values_3);
            unset($data['planIds']);
        }
        if (\array_key_exists('planItemIds', $data)) {
            $values_4 = [];
            foreach ($data['planItemIds'] as $value_4) {
                $values_4[] = $value_4;
            }
            $object->setPlanItemIds($values_4);
            unset($data['planItemIds']);
        }
        if (\array_key_exists('planItemTypes', $data)) {
            $values_5 = [];
            foreach ($data['planItemTypes'] as $value_5) {
                $values_5[] = $value_5;
            }
            $object->setPlanItemTypes($values_5);
            unset($data['planItemTypes']);
        }
        if (\array_key_exists('plannedTimeBreakdown', $data)) {
            $values_6 = [];
            foreach ($data['plannedTimeBreakdown'] as $value_6) {
                $values_6[] = $value_6;
            }
            $object->setPlannedTimeBreakdown($values_6);
            unset($data['plannedTimeBreakdown']);
        }
        if (\array_key_exists('to', $data)) {
            $object->setTo($data['to']);
            unset($data['to']);
        }
        if (\array_key_exists('updatedFrom', $data)) {
            $object->setUpdatedFrom($data['updatedFrom']);
            unset($data['updatedFrom']);
        }
        foreach ($data as $key => $value_7) {
            if (preg_match('/.*/', (string) $key)) {
                $object[$key] = $value_7;
            }
        }

        return $object;
    }

    /**
     * @return array|string|int|float|bool|\ArrayObject|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $data = [];
        if ($object->isInitialized('accountIds') && null !== $object->getAccountIds()) {
            $values = [];
            foreach ($object->getAccountIds() as $value) {
                $values[] = $value;
            }
            $data['accountIds'] = $values;
        }
        if ($object->isInitialized('assigneeTypes') && null !== $object->getAssigneeTypes()) {
            $values_1 = [];
            foreach ($object->getAssigneeTypes() as $value_1) {
                $values_1[] = $value_1;
            }
            $data['assigneeTypes'] = $values_1;
        }
        $data['from'] = $object->getFrom();
        if ($object->isInitialized('genericResourceIds') && null !== $object->getGenericResourceIds()) {
            $values_2 = [];
            foreach ($object->getGenericResourceIds() as $value_2) {
                $values_2[] = $value_2;
            }
            $data['genericResourceIds'] = $values_2;
        }
        if ($object->isInitialized('limit') && null !== $object->getLimit()) {
            $data['limit'] = $object->getLimit();
        }
        if ($object->isInitialized('offset') && null !== $object->getOffset()) {
            $data['offset'] = $object->getOffset();
        }
        if ($object->isInitialized('planIds') && null !== $object->getPlanIds()) {
            $values_3 = [];
            foreach ($object->getPlanIds() as $value_3) {
                $values_3[] = $value_3;
            }
            $data['planIds'] = $values_3;
        }
        if ($object->isInitialized('planItemIds') && null !== $object->getPlanItemIds()) {
            $values_4 = [];
            foreach ($object->getPlanItemIds() as $value_4) {
                $values_4[] = $value_4;
            }
            $data['planItemIds'] = $values_4;
        }
        if ($object->isInitialized('planItemTypes') && null !== $object->getPlanItemTypes()) {
            $values_5 = [];
            foreach ($object->getPlanItemTypes() as $value_5) {
                $values_5[] = $value_5;
            }
            $data['planItemTypes'] = $values_5;
        }
        if ($object->isInitialized('plannedTimeBreakdown') && null !== $object->getPlannedTimeBreakdown()) {
            $values_6 = [];
            foreach ($object->getPlannedTimeBreakdown() as $value_6) {
                $values_6[] = $value_6;
            }
            $data['plannedTimeBreakdown'] = $values_6;
        }
        $data['to'] = $object->getTo();
        if ($object->isInitialized('updatedFrom') && null !== $object->getUpdatedFrom()) {
            $data['updatedFrom'] = $object->getUpdatedFrom();
        }
        foreach ($object as $key => $value_7) {
            if (preg_match('/.*/', (string) $key)) {
                $data[$key] = $value_7;
            }
        }

        return $data;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [\Steefdw\TempoApi\Model\PlanSearchInput::class => false];
    }
}
