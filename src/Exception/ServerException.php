<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Exception;

interface ServerException extends ApiException
{
}
