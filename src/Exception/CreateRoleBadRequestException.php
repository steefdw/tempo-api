<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Exception;

class CreateRoleBadRequestException extends BadRequestException
{
    public function __construct(private ?\Psr\Http\Message\ResponseInterface $response = null)
    {
        parent::__construct('Role cannot be created because the name already exists');
    }

    public function getResponse(): ?\Psr\Http\Message\ResponseInterface
    {
        return $this->response;
    }
}
