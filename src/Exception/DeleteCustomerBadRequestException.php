<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Exception;

class DeleteCustomerBadRequestException extends BadRequestException
{
    public function __construct(private ?\Psr\Http\Message\ResponseInterface $response = null)
    {
        parent::__construct('Customer cannot be deleted due to invalid request');
    }

    public function getResponse(): ?\Psr\Http\Message\ResponseInterface
    {
        return $this->response;
    }
}
