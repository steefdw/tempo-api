<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Exception;

class CreateWorkAttributeValuesForWorklogsBadRequestException extends BadRequestException
{
    public function __construct(private ?\Psr\Http\Message\ResponseInterface $response = null)
    {
        parent::__construct('Work attribute values cannot be created for some reasons, for example if the value is not valid for the type of the work attribute, or if the worklog already has a value for the specified attribute.');
    }

    public function getResponse(): ?\Psr\Http\Message\ResponseInterface
    {
        return $this->response;
    }
}
