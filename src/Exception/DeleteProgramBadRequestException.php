<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Exception;

class DeleteProgramBadRequestException extends BadRequestException
{
    public function __construct(private ?\Psr\Http\Message\ResponseInterface $response = null)
    {
        parent::__construct('Program cannot be deleted due to invalid request');
    }

    public function getResponse(): ?\Psr\Http\Message\ResponseInterface
    {
        return $this->response;
    }
}
