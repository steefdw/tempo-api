<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Exception;

class GetTimesheetApprovalForUserNotFoundException extends NotFoundException
{
    public function __construct(private ?\Psr\Http\Message\ResponseInterface $response = null)
    {
        parent::__construct('User is invalid');
    }

    public function getResponse(): ?\Psr\Http\Message\ResponseInterface
    {
        return $this->response;
    }
}
