<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Exception;

class CreateAccountBadRequestException extends BadRequestException
{
    public function __construct(private ?\Psr\Http\Message\ResponseInterface $response = null)
    {
        parent::__construct('Account with this key already exists');
    }

    public function getResponse(): ?\Psr\Http\Message\ResponseInterface
    {
        return $this->response;
    }
}
