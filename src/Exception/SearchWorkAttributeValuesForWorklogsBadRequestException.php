<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Exception;

class SearchWorkAttributeValuesForWorklogsBadRequestException extends BadRequestException
{
    public function __construct(private ?\Psr\Http\Message\ResponseInterface $response = null)
    {
        parent::__construct('Please provide at least 1 and at most 500 worklog ids');
    }

    public function getResponse(): ?\Psr\Http\Message\ResponseInterface
    {
        return $this->response;
    }
}
