<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Exception;

class GetWorklogsNotFoundException extends NotFoundException
{
    public function __construct(private ?\Psr\Http\Message\ResponseInterface $response = null)
    {
        parent::__construct('Authenticated user is missing permission to fulfill the request');
    }

    public function getResponse(): ?\Psr\Http\Message\ResponseInterface
    {
        return $this->response;
    }
}
