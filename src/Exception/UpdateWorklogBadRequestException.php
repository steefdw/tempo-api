<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Exception;

class UpdateWorklogBadRequestException extends BadRequestException
{
    public function __construct(private ?\Psr\Http\Message\ResponseInterface $response = null)
    {
        parent::__construct('Worklog cannot be updated for some reasons');
    }

    public function getResponse(): ?\Psr\Http\Message\ResponseInterface
    {
        return $this->response;
    }
}
