<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Exception;

class CreateHolidayBadRequestException extends BadRequestException
{
    public function __construct(private ?\Psr\Http\Message\ResponseInterface $response = null)
    {
        parent::__construct('Holiday cannot be created for some reasons');
    }

    public function getResponse(): ?\Psr\Http\Message\ResponseInterface
    {
        return $this->response;
    }
}
