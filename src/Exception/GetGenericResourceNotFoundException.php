<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Exception;

class GetGenericResourceNotFoundException extends NotFoundException
{
    public function __construct(private ?\Psr\Http\Message\ResponseInterface $response = null)
    {
        parent::__construct('Generic resource not found');
    }

    public function getResponse(): ?\Psr\Http\Message\ResponseInterface
    {
        return $this->response;
    }
}
