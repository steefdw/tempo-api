<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Exception;

class RejectTimesheetForUserBadRequestException extends BadRequestException
{
    public function __construct(private ?\Psr\Http\Message\ResponseInterface $response = null)
    {
        parent::__construct('Non valid date \'2022-05-011\': format must be yyyy-MM-dd');
    }

    public function getResponse(): ?\Psr\Http\Message\ResponseInterface
    {
        return $this->response;
    }
}
