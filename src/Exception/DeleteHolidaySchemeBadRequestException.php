<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Exception;

class DeleteHolidaySchemeBadRequestException extends BadRequestException
{
    public function __construct(private ?\Psr\Http\Message\ResponseInterface $response = null)
    {
        parent::__construct('Holiday scheme cannot be deleted for some reasons.');
    }

    public function getResponse(): ?\Psr\Http\Message\ResponseInterface
    {
        return $this->response;
    }
}
