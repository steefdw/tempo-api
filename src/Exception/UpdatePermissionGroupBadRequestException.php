<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Exception;

class UpdatePermissionGroupBadRequestException extends BadRequestException
{
    public function __construct(private ?\Psr\Http\Message\ResponseInterface $response = null)
    {
        parent::__construct('Team with specified id does not exist.');
    }

    public function getResponse(): ?\Psr\Http\Message\ResponseInterface
    {
        return $this->response;
    }
}
