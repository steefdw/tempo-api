<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Exception;

class AddResourceToTeamBadRequestException extends BadRequestException
{
    public function __construct(private ?\Psr\Http\Message\ResponseInterface $response = null)
    {
        parent::__construct('Generic resource cannot be added to team to invalid request');
    }

    public function getResponse(): ?\Psr\Http\Message\ResponseInterface
    {
        return $this->response;
    }
}
