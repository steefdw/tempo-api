<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Exception;

class CreateLinkBadRequestException extends BadRequestException
{
    public function __construct(private ?\Psr\Http\Message\ResponseInterface $response = null)
    {
        parent::__construct('Account-link cannot be created for some reason');
    }

    public function getResponse(): ?\Psr\Http\Message\ResponseInterface
    {
        return $this->response;
    }
}
