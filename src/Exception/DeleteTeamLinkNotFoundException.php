<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Exception;

class DeleteTeamLinkNotFoundException extends NotFoundException
{
    public function __construct(private ?\Psr\Http\Message\ResponseInterface $response = null)
    {
        parent::__construct('Team-link cannot be found in the system');
    }

    public function getResponse(): ?\Psr\Http\Message\ResponseInterface
    {
        return $this->response;
    }
}
