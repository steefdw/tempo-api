<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Exception;

class CreateCustomerBadRequestException extends BadRequestException
{
    public function __construct(private ?\Psr\Http\Message\ResponseInterface $response = null)
    {
        parent::__construct('Customer cannot be created due to invalid request');
    }

    public function getResponse(): ?\Psr\Http\Message\ResponseInterface
    {
        return $this->response;
    }
}
