<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Endpoint;

use Steefdw\TempoApi\Runtime\Client\BaseEndpoint;
use Steefdw\TempoApi\Runtime\Client\Endpoint;

class UpdateMembership extends BaseEndpoint implements Endpoint
{
    use \Steefdw\TempoApi\Runtime\Client\EndpointTrait;
    protected $id;

    /**
     * Updates an existing Membership using the provided input and returns the updated Membership
     *
     * @param string $id
     * @param null|\Steefdw\TempoApi\Model\MembershipInput $requestBody
     */
    public function __construct(string $id, ?\Steefdw\TempoApi\Model\MembershipInput $requestBody = null)
    {
        $this->id = $id;
        $this->body = $requestBody;
    }

    public function getMethod(): string
    {
        return 'PUT';
    }

    public function getUri(): string
    {
        return str_replace(['{id}'], [$this->id], '/team-memberships/{id}');
    }

    public function getBody(\Symfony\Component\Serializer\SerializerInterface $serializer, $streamFactory = null): array
    {
        if ($this->body instanceof \Steefdw\TempoApi\Model\MembershipInput) {
            return [['Content-Type' => ['application/json']], $serializer->serialize($this->body, 'json')];
        }

        return [[], null];
    }

    public function getExtraHeaders(): array
    {
        return ['Accept' => ['application/json']];
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Steefdw\TempoApi\Exception\UpdateMembershipBadRequestException
     * @throws \Steefdw\TempoApi\Exception\UpdateMembershipNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\TeamMembership
     */
    protected function transformResponseBody(\Psr\Http\Message\ResponseInterface $response, \Symfony\Component\Serializer\SerializerInterface $serializer, ?string $contentType = null)
    {
        $status = $response->getStatusCode();
        $body = (string) $response->getBody();
        if (is_null($contentType) === false && (200 === $status && mb_strpos($contentType, 'application/json') !== false)) {
            return $serializer->deserialize($body, \Steefdw\TempoApi\Model\TeamMembership::class, 'json');
        }
        if (400 === $status) {
            throw new \Steefdw\TempoApi\Exception\UpdateMembershipBadRequestException($response);
        }
        if (404 === $status) {
            throw new \Steefdw\TempoApi\Exception\UpdateMembershipNotFoundException($response);
        }
    }

    public function getAuthenticationScopes(): array
    {
        return [];
    }
}
