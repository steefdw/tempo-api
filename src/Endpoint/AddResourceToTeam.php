<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Endpoint;

use Steefdw\TempoApi\Runtime\Client\BaseEndpoint;
use Steefdw\TempoApi\Runtime\Client\Endpoint;

class AddResourceToTeam extends BaseEndpoint implements Endpoint
{
    use \Steefdw\TempoApi\Runtime\Client\EndpointTrait;
    protected $teamId;

    /**
     * Adds Generic Resource to the given Team
     *
     * @param int $teamId An id that uniquely identifies the Team which the generic resource is to be a member of
     * @param null|\Steefdw\TempoApi\Model\GenericResourceTeamMemberInput $requestBody
     */
    public function __construct(int $teamId, ?\Steefdw\TempoApi\Model\GenericResourceTeamMemberInput $requestBody = null)
    {
        $this->teamId = $teamId;
        $this->body = $requestBody;
    }

    public function getMethod(): string
    {
        return 'POST';
    }

    public function getUri(): string
    {
        return str_replace(['{teamId}'], [$this->teamId], '/teams/{teamId}/generic-resources');
    }

    public function getBody(\Symfony\Component\Serializer\SerializerInterface $serializer, $streamFactory = null): array
    {
        if ($this->body instanceof \Steefdw\TempoApi\Model\GenericResourceTeamMemberInput) {
            return [['Content-Type' => ['application/json']], $serializer->serialize($this->body, 'json')];
        }

        return [[], null];
    }

    public function getExtraHeaders(): array
    {
        return ['Accept' => ['application/json']];
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Steefdw\TempoApi\Exception\AddResourceToTeamBadRequestException
     * @throws \Steefdw\TempoApi\Exception\AddResourceToTeamNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\GenericResourceTeamMember
     */
    protected function transformResponseBody(\Psr\Http\Message\ResponseInterface $response, \Symfony\Component\Serializer\SerializerInterface $serializer, ?string $contentType = null)
    {
        $status = $response->getStatusCode();
        $body = (string) $response->getBody();
        if (is_null($contentType) === false && (200 === $status && mb_strpos($contentType, 'application/json') !== false)) {
            return $serializer->deserialize($body, \Steefdw\TempoApi\Model\GenericResourceTeamMember::class, 'json');
        }
        if (400 === $status) {
            throw new \Steefdw\TempoApi\Exception\AddResourceToTeamBadRequestException($response);
        }
        if (404 === $status) {
            throw new \Steefdw\TempoApi\Exception\AddResourceToTeamNotFoundException($response);
        }
    }

    public function getAuthenticationScopes(): array
    {
        return [];
    }
}
