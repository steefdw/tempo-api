<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Endpoint;

use Steefdw\TempoApi\Runtime\Client\BaseEndpoint;
use Steefdw\TempoApi\Runtime\Client\Endpoint;

class GetTeamLink extends BaseEndpoint implements Endpoint
{
    use \Steefdw\TempoApi\Runtime\Client\EndpointTrait;
    protected $linkId;

    /**
     * Retrieves an existing Team Link for the given id
     *
     * @param string $linkId
     */
    public function __construct(string $linkId)
    {
        $this->linkId = $linkId;
    }

    public function getMethod(): string
    {
        return 'GET';
    }

    public function getUri(): string
    {
        return str_replace(['{linkId}'], [$this->linkId], '/team-links/{linkId}');
    }

    public function getBody(\Symfony\Component\Serializer\SerializerInterface $serializer, $streamFactory = null): array
    {
        return [[], null];
    }

    public function getExtraHeaders(): array
    {
        return ['Accept' => ['application/json']];
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Steefdw\TempoApi\Exception\GetTeamLinkNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\TeamLink
     */
    protected function transformResponseBody(\Psr\Http\Message\ResponseInterface $response, \Symfony\Component\Serializer\SerializerInterface $serializer, ?string $contentType = null)
    {
        $status = $response->getStatusCode();
        $body = (string) $response->getBody();
        if (is_null($contentType) === false && (200 === $status && mb_strpos($contentType, 'application/json') !== false)) {
            return $serializer->deserialize($body, \Steefdw\TempoApi\Model\TeamLink::class, 'json');
        }
        if (404 === $status) {
            throw new \Steefdw\TempoApi\Exception\GetTeamLinkNotFoundException($response);
        }
    }

    public function getAuthenticationScopes(): array
    {
        return [];
    }
}
