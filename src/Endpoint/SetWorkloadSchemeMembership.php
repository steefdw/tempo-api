<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Endpoint;

use Steefdw\TempoApi\Runtime\Client\BaseEndpoint;
use Steefdw\TempoApi\Runtime\Client\Endpoint;

class SetWorkloadSchemeMembership extends BaseEndpoint implements Endpoint
{
    use \Steefdw\TempoApi\Runtime\Client\EndpointTrait;
    protected $schemeId;

    /**
     * Assign a holiday scheme with the given id to members using the provided input
     *
     * @param string $schemeId The id of the Scheme
     * @param null|\Steefdw\TempoApi\Model\HolidaySchemeMembersInput $requestBody
     */
    public function __construct(string $schemeId, ?\Steefdw\TempoApi\Model\HolidaySchemeMembersInput $requestBody = null)
    {
        $this->schemeId = $schemeId;
        $this->body = $requestBody;
    }

    public function getMethod(): string
    {
        return 'POST';
    }

    public function getUri(): string
    {
        return str_replace(['{schemeId}'], [$this->schemeId], '/holiday-schemes/{schemeId}/members');
    }

    public function getBody(\Symfony\Component\Serializer\SerializerInterface $serializer, $streamFactory = null): array
    {
        if ($this->body instanceof \Steefdw\TempoApi\Model\HolidaySchemeMembersInput) {
            return [['Content-Type' => ['application/json']], $serializer->serialize($this->body, 'json')];
        }

        return [[], null];
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Steefdw\TempoApi\Exception\SetWorkloadSchemeMembershipBadRequestException
     * @throws \Steefdw\TempoApi\Exception\SetWorkloadSchemeMembershipNotFoundException
     *
     * @return null
     */
    protected function transformResponseBody(\Psr\Http\Message\ResponseInterface $response, \Symfony\Component\Serializer\SerializerInterface $serializer, ?string $contentType = null)
    {
        $status = $response->getStatusCode();
        $body = (string) $response->getBody();
        if (204 === $status) {
            return null;
        }
        if (400 === $status) {
            throw new \Steefdw\TempoApi\Exception\SetWorkloadSchemeMembershipBadRequestException($response);
        }
        if (404 === $status) {
            throw new \Steefdw\TempoApi\Exception\SetWorkloadSchemeMembershipNotFoundException($response);
        }
    }

    public function getAuthenticationScopes(): array
    {
        return [];
    }
}
