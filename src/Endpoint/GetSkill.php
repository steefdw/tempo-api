<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Endpoint;

use Steefdw\TempoApi\Runtime\Client\BaseEndpoint;
use Steefdw\TempoApi\Runtime\Client\Endpoint;

class GetSkill extends BaseEndpoint implements Endpoint
{
    use \Steefdw\TempoApi\Runtime\Client\EndpointTrait;
    protected $id;

    /**
     * Retrieves an existing Skill for the given id
     *
     * @param int $id An id that uniquely identifies the Skill
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getMethod(): string
    {
        return 'GET';
    }

    public function getUri(): string
    {
        return str_replace(['{id}'], [$this->id], '/skills/{id}');
    }

    public function getBody(\Symfony\Component\Serializer\SerializerInterface $serializer, $streamFactory = null): array
    {
        return [[], null];
    }

    public function getExtraHeaders(): array
    {
        return ['Accept' => ['application/json']];
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Steefdw\TempoApi\Exception\GetSkillNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\Skill
     */
    protected function transformResponseBody(\Psr\Http\Message\ResponseInterface $response, \Symfony\Component\Serializer\SerializerInterface $serializer, ?string $contentType = null)
    {
        $status = $response->getStatusCode();
        $body = (string) $response->getBody();
        if (is_null($contentType) === false && (200 === $status && mb_strpos($contentType, 'application/json') !== false)) {
            return $serializer->deserialize($body, \Steefdw\TempoApi\Model\Skill::class, 'json');
        }
        if (404 === $status) {
            throw new \Steefdw\TempoApi\Exception\GetSkillNotFoundException($response);
        }
    }

    public function getAuthenticationScopes(): array
    {
        return [];
    }
}
