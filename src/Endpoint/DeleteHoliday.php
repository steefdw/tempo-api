<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Endpoint;

use Steefdw\TempoApi\Runtime\Client\BaseEndpoint;
use Steefdw\TempoApi\Runtime\Client\Endpoint;

class DeleteHoliday extends BaseEndpoint implements Endpoint
{
    use \Steefdw\TempoApi\Runtime\Client\EndpointTrait;
    protected $schemeId;
    protected $holidayId;

    /**
     * Delete a holiday with the given id
     *
     * @param string $schemeId The id of the Scheme
     * @param string $holidayId The id of the holiday
     */
    public function __construct(string $schemeId, string $holidayId)
    {
        $this->schemeId = $schemeId;
        $this->holidayId = $holidayId;
    }

    public function getMethod(): string
    {
        return 'DELETE';
    }

    public function getUri(): string
    {
        return str_replace(['{schemeId}', '{holidayId}'], [$this->schemeId, $this->holidayId], '/holiday-schemes/{schemeId}/holidays/{holidayId}');
    }

    public function getBody(\Symfony\Component\Serializer\SerializerInterface $serializer, $streamFactory = null): array
    {
        return [[], null];
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Steefdw\TempoApi\Exception\DeleteHolidayNotFoundException
     *
     * @return null
     */
    protected function transformResponseBody(\Psr\Http\Message\ResponseInterface $response, \Symfony\Component\Serializer\SerializerInterface $serializer, ?string $contentType = null)
    {
        $status = $response->getStatusCode();
        $body = (string) $response->getBody();
        if (204 === $status) {
            return null;
        }
        if (404 === $status) {
            throw new \Steefdw\TempoApi\Exception\DeleteHolidayNotFoundException($response);
        }
    }

    public function getAuthenticationScopes(): array
    {
        return [];
    }
}
