<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Endpoint;

use Steefdw\TempoApi\Runtime\Client\BaseEndpoint;
use Steefdw\TempoApi\Runtime\Client\Endpoint;

class GetGenericResourceTeamMember extends BaseEndpoint implements Endpoint
{
    use \Steefdw\TempoApi\Runtime\Client\EndpointTrait;
    protected $teamId;
    protected $resourceId;

    /**
     * Retrieves an existing Generic Resource from the given Team
     *
     * @param int $teamId An id that uniquely identifies the Team
     * @param int $resourceId An id that uniquely identifies the Generic Resource
     */
    public function __construct(int $teamId, int $resourceId)
    {
        $this->teamId = $teamId;
        $this->resourceId = $resourceId;
    }

    public function getMethod(): string
    {
        return 'GET';
    }

    public function getUri(): string
    {
        return str_replace(['{teamId}', '{resourceId}'], [$this->teamId, $this->resourceId], '/teams/{teamId}/generic-resources/{resourceId}');
    }

    public function getBody(\Symfony\Component\Serializer\SerializerInterface $serializer, $streamFactory = null): array
    {
        return [[], null];
    }

    public function getExtraHeaders(): array
    {
        return ['Accept' => ['application/json']];
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Steefdw\TempoApi\Exception\GetGenericResourceTeamMemberNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\GenericResourceTeamMember
     */
    protected function transformResponseBody(\Psr\Http\Message\ResponseInterface $response, \Symfony\Component\Serializer\SerializerInterface $serializer, ?string $contentType = null)
    {
        $status = $response->getStatusCode();
        $body = (string) $response->getBody();
        if (is_null($contentType) === false && (200 === $status && mb_strpos($contentType, 'application/json') !== false)) {
            return $serializer->deserialize($body, \Steefdw\TempoApi\Model\GenericResourceTeamMember::class, 'json');
        }
        if (404 === $status) {
            throw new \Steefdw\TempoApi\Exception\GetGenericResourceTeamMemberNotFoundException($response);
        }
    }

    public function getAuthenticationScopes(): array
    {
        return [];
    }
}
