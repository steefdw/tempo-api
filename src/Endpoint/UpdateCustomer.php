<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Endpoint;

use Steefdw\TempoApi\Runtime\Client\BaseEndpoint;
use Steefdw\TempoApi\Runtime\Client\Endpoint;

class UpdateCustomer extends BaseEndpoint implements Endpoint
{
    use \Steefdw\TempoApi\Runtime\Client\EndpointTrait;
    protected $key;

    /**
     * Updates the Customer with the given key using the provided input and returns the updated Customer
     *
     * @param string $key
     * @param null|\Steefdw\TempoApi\Model\CustomerInput $requestBody
     */
    public function __construct(string $key, ?\Steefdw\TempoApi\Model\CustomerInput $requestBody = null)
    {
        $this->key = $key;
        $this->body = $requestBody;
    }

    public function getMethod(): string
    {
        return 'PUT';
    }

    public function getUri(): string
    {
        return str_replace(['{key}'], [$this->key], '/customers/{key}');
    }

    public function getBody(\Symfony\Component\Serializer\SerializerInterface $serializer, $streamFactory = null): array
    {
        if ($this->body instanceof \Steefdw\TempoApi\Model\CustomerInput) {
            return [['Content-Type' => ['application/json']], $serializer->serialize($this->body, 'json')];
        }

        return [[], null];
    }

    public function getExtraHeaders(): array
    {
        return ['Accept' => ['application/json']];
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Steefdw\TempoApi\Exception\UpdateCustomerBadRequestException
     * @throws \Steefdw\TempoApi\Exception\UpdateCustomerNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\Customer
     */
    protected function transformResponseBody(\Psr\Http\Message\ResponseInterface $response, \Symfony\Component\Serializer\SerializerInterface $serializer, ?string $contentType = null)
    {
        $status = $response->getStatusCode();
        $body = (string) $response->getBody();
        if (is_null($contentType) === false && (200 === $status && mb_strpos($contentType, 'application/json') !== false)) {
            return $serializer->deserialize($body, \Steefdw\TempoApi\Model\Customer::class, 'json');
        }
        if (400 === $status) {
            throw new \Steefdw\TempoApi\Exception\UpdateCustomerBadRequestException($response);
        }
        if (404 === $status) {
            throw new \Steefdw\TempoApi\Exception\UpdateCustomerNotFoundException($response);
        }
    }

    public function getAuthenticationScopes(): array
    {
        return [];
    }
}
