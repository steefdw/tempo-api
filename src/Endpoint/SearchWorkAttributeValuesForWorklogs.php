<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Endpoint;

use Steefdw\TempoApi\Runtime\Client\BaseEndpoint;
use Steefdw\TempoApi\Runtime\Client\Endpoint;

class SearchWorkAttributeValuesForWorklogs extends BaseEndpoint implements Endpoint
{
    use \Steefdw\TempoApi\Runtime\Client\EndpointTrait;

    /**
     * Retrieves a list of existing Work Attribute values that matches the given worklog ids
     *
     * @param null|\Steefdw\TempoApi\Model\WorkAttributeSearchInput $requestBody
     */
    public function __construct(?\Steefdw\TempoApi\Model\WorkAttributeSearchInput $requestBody = null)
    {
        $this->body = $requestBody;
    }

    public function getMethod(): string
    {
        return 'POST';
    }

    public function getUri(): string
    {
        return '/worklogs/work-attribute-values/search';
    }

    public function getBody(\Symfony\Component\Serializer\SerializerInterface $serializer, $streamFactory = null): array
    {
        if ($this->body instanceof \Steefdw\TempoApi\Model\WorkAttributeSearchInput) {
            return [['Content-Type' => ['application/json']], $serializer->serialize($this->body, 'json')];
        }

        return [[], null];
    }

    public function getExtraHeaders(): array
    {
        return ['Accept' => ['application/json']];
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Steefdw\TempoApi\Exception\SearchWorkAttributeValuesForWorklogsBadRequestException
     *
     * @return null|\Steefdw\TempoApi\Model\WorkAttributeValuesByWorklog[]
     */
    protected function transformResponseBody(\Psr\Http\Message\ResponseInterface $response, \Symfony\Component\Serializer\SerializerInterface $serializer, ?string $contentType = null)
    {
        $status = $response->getStatusCode();
        $body = (string) $response->getBody();
        if (is_null($contentType) === false && (200 === $status && mb_strpos($contentType, 'application/json') !== false)) {
            return $serializer->deserialize($body, 'Steefdw\\TempoApi\\Model\\WorkAttributeValuesByWorklog[]', 'json');
        }
        if (400 === $status) {
            throw new \Steefdw\TempoApi\Exception\SearchWorkAttributeValuesForWorklogsBadRequestException($response);
        }
    }

    public function getAuthenticationScopes(): array
    {
        return [];
    }
}
