<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Endpoint;

use Steefdw\TempoApi\Runtime\Client\BaseEndpoint;
use Steefdw\TempoApi\Runtime\Client\Endpoint;

class RemoveGenericResourceFromTeam extends BaseEndpoint implements Endpoint
{
    use \Steefdw\TempoApi\Runtime\Client\EndpointTrait;
    protected $teamId;
    protected $resourceId;

    /**
     * Deletes an existing Generic Resource from the given Team
     *
     * @param int $teamId An id that uniquely identifies the Team
     * @param int $resourceId An id that uniquely identifies the Generic Resource
     */
    public function __construct(int $teamId, int $resourceId)
    {
        $this->teamId = $teamId;
        $this->resourceId = $resourceId;
    }

    public function getMethod(): string
    {
        return 'DELETE';
    }

    public function getUri(): string
    {
        return str_replace(['{teamId}', '{resourceId}'], [$this->teamId, $this->resourceId], '/teams/{teamId}/generic-resources/{resourceId}');
    }

    public function getBody(\Symfony\Component\Serializer\SerializerInterface $serializer, $streamFactory = null): array
    {
        return [[], null];
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Steefdw\TempoApi\Exception\RemoveGenericResourceFromTeamNotFoundException
     *
     * @return null
     */
    protected function transformResponseBody(\Psr\Http\Message\ResponseInterface $response, \Symfony\Component\Serializer\SerializerInterface $serializer, ?string $contentType = null)
    {
        $status = $response->getStatusCode();
        $body = (string) $response->getBody();
        if (204 === $status) {
            return null;
        }
        if (404 === $status) {
            throw new \Steefdw\TempoApi\Exception\RemoveGenericResourceFromTeamNotFoundException($response);
        }
    }

    public function getAuthenticationScopes(): array
    {
        return [];
    }
}
