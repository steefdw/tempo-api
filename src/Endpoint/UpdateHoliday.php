<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Endpoint;

use Steefdw\TempoApi\Runtime\Client\BaseEndpoint;
use Steefdw\TempoApi\Runtime\Client\Endpoint;

class UpdateHoliday extends BaseEndpoint implements Endpoint
{
    use \Steefdw\TempoApi\Runtime\Client\EndpointTrait;
    protected $schemeId;
    protected $holidayId;

    /**
     * Update a holiday with the given id using the provided input
     *
     * @param string $schemeId The id of the Scheme
     * @param string $holidayId The id of the holiday
     * @param null|\Steefdw\TempoApi\Model\HolidayInput $requestBody
     */
    public function __construct(string $schemeId, string $holidayId, ?\Steefdw\TempoApi\Model\HolidayInput $requestBody = null)
    {
        $this->schemeId = $schemeId;
        $this->holidayId = $holidayId;
        $this->body = $requestBody;
    }

    public function getMethod(): string
    {
        return 'PUT';
    }

    public function getUri(): string
    {
        return str_replace(['{schemeId}', '{holidayId}'], [$this->schemeId, $this->holidayId], '/holiday-schemes/{schemeId}/holidays/{holidayId}');
    }

    public function getBody(\Symfony\Component\Serializer\SerializerInterface $serializer, $streamFactory = null): array
    {
        if ($this->body instanceof \Steefdw\TempoApi\Model\HolidayInput) {
            return [['Content-Type' => ['application/json']], $serializer->serialize($this->body, 'json')];
        }

        return [[], null];
    }

    public function getExtraHeaders(): array
    {
        return ['Accept' => ['application/json']];
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Steefdw\TempoApi\Exception\UpdateHolidayBadRequestException
     * @throws \Steefdw\TempoApi\Exception\UpdateHolidayNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\Holiday
     */
    protected function transformResponseBody(\Psr\Http\Message\ResponseInterface $response, \Symfony\Component\Serializer\SerializerInterface $serializer, ?string $contentType = null)
    {
        $status = $response->getStatusCode();
        $body = (string) $response->getBody();
        if (is_null($contentType) === false && (200 === $status && mb_strpos($contentType, 'application/json') !== false)) {
            return $serializer->deserialize($body, \Steefdw\TempoApi\Model\Holiday::class, 'json');
        }
        if (400 === $status) {
            throw new \Steefdw\TempoApi\Exception\UpdateHolidayBadRequestException($response);
        }
        if (404 === $status) {
            throw new \Steefdw\TempoApi\Exception\UpdateHolidayNotFoundException($response);
        }
    }

    public function getAuthenticationScopes(): array
    {
        return [];
    }
}
