<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Endpoint;

use Steefdw\TempoApi\Runtime\Client\BaseEndpoint;
use Steefdw\TempoApi\Runtime\Client\Endpoint;

class GetResourcesInTeam extends BaseEndpoint implements Endpoint
{
    use \Steefdw\TempoApi\Runtime\Client\EndpointTrait;
    protected $teamId;

    /**
     * Retrieves a list of all Generic Resources that are members of the given Team
     *
     * @param int $teamId An id that uniquely identifies the Team from which generic resources are to be retrieved
     */
    public function __construct(int $teamId)
    {
        $this->teamId = $teamId;
    }

    public function getMethod(): string
    {
        return 'GET';
    }

    public function getUri(): string
    {
        return str_replace(['{teamId}'], [$this->teamId], '/teams/{teamId}/generic-resources');
    }

    public function getBody(\Symfony\Component\Serializer\SerializerInterface $serializer, $streamFactory = null): array
    {
        return [[], null];
    }

    public function getExtraHeaders(): array
    {
        return ['Accept' => ['application/json']];
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Steefdw\TempoApi\Exception\GetResourcesInTeamNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\GenericResourceTeamMembers
     */
    protected function transformResponseBody(\Psr\Http\Message\ResponseInterface $response, \Symfony\Component\Serializer\SerializerInterface $serializer, ?string $contentType = null)
    {
        $status = $response->getStatusCode();
        $body = (string) $response->getBody();
        if (is_null($contentType) === false && (200 === $status && mb_strpos($contentType, 'application/json') !== false)) {
            return $serializer->deserialize($body, \Steefdw\TempoApi\Model\GenericResourceTeamMembers::class, 'json');
        }
        if (404 === $status) {
            throw new \Steefdw\TempoApi\Exception\GetResourcesInTeamNotFoundException($response);
        }
    }

    public function getAuthenticationScopes(): array
    {
        return [];
    }
}
