<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Endpoint;

use Steefdw\TempoApi\Runtime\Client\BaseEndpoint;
use Steefdw\TempoApi\Runtime\Client\Endpoint;

class DeleteProgram extends BaseEndpoint implements Endpoint
{
    use \Steefdw\TempoApi\Runtime\Client\EndpointTrait;
    protected $id;

    /**
     * Deletes an existing Program for the given id
     *
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function getMethod(): string
    {
        return 'DELETE';
    }

    public function getUri(): string
    {
        return str_replace(['{id}'], [$this->id], '/programs/{id}');
    }

    public function getBody(\Symfony\Component\Serializer\SerializerInterface $serializer, $streamFactory = null): array
    {
        return [[], null];
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Steefdw\TempoApi\Exception\DeleteProgramBadRequestException
     * @throws \Steefdw\TempoApi\Exception\DeleteProgramNotFoundException
     *
     * @return null
     */
    protected function transformResponseBody(\Psr\Http\Message\ResponseInterface $response, \Symfony\Component\Serializer\SerializerInterface $serializer, ?string $contentType = null)
    {
        $status = $response->getStatusCode();
        $body = (string) $response->getBody();
        if (204 === $status) {
            return null;
        }
        if (400 === $status) {
            throw new \Steefdw\TempoApi\Exception\DeleteProgramBadRequestException($response);
        }
        if (404 === $status) {
            throw new \Steefdw\TempoApi\Exception\DeleteProgramNotFoundException($response);
        }
    }

    public function getAuthenticationScopes(): array
    {
        return [];
    }
}
