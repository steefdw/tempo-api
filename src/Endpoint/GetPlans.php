<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Endpoint;

use Steefdw\TempoApi\Runtime\Client\BaseEndpoint;
use Steefdw\TempoApi\Runtime\Client\Endpoint;

class GetPlans extends BaseEndpoint implements Endpoint
{
    use \Steefdw\TempoApi\Runtime\Client\EndpointTrait;

    /**
     * Retrieves a list of existing Plans (Resource Allocations) that matches the given search parameters
     *
     * @param array $queryParameters {
     *     @var array $accountIds Ids of the user assignees you want to search plans for.
     *     @var array $assigneeTypes Types of the assignees you want to search plans for.
     *     @var string $from Retrieve plans starting with this date.
     *     @var array $genericResourceIds Ids of the generic resources you want to search plans for.
     *     @var int $limit Maximum number of results on each page.
     *     @var int $offset Number of skipped results.
     *     @var array $planIds Ids of the plans you want to search for.
     *     @var array $planItemIds Ids of the items you want to search plans for.
     *     @var array $planItemTypes Types of the items you want to search plans for.
     *     @var array $plannedTimeBreakdown Defines how detailed you would like to see the breakdown of the planned time for each `Plan`.
     *     @var string $to Retrieve plans that ends up to and including this date.
     *     @var string $updatedFrom Retrieve plans that have been updated from this date.
     * }
     */
    public function __construct(array $queryParameters = [])
    {
        $this->queryParameters = $queryParameters;
    }

    public function getMethod(): string
    {
        return 'GET';
    }

    public function getUri(): string
    {
        return '/plans';
    }

    public function getBody(\Symfony\Component\Serializer\SerializerInterface $serializer, $streamFactory = null): array
    {
        return [[], null];
    }

    public function getExtraHeaders(): array
    {
        return ['Accept' => ['application/json']];
    }

    protected function getQueryOptionsResolver(): \Symfony\Component\OptionsResolver\OptionsResolver
    {
        $optionsResolver = parent::getQueryOptionsResolver();
        $optionsResolver->setDefined(['accountIds', 'assigneeTypes', 'from', 'genericResourceIds', 'limit', 'offset', 'planIds', 'planItemIds', 'planItemTypes', 'plannedTimeBreakdown', 'to', 'updatedFrom']);
        $optionsResolver->setRequired(['from', 'to']);
        $optionsResolver->setDefaults([]);
        $optionsResolver->addAllowedTypes('accountIds', ['array']);
        $optionsResolver->addAllowedTypes('assigneeTypes', ['array']);
        $optionsResolver->addAllowedTypes('from', ['string']);
        $optionsResolver->addAllowedTypes('genericResourceIds', ['array']);
        $optionsResolver->addAllowedTypes('limit', ['int']);
        $optionsResolver->addAllowedTypes('offset', ['int']);
        $optionsResolver->addAllowedTypes('planIds', ['array']);
        $optionsResolver->addAllowedTypes('planItemIds', ['array']);
        $optionsResolver->addAllowedTypes('planItemTypes', ['array']);
        $optionsResolver->addAllowedTypes('plannedTimeBreakdown', ['array']);
        $optionsResolver->addAllowedTypes('to', ['string']);
        $optionsResolver->addAllowedTypes('updatedFrom', ['string']);

        return $optionsResolver;
    }

    /**
     * {@inheritdoc}
     *
     *
     * @return null|\Steefdw\TempoApi\Model\PageablePlan
     */
    protected function transformResponseBody(\Psr\Http\Message\ResponseInterface $response, \Symfony\Component\Serializer\SerializerInterface $serializer, ?string $contentType = null)
    {
        $status = $response->getStatusCode();
        $body = (string) $response->getBody();
        if (is_null($contentType) === false && (200 === $status && mb_strpos($contentType, 'application/json') !== false)) {
            return $serializer->deserialize($body, \Steefdw\TempoApi\Model\PageablePlan::class, 'json');
        }
    }

    public function getAuthenticationScopes(): array
    {
        return [];
    }
}
