<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Endpoint;

use Steefdw\TempoApi\Runtime\Client\BaseEndpoint;
use Steefdw\TempoApi\Runtime\Client\Endpoint;

class RemoveSkillAssignment extends BaseEndpoint implements Endpoint
{
    use \Steefdw\TempoApi\Runtime\Client\EndpointTrait;
    protected $assigneeId;
    protected $assigneeType;
    protected $skillId;

    /**
     * Deletes an existing Skill for the given Resource
     *
     * @param string $assigneeId A string that uniquely identifies the resource
     * @param string $assigneeType A string that identifies the resource type, which can be USER or GENERIC
     * @param int $skillId An id that uniquely identify the Skill
     */
    public function __construct(string $assigneeId, string $assigneeType, int $skillId)
    {
        $this->assigneeId = $assigneeId;
        $this->assigneeType = $assigneeType;
        $this->skillId = $skillId;
    }

    public function getMethod(): string
    {
        return 'DELETE';
    }

    public function getUri(): string
    {
        return str_replace(['{assigneeId}', '{assigneeType}', '{skillId}'], [$this->assigneeId, $this->assigneeType, $this->skillId], '/skill-assignments/{assigneeId}/{assigneeType}/{skillId}');
    }

    public function getBody(\Symfony\Component\Serializer\SerializerInterface $serializer, $streamFactory = null): array
    {
        return [[], null];
    }

    /**
     * {@inheritdoc}
     *
     *
     * @return null
     */
    protected function transformResponseBody(\Psr\Http\Message\ResponseInterface $response, \Symfony\Component\Serializer\SerializerInterface $serializer, ?string $contentType = null)
    {
        $status = $response->getStatusCode();
        $body = (string) $response->getBody();
        if (204 === $status) {
            return null;
        }
    }

    public function getAuthenticationScopes(): array
    {
        return [];
    }
}
