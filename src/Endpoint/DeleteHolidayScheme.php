<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Endpoint;

use Steefdw\TempoApi\Runtime\Client\BaseEndpoint;
use Steefdw\TempoApi\Runtime\Client\Endpoint;

class DeleteHolidayScheme extends BaseEndpoint implements Endpoint
{
    use \Steefdw\TempoApi\Runtime\Client\EndpointTrait;
    protected $schemeId;

    /**
     * Delete a holiday scheme with the given id
     *
     * @param string $schemeId The id of the Scheme
     */
    public function __construct(string $schemeId)
    {
        $this->schemeId = $schemeId;
    }

    public function getMethod(): string
    {
        return 'DELETE';
    }

    public function getUri(): string
    {
        return str_replace(['{schemeId}'], [$this->schemeId], '/holiday-schemes/{schemeId}');
    }

    public function getBody(\Symfony\Component\Serializer\SerializerInterface $serializer, $streamFactory = null): array
    {
        return [[], null];
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Steefdw\TempoApi\Exception\DeleteHolidaySchemeBadRequestException
     * @throws \Steefdw\TempoApi\Exception\DeleteHolidaySchemeNotFoundException
     *
     * @return null
     */
    protected function transformResponseBody(\Psr\Http\Message\ResponseInterface $response, \Symfony\Component\Serializer\SerializerInterface $serializer, ?string $contentType = null)
    {
        $status = $response->getStatusCode();
        $body = (string) $response->getBody();
        if (204 === $status) {
            return null;
        }
        if (400 === $status) {
            throw new \Steefdw\TempoApi\Exception\DeleteHolidaySchemeBadRequestException($response);
        }
        if (404 === $status) {
            throw new \Steefdw\TempoApi\Exception\DeleteHolidaySchemeNotFoundException($response);
        }
    }

    public function getAuthenticationScopes(): array
    {
        return [];
    }
}
