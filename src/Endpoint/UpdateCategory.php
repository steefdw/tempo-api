<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Endpoint;

use Steefdw\TempoApi\Runtime\Client\BaseEndpoint;
use Steefdw\TempoApi\Runtime\Client\Endpoint;

class UpdateCategory extends BaseEndpoint implements Endpoint
{
    use \Steefdw\TempoApi\Runtime\Client\EndpointTrait;
    protected $key;

    /**
     * Updates the Category for the given key using the provided input and returns the updated Category
     *
     * @param string $key A set of characters that uniquely identify the category to be updated
     * @param null|\Steefdw\TempoApi\Model\CategoryInput $requestBody
     */
    public function __construct(string $key, ?\Steefdw\TempoApi\Model\CategoryInput $requestBody = null)
    {
        $this->key = $key;
        $this->body = $requestBody;
    }

    public function getMethod(): string
    {
        return 'PUT';
    }

    public function getUri(): string
    {
        return str_replace(['{key}'], [$this->key], '/account-categories/{key}');
    }

    public function getBody(\Symfony\Component\Serializer\SerializerInterface $serializer, $streamFactory = null): array
    {
        if ($this->body instanceof \Steefdw\TempoApi\Model\CategoryInput) {
            return [['Content-Type' => ['application/json']], $serializer->serialize($this->body, 'json')];
        }

        return [[], null];
    }

    public function getExtraHeaders(): array
    {
        return ['Accept' => ['application/json']];
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Steefdw\TempoApi\Exception\UpdateCategoryBadRequestException
     * @throws \Steefdw\TempoApi\Exception\UpdateCategoryNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\Category
     */
    protected function transformResponseBody(\Psr\Http\Message\ResponseInterface $response, \Symfony\Component\Serializer\SerializerInterface $serializer, ?string $contentType = null)
    {
        $status = $response->getStatusCode();
        $body = (string) $response->getBody();
        if (is_null($contentType) === false && (200 === $status && mb_strpos($contentType, 'application/json') !== false)) {
            return $serializer->deserialize($body, \Steefdw\TempoApi\Model\Category::class, 'json');
        }
        if (400 === $status) {
            throw new \Steefdw\TempoApi\Exception\UpdateCategoryBadRequestException($response);
        }
        if (404 === $status) {
            throw new \Steefdw\TempoApi\Exception\UpdateCategoryNotFoundException($response);
        }
    }

    public function getAuthenticationScopes(): array
    {
        return [];
    }
}
