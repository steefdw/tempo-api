<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Endpoint;

use Steefdw\TempoApi\Runtime\Client\BaseEndpoint;
use Steefdw\TempoApi\Runtime\Client\Endpoint;

class GetSkillAssignments extends BaseEndpoint implements Endpoint
{
    use \Steefdw\TempoApi\Runtime\Client\EndpointTrait;
    protected $assigneeId;
    protected $assigneeType;

    /**
     * Retrieves a list of Skill Assignments for the given Resource
     *
     * @param string $assigneeId A string that uniquely identifies the resource
     * @param string $assigneeType A string that identifies the resource type, which can be USER or GENERIC
     */
    public function __construct(string $assigneeId, string $assigneeType)
    {
        $this->assigneeId = $assigneeId;
        $this->assigneeType = $assigneeType;
    }

    public function getMethod(): string
    {
        return 'GET';
    }

    public function getUri(): string
    {
        return str_replace(['{assigneeId}', '{assigneeType}'], [$this->assigneeId, $this->assigneeType], '/skill-assignments/{assigneeId}/{assigneeType}');
    }

    public function getBody(\Symfony\Component\Serializer\SerializerInterface $serializer, $streamFactory = null): array
    {
        return [[], null];
    }

    public function getExtraHeaders(): array
    {
        return ['Accept' => ['application/json']];
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Steefdw\TempoApi\Exception\GetSkillAssignmentsBadRequestException
     *
     * @return null|\Steefdw\TempoApi\Model\Skill[]
     */
    protected function transformResponseBody(\Psr\Http\Message\ResponseInterface $response, \Symfony\Component\Serializer\SerializerInterface $serializer, ?string $contentType = null)
    {
        $status = $response->getStatusCode();
        $body = (string) $response->getBody();
        if (is_null($contentType) === false && (200 === $status && mb_strpos($contentType, 'application/json') !== false)) {
            return $serializer->deserialize($body, 'Steefdw\\TempoApi\\Model\\Skill[]', 'json');
        }
        if (400 === $status) {
            throw new \Steefdw\TempoApi\Exception\GetSkillAssignmentsBadRequestException($response);
        }
    }

    public function getAuthenticationScopes(): array
    {
        return [];
    }
}
