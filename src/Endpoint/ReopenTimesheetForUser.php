<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Endpoint;

use Steefdw\TempoApi\Runtime\Client\BaseEndpoint;
use Steefdw\TempoApi\Runtime\Client\Endpoint;

class ReopenTimesheetForUser extends BaseEndpoint implements Endpoint
{
    use \Steefdw\TempoApi\Runtime\Client\EndpointTrait;
    protected $accountId;

    /**
     * Reopens a Timesheet for the given User in the given period and returns the reopened Timesheet
     *
     * @param string $accountId
     * @param null|\Steefdw\TempoApi\Model\TimesheetApprovalInput $requestBody
     * @param array $queryParameters {
     *     @var string $from The Start Date in format yyyy-mm-dd
     *     @var string $to The End Date in format yyyy-mm-dd
     * }
     */
    public function __construct(string $accountId, ?\Steefdw\TempoApi\Model\TimesheetApprovalInput $requestBody = null, array $queryParameters = [])
    {
        $this->accountId = $accountId;
        $this->body = $requestBody;
        $this->queryParameters = $queryParameters;
    }

    public function getMethod(): string
    {
        return 'POST';
    }

    public function getUri(): string
    {
        return str_replace(['{accountId}'], [$this->accountId], '/timesheet-approvals/user/{accountId}/reopen');
    }

    public function getBody(\Symfony\Component\Serializer\SerializerInterface $serializer, $streamFactory = null): array
    {
        if ($this->body instanceof \Steefdw\TempoApi\Model\TimesheetApprovalInput) {
            return [['Content-Type' => ['application/json']], $serializer->serialize($this->body, 'json')];
        }

        return [[], null];
    }

    public function getExtraHeaders(): array
    {
        return ['Accept' => ['application/json']];
    }

    protected function getQueryOptionsResolver(): \Symfony\Component\OptionsResolver\OptionsResolver
    {
        $optionsResolver = parent::getQueryOptionsResolver();
        $optionsResolver->setDefined(['from', 'to']);
        $optionsResolver->setRequired(['from']);
        $optionsResolver->setDefaults([]);
        $optionsResolver->addAllowedTypes('from', ['string']);
        $optionsResolver->addAllowedTypes('to', ['string']);

        return $optionsResolver;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Steefdw\TempoApi\Exception\ReopenTimesheetForUserBadRequestException
     * @throws \Steefdw\TempoApi\Exception\ReopenTimesheetForUserNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\TimesheetApproval
     */
    protected function transformResponseBody(\Psr\Http\Message\ResponseInterface $response, \Symfony\Component\Serializer\SerializerInterface $serializer, ?string $contentType = null)
    {
        $status = $response->getStatusCode();
        $body = (string) $response->getBody();
        if (is_null($contentType) === false && (200 === $status && mb_strpos($contentType, 'application/json') !== false)) {
            return $serializer->deserialize($body, \Steefdw\TempoApi\Model\TimesheetApproval::class, 'json');
        }
        if (400 === $status) {
            throw new \Steefdw\TempoApi\Exception\ReopenTimesheetForUserBadRequestException($response);
        }
        if (404 === $status) {
            throw new \Steefdw\TempoApi\Exception\ReopenTimesheetForUserNotFoundException($response);
        }
    }

    public function getAuthenticationScopes(): array
    {
        return [];
    }
}
