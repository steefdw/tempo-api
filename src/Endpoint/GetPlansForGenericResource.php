<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Endpoint;

use Steefdw\TempoApi\Runtime\Client\BaseEndpoint;
use Steefdw\TempoApi\Runtime\Client\Endpoint;

class GetPlansForGenericResource extends BaseEndpoint implements Endpoint
{
    use \Steefdw\TempoApi\Runtime\Client\EndpointTrait;
    protected $genericResourceId;

    /**
     * Retrieves a list of existing Plans (Resource Allocations) for the given Generic Resource
     *
     * @param int $genericResourceId Id of the generic resource you want to search plans for
     * @param array $queryParameters {
     *     @var array $plannedTimeBreakdown Defines how detailed you would like to see the breakdown of the planned time for each plan.You can add one or both of these values as query params [DAILY, PERIOD] to see more detailed breakdown of planned time.
     *     @var string $from Retrieve plans starting with this date
     *     @var string $to Retrieve plans that ends up to and including this date
     *     @var string $updatedFrom Retrieve plans that have been updated from this date
     * }
     */
    public function __construct(int $genericResourceId, array $queryParameters = [])
    {
        $this->genericResourceId = $genericResourceId;
        $this->queryParameters = $queryParameters;
    }

    public function getMethod(): string
    {
        return 'GET';
    }

    public function getUri(): string
    {
        return str_replace(['{genericResourceId}'], [$this->genericResourceId], '/plans/generic-resource/{genericResourceId}');
    }

    public function getBody(\Symfony\Component\Serializer\SerializerInterface $serializer, $streamFactory = null): array
    {
        return [[], null];
    }

    public function getExtraHeaders(): array
    {
        return ['Accept' => ['application/json']];
    }

    protected function getQueryOptionsResolver(): \Symfony\Component\OptionsResolver\OptionsResolver
    {
        $optionsResolver = parent::getQueryOptionsResolver();
        $optionsResolver->setDefined(['plannedTimeBreakdown', 'from', 'to', 'updatedFrom']);
        $optionsResolver->setRequired([]);
        $optionsResolver->setDefaults([]);
        $optionsResolver->addAllowedTypes('plannedTimeBreakdown', ['array']);
        $optionsResolver->addAllowedTypes('from', ['string']);
        $optionsResolver->addAllowedTypes('to', ['string']);
        $optionsResolver->addAllowedTypes('updatedFrom', ['string']);

        return $optionsResolver;
    }

    /**
     * {@inheritdoc}
     *
     *
     * @return null|\Steefdw\TempoApi\Model\PlanResults
     */
    protected function transformResponseBody(\Psr\Http\Message\ResponseInterface $response, \Symfony\Component\Serializer\SerializerInterface $serializer, ?string $contentType = null)
    {
        $status = $response->getStatusCode();
        $body = (string) $response->getBody();
        if (is_null($contentType) === false && (200 === $status && mb_strpos($contentType, 'application/json') !== false)) {
            return $serializer->deserialize($body, \Steefdw\TempoApi\Model\PlanResults::class, 'json');
        }
    }

    public function getAuthenticationScopes(): array
    {
        return [];
    }
}
