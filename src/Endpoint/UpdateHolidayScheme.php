<?php

declare(strict_types=1);

namespace Steefdw\TempoApi\Endpoint;

use Steefdw\TempoApi\Runtime\Client\BaseEndpoint;
use Steefdw\TempoApi\Runtime\Client\Endpoint;

class UpdateHolidayScheme extends BaseEndpoint implements Endpoint
{
    use \Steefdw\TempoApi\Runtime\Client\EndpointTrait;
    protected $schemeId;

    /**
     * Update a holiday scheme with the given id using the provided input
     *
     * @param string $schemeId The id of the Scheme
     * @param null|\Steefdw\TempoApi\Model\HolidaySchemeInputBean $requestBody
     */
    public function __construct(string $schemeId, ?\Steefdw\TempoApi\Model\HolidaySchemeInputBean $requestBody = null)
    {
        $this->schemeId = $schemeId;
        $this->body = $requestBody;
    }

    public function getMethod(): string
    {
        return 'PUT';
    }

    public function getUri(): string
    {
        return str_replace(['{schemeId}'], [$this->schemeId], '/holiday-schemes/{schemeId}');
    }

    public function getBody(\Symfony\Component\Serializer\SerializerInterface $serializer, $streamFactory = null): array
    {
        if ($this->body instanceof \Steefdw\TempoApi\Model\HolidaySchemeInputBean) {
            return [['Content-Type' => ['application/json']], $serializer->serialize($this->body, 'json')];
        }

        return [[], null];
    }

    public function getExtraHeaders(): array
    {
        return ['Accept' => ['application/json']];
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Steefdw\TempoApi\Exception\UpdateHolidaySchemeBadRequestException
     * @throws \Steefdw\TempoApi\Exception\UpdateHolidaySchemeNotFoundException
     *
     * @return null|\Steefdw\TempoApi\Model\HolidayScheme
     */
    protected function transformResponseBody(\Psr\Http\Message\ResponseInterface $response, \Symfony\Component\Serializer\SerializerInterface $serializer, ?string $contentType = null)
    {
        $status = $response->getStatusCode();
        $body = (string) $response->getBody();
        if (is_null($contentType) === false && (200 === $status && mb_strpos($contentType, 'application/json') !== false)) {
            return $serializer->deserialize($body, \Steefdw\TempoApi\Model\HolidayScheme::class, 'json');
        }
        if (400 === $status) {
            throw new \Steefdw\TempoApi\Exception\UpdateHolidaySchemeBadRequestException($response);
        }
        if (404 === $status) {
            throw new \Steefdw\TempoApi\Exception\UpdateHolidaySchemeNotFoundException($response);
        }
    }

    public function getAuthenticationScopes(): array
    {
        return [];
    }
}
